Problem:    sop
Rows:       1397
Columns:    694 (667 integer, 667 binary)
Non-zeros:  5782
Status:     INTEGER OPTIMAL
Objective:  totalcost = 1762 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 totalcost                1762                             
     2 VISIT_ALL[0]                1             1             = 
     3 VISIT_ALL[1]                1             1             = 
     4 VISIT_ALL[2]                1             1             = 
     5 VISIT_ALL[3]                1             1             = 
     6 VISIT_ALL[4]                1             1             = 
     7 VISIT_ALL[5]                1             1             = 
     8 VISIT_ALL[6]                1             1             = 
     9 VISIT_ALL[7]                1             1             = 
    10 VISIT_ALL[8]                1             1             = 
    11 VISIT_ALL[9]                1             1             = 
    12 VISIT_ALL[10]
                                   1             1             = 
    13 VISIT_ALL[11]
                                   1             1             = 
    14 VISIT_ALL[12]
                                   1             1             = 
    15 VISIT_ALL[13]
                                   1             1             = 
    16 VISIT_ALL[14]
                                   1             1             = 
    17 VISIT_ALL[15]
                                   1             1             = 
    18 VISIT_ALL[16]
                                   1             1             = 
    19 VISIT_ALL[17]
                                   1             1             = 
    20 VISIT_ALL[18]
                                   1             1             = 
    21 VISIT_ALL[19]
                                   1             1             = 
    22 VISIT_ALL[20]
                                   1             1             = 
    23 VISIT_ALL[21]
                                   1             1             = 
    24 VISIT_ALL[22]
                                   1             1             = 
    25 VISIT_ALL[23]
                                   1             1             = 
    26 VISIT_ALL[24]
                                   1             1             = 
    27 VISIT_ALL[25]
                                   1             1             = 
    28 VISIT_ALL[26]
                                   1             1             = 
    29 ARC_NODE_IMPLICATION[0,1]
                                   0                          -0 
    30 ARC_NODE_IMPLICATION[0,2]
                                  -2                          -0 
    31 ARC_NODE_IMPLICATION[0,3]
                                  -2                          -0 
    32 ARC_NODE_IMPLICATION[0,4]
                                  -2                          -0 
    33 ARC_NODE_IMPLICATION[0,5]
                                  -2                          -0 
    34 ARC_NODE_IMPLICATION[0,6]
                                  -2                          -0 
    35 ARC_NODE_IMPLICATION[0,7]
                                  -2                          -0 
    36 ARC_NODE_IMPLICATION[0,8]
                                  -2                          -0 
    37 ARC_NODE_IMPLICATION[0,9]
                                  -2                          -0 
    38 ARC_NODE_IMPLICATION[0,10]
                                  -2                          -0 
    39 ARC_NODE_IMPLICATION[0,11]
                                  -2                          -0 
    40 ARC_NODE_IMPLICATION[0,12]
                                  -2                          -0 
    41 ARC_NODE_IMPLICATION[0,13]
                                  -2                          -0 
    42 ARC_NODE_IMPLICATION[0,14]
                                  -2                          -0 
    43 ARC_NODE_IMPLICATION[0,15]
                                  -2                          -0 
    44 ARC_NODE_IMPLICATION[0,16]
                                  -2                          -0 
    45 ARC_NODE_IMPLICATION[0,17]
                                  -2                          -0 
    46 ARC_NODE_IMPLICATION[0,18]
                                  -2                          -0 
    47 ARC_NODE_IMPLICATION[0,19]
                                  -2                          -0 
    48 ARC_NODE_IMPLICATION[0,20]
                                  -2                          -0 
    49 ARC_NODE_IMPLICATION[0,21]
                                  -2                          -0 
    50 ARC_NODE_IMPLICATION[0,22]
                                  -2                          -0 
    51 ARC_NODE_IMPLICATION[0,23]
                                  -2                          -0 
    52 ARC_NODE_IMPLICATION[0,24]
                                  -2                          -0 
    53 ARC_NODE_IMPLICATION[0,25]
                                  -2                          -0 
    54 ARC_NODE_IMPLICATION[0,26]
                                  -2                          -0 
    55 ARC_NODE_IMPLICATION[1,2]
                                  -2                          -0 
    56 ARC_NODE_IMPLICATION[1,3]
                                  -2                          -0 
    57 ARC_NODE_IMPLICATION[1,4]
                                  -2                          -0 
    58 ARC_NODE_IMPLICATION[1,5]
                                  -2                          -0 
    59 ARC_NODE_IMPLICATION[1,6]
                                  -2                          -0 
    60 ARC_NODE_IMPLICATION[1,7]
                                  -2                          -0 
    61 ARC_NODE_IMPLICATION[1,8]
                                  -2                          -0 
    62 ARC_NODE_IMPLICATION[1,9]
                                  -2                          -0 
    63 ARC_NODE_IMPLICATION[1,10]
                                  -2                          -0 
    64 ARC_NODE_IMPLICATION[1,11]
                                  -2                          -0 
    65 ARC_NODE_IMPLICATION[1,12]
                                  -2                          -0 
    66 ARC_NODE_IMPLICATION[1,13]
                                   0                          -0 
    67 ARC_NODE_IMPLICATION[1,14]
                                  -2                          -0 
    68 ARC_NODE_IMPLICATION[1,15]
                                  -2                          -0 
    69 ARC_NODE_IMPLICATION[1,16]
                                  -2                          -0 
    70 ARC_NODE_IMPLICATION[1,17]
                                  -2                          -0 
    71 ARC_NODE_IMPLICATION[1,18]
                                  -2                          -0 
    72 ARC_NODE_IMPLICATION[1,19]
                                  -2                          -0 
    73 ARC_NODE_IMPLICATION[1,20]
                                  -2                          -0 
    74 ARC_NODE_IMPLICATION[1,21]
                                  -2                          -0 
    75 ARC_NODE_IMPLICATION[1,22]
                                  -2                          -0 
    76 ARC_NODE_IMPLICATION[1,23]
                                  -2                          -0 
    77 ARC_NODE_IMPLICATION[1,24]
                                  -2                          -0 
    78 ARC_NODE_IMPLICATION[1,25]
                                  -2                          -0 
    79 ARC_NODE_IMPLICATION[1,26]
                                  -2                          -0 
    80 ARC_NODE_IMPLICATION[2,1]
                                  -2                          -0 
    81 ARC_NODE_IMPLICATION[2,3]
                                  -2                          -0 
    82 ARC_NODE_IMPLICATION[2,4]
                                  -2                          -0 
    83 ARC_NODE_IMPLICATION[2,5]
                                  -2                          -0 
    84 ARC_NODE_IMPLICATION[2,6]
                                  -2                          -0 
    85 ARC_NODE_IMPLICATION[2,7]
                                  -2                          -0 
    86 ARC_NODE_IMPLICATION[2,8]
                                  -2                          -0 
    87 ARC_NODE_IMPLICATION[2,9]
                                  -2                          -0 
    88 ARC_NODE_IMPLICATION[2,10]
                                   0                          -0 
    89 ARC_NODE_IMPLICATION[2,11]
                                  -2                          -0 
    90 ARC_NODE_IMPLICATION[2,12]
                                  -2                          -0 
    91 ARC_NODE_IMPLICATION[2,13]
                                  -2                          -0 
    92 ARC_NODE_IMPLICATION[2,14]
                                  -2                          -0 
    93 ARC_NODE_IMPLICATION[2,15]
                                  -2                          -0 
    94 ARC_NODE_IMPLICATION[2,16]
                                  -2                          -0 
    95 ARC_NODE_IMPLICATION[2,17]
                                  -2                          -0 
    96 ARC_NODE_IMPLICATION[2,18]
                                  -2                          -0 
    97 ARC_NODE_IMPLICATION[2,19]
                                  -2                          -0 
    98 ARC_NODE_IMPLICATION[2,20]
                                  -2                          -0 
    99 ARC_NODE_IMPLICATION[2,21]
                                  -2                          -0 
   100 ARC_NODE_IMPLICATION[2,22]
                                  -2                          -0 
   101 ARC_NODE_IMPLICATION[2,23]
                                  -2                          -0 
   102 ARC_NODE_IMPLICATION[2,24]
                                  -2                          -0 
   103 ARC_NODE_IMPLICATION[2,25]
                                  -2                          -0 
   104 ARC_NODE_IMPLICATION[2,26]
                                  -2                          -0 
   105 ARC_NODE_IMPLICATION[3,1]
                                  -2                          -0 
   106 ARC_NODE_IMPLICATION[3,2]
                                  -2                          -0 
   107 ARC_NODE_IMPLICATION[3,4]
                                  -2                          -0 
   108 ARC_NODE_IMPLICATION[3,5]
                                  -2                          -0 
   109 ARC_NODE_IMPLICATION[3,6]
                                  -2                          -0 
   110 ARC_NODE_IMPLICATION[3,7]
                                  -2                          -0 
   111 ARC_NODE_IMPLICATION[3,8]
                                  -2                          -0 
   112 ARC_NODE_IMPLICATION[3,9]
                                   0                          -0 
   113 ARC_NODE_IMPLICATION[3,10]
                                  -2                          -0 
   114 ARC_NODE_IMPLICATION[3,11]
                                  -2                          -0 
   115 ARC_NODE_IMPLICATION[3,12]
                                  -2                          -0 
   116 ARC_NODE_IMPLICATION[3,13]
                                  -2                          -0 
   117 ARC_NODE_IMPLICATION[3,14]
                                  -2                          -0 
   118 ARC_NODE_IMPLICATION[3,15]
                                  -2                          -0 
   119 ARC_NODE_IMPLICATION[3,16]
                                  -2                          -0 
   120 ARC_NODE_IMPLICATION[3,17]
                                  -2                          -0 
   121 ARC_NODE_IMPLICATION[3,18]
                                  -2                          -0 
   122 ARC_NODE_IMPLICATION[3,19]
                                  -2                          -0 
   123 ARC_NODE_IMPLICATION[3,20]
                                  -2                          -0 
   124 ARC_NODE_IMPLICATION[3,21]
                                  -2                          -0 
   125 ARC_NODE_IMPLICATION[3,22]
                                  -2                          -0 
   126 ARC_NODE_IMPLICATION[3,23]
                                  -2                          -0 
   127 ARC_NODE_IMPLICATION[3,24]
                                  -2                          -0 
   128 ARC_NODE_IMPLICATION[3,25]
                                  -2                          -0 
   129 ARC_NODE_IMPLICATION[3,26]
                                  -2                          -0 
   130 ARC_NODE_IMPLICATION[4,1]
                                  -2                          -0 
   131 ARC_NODE_IMPLICATION[4,2]
                                  -2                          -0 
   132 ARC_NODE_IMPLICATION[4,3]
                                  -2                          -0 
   133 ARC_NODE_IMPLICATION[4,5]
                                  -2                          -0 
   134 ARC_NODE_IMPLICATION[4,6]
                                  -2                          -0 
   135 ARC_NODE_IMPLICATION[4,7]
                                  -2                          -0 
   136 ARC_NODE_IMPLICATION[4,8]
                                  -2                          -0 
   137 ARC_NODE_IMPLICATION[4,9]
                                  -2                          -0 
   138 ARC_NODE_IMPLICATION[4,10]
                                  -2                          -0 
   139 ARC_NODE_IMPLICATION[4,11]
                                   0                          -0 
   140 ARC_NODE_IMPLICATION[4,12]
                                  -2                          -0 
   141 ARC_NODE_IMPLICATION[4,13]
                                  -2                          -0 
   142 ARC_NODE_IMPLICATION[4,14]
                                  -2                          -0 
   143 ARC_NODE_IMPLICATION[4,15]
                                  -2                          -0 
   144 ARC_NODE_IMPLICATION[4,16]
                                  -2                          -0 
   145 ARC_NODE_IMPLICATION[4,17]
                                  -2                          -0 
   146 ARC_NODE_IMPLICATION[4,18]
                                  -2                          -0 
   147 ARC_NODE_IMPLICATION[4,19]
                                  -2                          -0 
   148 ARC_NODE_IMPLICATION[4,20]
                                  -2                          -0 
   149 ARC_NODE_IMPLICATION[4,21]
                                  -2                          -0 
   150 ARC_NODE_IMPLICATION[4,22]
                                  -2                          -0 
   151 ARC_NODE_IMPLICATION[4,23]
                                  -2                          -0 
   152 ARC_NODE_IMPLICATION[4,24]
                                  -2                          -0 
   153 ARC_NODE_IMPLICATION[4,25]
                                  -2                          -0 
   154 ARC_NODE_IMPLICATION[4,26]
                                  -2                          -0 
   155 ARC_NODE_IMPLICATION[5,1]
                                  -2                          -0 
   156 ARC_NODE_IMPLICATION[5,2]
                                  -2                          -0 
   157 ARC_NODE_IMPLICATION[5,3]
                                  -2                          -0 
   158 ARC_NODE_IMPLICATION[5,4]
                                  -2                          -0 
   159 ARC_NODE_IMPLICATION[5,6]
                                  -2                          -0 
   160 ARC_NODE_IMPLICATION[5,7]
                                   0                          -0 
   161 ARC_NODE_IMPLICATION[5,8]
                                  -2                          -0 
   162 ARC_NODE_IMPLICATION[5,9]
                                  -2                          -0 
   163 ARC_NODE_IMPLICATION[5,10]
                                  -2                          -0 
   164 ARC_NODE_IMPLICATION[5,11]
                                  -2                          -0 
   165 ARC_NODE_IMPLICATION[5,12]
                                  -2                          -0 
   166 ARC_NODE_IMPLICATION[5,13]
                                  -2                          -0 
   167 ARC_NODE_IMPLICATION[5,14]
                                  -2                          -0 
   168 ARC_NODE_IMPLICATION[5,15]
                                  -2                          -0 
   169 ARC_NODE_IMPLICATION[5,16]
                                  -2                          -0 
   170 ARC_NODE_IMPLICATION[5,17]
                                  -2                          -0 
   171 ARC_NODE_IMPLICATION[5,18]
                                  -2                          -0 
   172 ARC_NODE_IMPLICATION[5,19]
                                  -2                          -0 
   173 ARC_NODE_IMPLICATION[5,20]
                                  -2                          -0 
   174 ARC_NODE_IMPLICATION[5,21]
                                  -2                          -0 
   175 ARC_NODE_IMPLICATION[5,22]
                                  -2                          -0 
   176 ARC_NODE_IMPLICATION[5,23]
                                  -2                          -0 
   177 ARC_NODE_IMPLICATION[5,24]
                                  -2                          -0 
   178 ARC_NODE_IMPLICATION[5,25]
                                  -2                          -0 
   179 ARC_NODE_IMPLICATION[5,26]
                                  -2                          -0 
   180 ARC_NODE_IMPLICATION[6,2]
                                  -2                          -0 
   181 ARC_NODE_IMPLICATION[6,3]
                                  -2                          -0 
   182 ARC_NODE_IMPLICATION[6,4]
                                  -2                          -0 
   183 ARC_NODE_IMPLICATION[6,5]
                                  -2                          -0 
   184 ARC_NODE_IMPLICATION[6,7]
                                  -2                          -0 
   185 ARC_NODE_IMPLICATION[6,8]
                                  -2                          -0 
   186 ARC_NODE_IMPLICATION[6,9]
                                  -2                          -0 
   187 ARC_NODE_IMPLICATION[6,10]
                                  -2                          -0 
   188 ARC_NODE_IMPLICATION[6,11]
                                  -2                          -0 
   189 ARC_NODE_IMPLICATION[6,12]
                                  -2                          -0 
   190 ARC_NODE_IMPLICATION[6,13]
                                  -2                          -0 
   191 ARC_NODE_IMPLICATION[6,14]
                                  -2                          -0 
   192 ARC_NODE_IMPLICATION[6,15]
                                  -2                          -0 
   193 ARC_NODE_IMPLICATION[6,16]
                                  -2                          -0 
   194 ARC_NODE_IMPLICATION[6,17]
                                  -2                          -0 
   195 ARC_NODE_IMPLICATION[6,18]
                                  -2                          -0 
   196 ARC_NODE_IMPLICATION[6,19]
                                  -2                          -0 
   197 ARC_NODE_IMPLICATION[6,20]
                                  -2                          -0 
   198 ARC_NODE_IMPLICATION[6,21]
                                   0                          -0 
   199 ARC_NODE_IMPLICATION[6,22]
                                  -2                          -0 
   200 ARC_NODE_IMPLICATION[6,23]
                                  -2                          -0 
   201 ARC_NODE_IMPLICATION[6,24]
                                  -2                          -0 
   202 ARC_NODE_IMPLICATION[6,25]
                                  -2                          -0 
   203 ARC_NODE_IMPLICATION[6,26]
                                  -2                          -0 
   204 ARC_NODE_IMPLICATION[7,1]
                                  -2                          -0 
   205 ARC_NODE_IMPLICATION[7,2]
                                  -2                          -0 
   206 ARC_NODE_IMPLICATION[7,3]
                                  -2                          -0 
   207 ARC_NODE_IMPLICATION[7,4]
                                  -2                          -0 
   208 ARC_NODE_IMPLICATION[7,5]
                                  -2                          -0 
   209 ARC_NODE_IMPLICATION[7,6]
                                  -2                          -0 
   210 ARC_NODE_IMPLICATION[7,8]
                                  -2                          -0 
   211 ARC_NODE_IMPLICATION[7,9]
                                  -2                          -0 
   212 ARC_NODE_IMPLICATION[7,10]
                                  -2                          -0 
   213 ARC_NODE_IMPLICATION[7,11]
                                  -2                          -0 
   214 ARC_NODE_IMPLICATION[7,12]
                                  -2                          -0 
   215 ARC_NODE_IMPLICATION[7,13]
                                  -2                          -0 
   216 ARC_NODE_IMPLICATION[7,14]
                                   0                          -0 
   217 ARC_NODE_IMPLICATION[7,15]
                                  -2                          -0 
   218 ARC_NODE_IMPLICATION[7,16]
                                  -2                          -0 
   219 ARC_NODE_IMPLICATION[7,17]
                                  -2                          -0 
   220 ARC_NODE_IMPLICATION[7,18]
                                  -2                          -0 
   221 ARC_NODE_IMPLICATION[7,19]
                                  -2                          -0 
   222 ARC_NODE_IMPLICATION[7,20]
                                  -2                          -0 
   223 ARC_NODE_IMPLICATION[7,21]
                                  -2                          -0 
   224 ARC_NODE_IMPLICATION[7,22]
                                  -2                          -0 
   225 ARC_NODE_IMPLICATION[7,23]
                                  -2                          -0 
   226 ARC_NODE_IMPLICATION[7,24]
                                  -2                          -0 
   227 ARC_NODE_IMPLICATION[7,25]
                                  -2                          -0 
   228 ARC_NODE_IMPLICATION[7,26]
                                  -2                          -0 
   229 ARC_NODE_IMPLICATION[8,1]
                                  -2                          -0 
   230 ARC_NODE_IMPLICATION[8,2]
                                  -2                          -0 
   231 ARC_NODE_IMPLICATION[8,3]
                                  -2                          -0 
   232 ARC_NODE_IMPLICATION[8,4]
                                   0                          -0 
   233 ARC_NODE_IMPLICATION[8,5]
                                  -2                          -0 
   234 ARC_NODE_IMPLICATION[8,6]
                                  -2                          -0 
   235 ARC_NODE_IMPLICATION[8,7]
                                  -2                          -0 
   236 ARC_NODE_IMPLICATION[8,9]
                                  -2                          -0 
   237 ARC_NODE_IMPLICATION[8,10]
                                  -2                          -0 
   238 ARC_NODE_IMPLICATION[8,11]
                                  -2                          -0 
   239 ARC_NODE_IMPLICATION[8,12]
                                  -2                          -0 
   240 ARC_NODE_IMPLICATION[8,13]
                                  -2                          -0 
   241 ARC_NODE_IMPLICATION[8,14]
                                  -2                          -0 
   242 ARC_NODE_IMPLICATION[8,15]
                                  -2                          -0 
   243 ARC_NODE_IMPLICATION[8,16]
                                  -2                          -0 
   244 ARC_NODE_IMPLICATION[8,17]
                                  -2                          -0 
   245 ARC_NODE_IMPLICATION[8,18]
                                  -2                          -0 
   246 ARC_NODE_IMPLICATION[8,19]
                                  -2                          -0 
   247 ARC_NODE_IMPLICATION[8,20]
                                  -2                          -0 
   248 ARC_NODE_IMPLICATION[8,21]
                                  -2                          -0 
   249 ARC_NODE_IMPLICATION[8,22]
                                  -2                          -0 
   250 ARC_NODE_IMPLICATION[8,23]
                                  -2                          -0 
   251 ARC_NODE_IMPLICATION[8,24]
                                  -2                          -0 
   252 ARC_NODE_IMPLICATION[8,25]
                                  -2                          -0 
   253 ARC_NODE_IMPLICATION[8,26]
                                  -2                          -0 
   254 ARC_NODE_IMPLICATION[9,1]
                                  -2                          -0 
   255 ARC_NODE_IMPLICATION[9,2]
                                  -2                          -0 
   256 ARC_NODE_IMPLICATION[9,3]
                                  -2                          -0 
   257 ARC_NODE_IMPLICATION[9,4]
                                  -2                          -0 
   258 ARC_NODE_IMPLICATION[9,5]
                                  -2                          -0 
   259 ARC_NODE_IMPLICATION[9,6]
                                  -2                          -0 
   260 ARC_NODE_IMPLICATION[9,7]
                                  -2                          -0 
   261 ARC_NODE_IMPLICATION[9,8]
                                  -2                          -0 
   262 ARC_NODE_IMPLICATION[9,10]
                                  -2                          -0 
   263 ARC_NODE_IMPLICATION[9,11]
                                  -2                          -0 
   264 ARC_NODE_IMPLICATION[9,12]
                                  -2                          -0 
   265 ARC_NODE_IMPLICATION[9,13]
                                  -2                          -0 
   266 ARC_NODE_IMPLICATION[9,14]
                                  -2                          -0 
   267 ARC_NODE_IMPLICATION[9,15]
                                  -2                          -0 
   268 ARC_NODE_IMPLICATION[9,16]
                                  -2                          -0 
   269 ARC_NODE_IMPLICATION[9,17]
                                  -2                          -0 
   270 ARC_NODE_IMPLICATION[9,18]
                                  -2                          -0 
   271 ARC_NODE_IMPLICATION[9,19]
                                  -2                          -0 
   272 ARC_NODE_IMPLICATION[9,20]
                                  -2                          -0 
   273 ARC_NODE_IMPLICATION[9,21]
                                  -2                          -0 
   274 ARC_NODE_IMPLICATION[9,22]
                                   0                          -0 
   275 ARC_NODE_IMPLICATION[9,23]
                                  -2                          -0 
   276 ARC_NODE_IMPLICATION[9,24]
                                  -2                          -0 
   277 ARC_NODE_IMPLICATION[9,25]
                                  -2                          -0 
   278 ARC_NODE_IMPLICATION[9,26]
                                  -2                          -0 
   279 ARC_NODE_IMPLICATION[10,1]
                                  -2                          -0 
   280 ARC_NODE_IMPLICATION[10,2]
                                  -2                          -0 
   281 ARC_NODE_IMPLICATION[10,3]
                                  -2                          -0 
   282 ARC_NODE_IMPLICATION[10,4]
                                  -2                          -0 
   283 ARC_NODE_IMPLICATION[10,5]
                                  -2                          -0 
   284 ARC_NODE_IMPLICATION[10,6]
                                  -2                          -0 
   285 ARC_NODE_IMPLICATION[10,7]
                                  -2                          -0 
   286 ARC_NODE_IMPLICATION[10,8]
                                  -2                          -0 
   287 ARC_NODE_IMPLICATION[10,9]
                                  -2                          -0 
   288 ARC_NODE_IMPLICATION[10,11]
                                  -2                          -0 
   289 ARC_NODE_IMPLICATION[10,12]
                                  -2                          -0 
   290 ARC_NODE_IMPLICATION[10,13]
                                  -2                          -0 
   291 ARC_NODE_IMPLICATION[10,14]
                                  -2                          -0 
   292 ARC_NODE_IMPLICATION[10,15]
                                  -2                          -0 
   293 ARC_NODE_IMPLICATION[10,16]
                                  -2                          -0 
   294 ARC_NODE_IMPLICATION[10,17]
                                  -2                          -0 
   295 ARC_NODE_IMPLICATION[10,18]
                                  -2                          -0 
   296 ARC_NODE_IMPLICATION[10,19]
                                   0                          -0 
   297 ARC_NODE_IMPLICATION[10,20]
                                  -2                          -0 
   298 ARC_NODE_IMPLICATION[10,21]
                                  -2                          -0 
   299 ARC_NODE_IMPLICATION[10,22]
                                  -2                          -0 
   300 ARC_NODE_IMPLICATION[10,23]
                                  -2                          -0 
   301 ARC_NODE_IMPLICATION[10,24]
                                  -2                          -0 
   302 ARC_NODE_IMPLICATION[10,25]
                                  -2                          -0 
   303 ARC_NODE_IMPLICATION[10,26]
                                  -2                          -0 
   304 ARC_NODE_IMPLICATION[11,2]
                                  -2                          -0 
   305 ARC_NODE_IMPLICATION[11,3]
                                  -2                          -0 
   306 ARC_NODE_IMPLICATION[11,4]
                                  -2                          -0 
   307 ARC_NODE_IMPLICATION[11,5]
                                  -2                          -0 
   308 ARC_NODE_IMPLICATION[11,6]
                                  -2                          -0 
   309 ARC_NODE_IMPLICATION[11,7]
                                  -2                          -0 
   310 ARC_NODE_IMPLICATION[11,8]
                                  -2                          -0 
   311 ARC_NODE_IMPLICATION[11,9]
                                  -2                          -0 
   312 ARC_NODE_IMPLICATION[11,10]
                                  -2                          -0 
   313 ARC_NODE_IMPLICATION[11,12]
                                  -2                          -0 
   314 ARC_NODE_IMPLICATION[11,13]
                                  -2                          -0 
   315 ARC_NODE_IMPLICATION[11,14]
                                  -2                          -0 
   316 ARC_NODE_IMPLICATION[11,15]
                                  -2                          -0 
   317 ARC_NODE_IMPLICATION[11,16]
                                   0                          -0 
   318 ARC_NODE_IMPLICATION[11,17]
                                  -2                          -0 
   319 ARC_NODE_IMPLICATION[11,18]
                                  -2                          -0 
   320 ARC_NODE_IMPLICATION[11,19]
                                  -2                          -0 
   321 ARC_NODE_IMPLICATION[11,20]
                                  -2                          -0 
   322 ARC_NODE_IMPLICATION[11,21]
                                  -2                          -0 
   323 ARC_NODE_IMPLICATION[11,22]
                                  -2                          -0 
   324 ARC_NODE_IMPLICATION[11,23]
                                  -2                          -0 
   325 ARC_NODE_IMPLICATION[11,24]
                                  -2                          -0 
   326 ARC_NODE_IMPLICATION[11,25]
                                  -2                          -0 
   327 ARC_NODE_IMPLICATION[11,26]
                                  -2                          -0 
   328 ARC_NODE_IMPLICATION[12,2]
                                  -2                          -0 
   329 ARC_NODE_IMPLICATION[12,3]
                                  -2                          -0 
   330 ARC_NODE_IMPLICATION[12,4]
                                  -2                          -0 
   331 ARC_NODE_IMPLICATION[12,5]
                                  -2                          -0 
   332 ARC_NODE_IMPLICATION[12,7]
                                  -2                          -0 
   333 ARC_NODE_IMPLICATION[12,9]
                                  -2                          -0 
   334 ARC_NODE_IMPLICATION[12,10]
                                  -2                          -0 
   335 ARC_NODE_IMPLICATION[12,11]
                                  -2                          -0 
   336 ARC_NODE_IMPLICATION[12,13]
                                  -2                          -0 
   337 ARC_NODE_IMPLICATION[12,14]
                                  -2                          -0 
   338 ARC_NODE_IMPLICATION[12,15]
                                   0                          -0 
   339 ARC_NODE_IMPLICATION[12,16]
                                  -2                          -0 
   340 ARC_NODE_IMPLICATION[12,17]
                                  -2                          -0 
   341 ARC_NODE_IMPLICATION[12,18]
                                  -2                          -0 
   342 ARC_NODE_IMPLICATION[12,19]
                                  -2                          -0 
   343 ARC_NODE_IMPLICATION[12,20]
                                  -2                          -0 
   344 ARC_NODE_IMPLICATION[12,21]
                                  -2                          -0 
   345 ARC_NODE_IMPLICATION[12,22]
                                  -2                          -0 
   346 ARC_NODE_IMPLICATION[12,23]
                                  -2                          -0 
   347 ARC_NODE_IMPLICATION[12,24]
                                  -2                          -0 
   348 ARC_NODE_IMPLICATION[12,25]
                                  -2                          -0 
   349 ARC_NODE_IMPLICATION[12,26]
                                  -2                          -0 
   350 ARC_NODE_IMPLICATION[13,1]
                                  -2                          -0 
   351 ARC_NODE_IMPLICATION[13,2]
                                  -2                          -0 
   352 ARC_NODE_IMPLICATION[13,3]
                                  -2                          -0 
   353 ARC_NODE_IMPLICATION[13,4]
                                  -2                          -0 
   354 ARC_NODE_IMPLICATION[13,5]
                                  -2                          -0 
   355 ARC_NODE_IMPLICATION[13,6]
                                  -2                          -0 
   356 ARC_NODE_IMPLICATION[13,7]
                                  -2                          -0 
   357 ARC_NODE_IMPLICATION[13,8]
                                  -2                          -0 
   358 ARC_NODE_IMPLICATION[13,9]
                                  -2                          -0 
   359 ARC_NODE_IMPLICATION[13,10]
                                  -2                          -0 
   360 ARC_NODE_IMPLICATION[13,11]
                                  -2                          -0 
   361 ARC_NODE_IMPLICATION[13,12]
                                  -2                          -0 
   362 ARC_NODE_IMPLICATION[13,14]
                                  -2                          -0 
   363 ARC_NODE_IMPLICATION[13,15]
                                  -2                          -0 
   364 ARC_NODE_IMPLICATION[13,16]
                                  -2                          -0 
   365 ARC_NODE_IMPLICATION[13,17]
                                  -2                          -0 
   366 ARC_NODE_IMPLICATION[13,18]
                                  -2                          -0 
   367 ARC_NODE_IMPLICATION[13,19]
                                  -2                          -0 
   368 ARC_NODE_IMPLICATION[13,20]
                                   0                          -0 
   369 ARC_NODE_IMPLICATION[13,21]
                                  -2                          -0 
   370 ARC_NODE_IMPLICATION[13,22]
                                  -2                          -0 
   371 ARC_NODE_IMPLICATION[13,23]
                                  -2                          -0 
   372 ARC_NODE_IMPLICATION[13,24]
                                  -2                          -0 
   373 ARC_NODE_IMPLICATION[13,25]
                                  -2                          -0 
   374 ARC_NODE_IMPLICATION[13,26]
                                  -2                          -0 
   375 ARC_NODE_IMPLICATION[14,1]
                                  -2                          -0 
   376 ARC_NODE_IMPLICATION[14,2]
                                  -2                          -0 
   377 ARC_NODE_IMPLICATION[14,3]
                                  -2                          -0 
   378 ARC_NODE_IMPLICATION[14,4]
                                  -2                          -0 
   379 ARC_NODE_IMPLICATION[14,5]
                                  -2                          -0 
   380 ARC_NODE_IMPLICATION[14,6]
                                   0                          -0 
   381 ARC_NODE_IMPLICATION[14,7]
                                  -2                          -0 
   382 ARC_NODE_IMPLICATION[14,8]
                                  -2                          -0 
   383 ARC_NODE_IMPLICATION[14,9]
                                  -2                          -0 
   384 ARC_NODE_IMPLICATION[14,10]
                                  -2                          -0 
   385 ARC_NODE_IMPLICATION[14,11]
                                  -2                          -0 
   386 ARC_NODE_IMPLICATION[14,12]
                                  -2                          -0 
   387 ARC_NODE_IMPLICATION[14,13]
                                  -2                          -0 
   388 ARC_NODE_IMPLICATION[14,15]
                                  -2                          -0 
   389 ARC_NODE_IMPLICATION[14,16]
                                  -2                          -0 
   390 ARC_NODE_IMPLICATION[14,17]
                                  -2                          -0 
   391 ARC_NODE_IMPLICATION[14,18]
                                  -2                          -0 
   392 ARC_NODE_IMPLICATION[14,19]
                                  -2                          -0 
   393 ARC_NODE_IMPLICATION[14,20]
                                  -2                          -0 
   394 ARC_NODE_IMPLICATION[14,21]
                                  -2                          -0 
   395 ARC_NODE_IMPLICATION[14,22]
                                  -2                          -0 
   396 ARC_NODE_IMPLICATION[14,23]
                                  -2                          -0 
   397 ARC_NODE_IMPLICATION[14,24]
                                  -2                          -0 
   398 ARC_NODE_IMPLICATION[14,25]
                                  -2                          -0 
   399 ARC_NODE_IMPLICATION[14,26]
                                  -2                          -0 
   400 ARC_NODE_IMPLICATION[15,1]
                                  -2                          -0 
   401 ARC_NODE_IMPLICATION[15,2]
                                  -2                          -0 
   402 ARC_NODE_IMPLICATION[15,3]
                                   0                          -0 
   403 ARC_NODE_IMPLICATION[15,4]
                                  -2                          -0 
   404 ARC_NODE_IMPLICATION[15,5]
                                  -2                          -0 
   405 ARC_NODE_IMPLICATION[15,6]
                                  -2                          -0 
   406 ARC_NODE_IMPLICATION[15,7]
                                  -2                          -0 
   407 ARC_NODE_IMPLICATION[15,8]
                                  -2                          -0 
   408 ARC_NODE_IMPLICATION[15,9]
                                  -2                          -0 
   409 ARC_NODE_IMPLICATION[15,10]
                                  -2                          -0 
   410 ARC_NODE_IMPLICATION[15,11]
                                  -2                          -0 
   411 ARC_NODE_IMPLICATION[15,12]
                                  -2                          -0 
   412 ARC_NODE_IMPLICATION[15,14]
                                  -2                          -0 
   413 ARC_NODE_IMPLICATION[15,16]
                                  -2                          -0 
   414 ARC_NODE_IMPLICATION[15,17]
                                  -2                          -0 
   415 ARC_NODE_IMPLICATION[15,18]
                                  -2                          -0 
   416 ARC_NODE_IMPLICATION[15,19]
                                  -2                          -0 
   417 ARC_NODE_IMPLICATION[15,20]
                                  -2                          -0 
   418 ARC_NODE_IMPLICATION[15,21]
                                  -2                          -0 
   419 ARC_NODE_IMPLICATION[15,22]
                                  -2                          -0 
   420 ARC_NODE_IMPLICATION[15,23]
                                  -2                          -0 
   421 ARC_NODE_IMPLICATION[15,24]
                                  -2                          -0 
   422 ARC_NODE_IMPLICATION[15,25]
                                  -2                          -0 
   423 ARC_NODE_IMPLICATION[15,26]
                                  -2                          -0 
   424 ARC_NODE_IMPLICATION[16,2]
                                   0                          -0 
   425 ARC_NODE_IMPLICATION[16,3]
                                  -2                          -0 
   426 ARC_NODE_IMPLICATION[16,4]
                                  -2                          -0 
   427 ARC_NODE_IMPLICATION[16,5]
                                  -2                          -0 
   428 ARC_NODE_IMPLICATION[16,6]
                                  -2                          -0 
   429 ARC_NODE_IMPLICATION[16,7]
                                  -2                          -0 
   430 ARC_NODE_IMPLICATION[16,8]
                                  -2                          -0 
   431 ARC_NODE_IMPLICATION[16,9]
                                  -2                          -0 
   432 ARC_NODE_IMPLICATION[16,10]
                                  -2                          -0 
   433 ARC_NODE_IMPLICATION[16,11]
                                  -2                          -0 
   434 ARC_NODE_IMPLICATION[16,12]
                                  -2                          -0 
   435 ARC_NODE_IMPLICATION[16,13]
                                  -2                          -0 
   436 ARC_NODE_IMPLICATION[16,14]
                                  -2                          -0 
   437 ARC_NODE_IMPLICATION[16,15]
                                  -2                          -0 
   438 ARC_NODE_IMPLICATION[16,17]
                                  -2                          -0 
   439 ARC_NODE_IMPLICATION[16,18]
                                  -2                          -0 
   440 ARC_NODE_IMPLICATION[16,19]
                                  -2                          -0 
   441 ARC_NODE_IMPLICATION[16,20]
                                  -2                          -0 
   442 ARC_NODE_IMPLICATION[16,21]
                                  -2                          -0 
   443 ARC_NODE_IMPLICATION[16,22]
                                  -2                          -0 
   444 ARC_NODE_IMPLICATION[16,23]
                                  -2                          -0 
   445 ARC_NODE_IMPLICATION[16,24]
                                  -2                          -0 
   446 ARC_NODE_IMPLICATION[16,25]
                                  -2                          -0 
   447 ARC_NODE_IMPLICATION[16,26]
                                  -2                          -0 
   448 ARC_NODE_IMPLICATION[17,1]
                                  -2                          -0 
   449 ARC_NODE_IMPLICATION[17,2]
                                  -2                          -0 
   450 ARC_NODE_IMPLICATION[17,3]
                                  -2                          -0 
   451 ARC_NODE_IMPLICATION[17,4]
                                  -2                          -0 
   452 ARC_NODE_IMPLICATION[17,5]
                                  -2                          -0 
   453 ARC_NODE_IMPLICATION[17,6]
                                  -2                          -0 
   454 ARC_NODE_IMPLICATION[17,7]
                                  -2                          -0 
   455 ARC_NODE_IMPLICATION[17,8]
                                  -2                          -0 
   456 ARC_NODE_IMPLICATION[17,9]
                                  -2                          -0 
   457 ARC_NODE_IMPLICATION[17,10]
                                  -2                          -0 
   458 ARC_NODE_IMPLICATION[17,11]
                                  -2                          -0 
   459 ARC_NODE_IMPLICATION[17,12]
                                  -2                          -0 
   460 ARC_NODE_IMPLICATION[17,13]
                                  -2                          -0 
   461 ARC_NODE_IMPLICATION[17,14]
                                  -2                          -0 
   462 ARC_NODE_IMPLICATION[17,15]
                                  -2                          -0 
   463 ARC_NODE_IMPLICATION[17,16]
                                  -2                          -0 
   464 ARC_NODE_IMPLICATION[17,18]
                                  -2                          -0 
   465 ARC_NODE_IMPLICATION[17,19]
                                  -2                          -0 
   466 ARC_NODE_IMPLICATION[17,20]
                                  -2                          -0 
   467 ARC_NODE_IMPLICATION[17,21]
                                  -2                          -0 
   468 ARC_NODE_IMPLICATION[17,22]
                                  -2                          -0 
   469 ARC_NODE_IMPLICATION[17,23]
                                  -2                          -0 
   470 ARC_NODE_IMPLICATION[17,24]
                                   0                          -0 
   471 ARC_NODE_IMPLICATION[17,25]
                                  -2                          -0 
   472 ARC_NODE_IMPLICATION[17,26]
                                  -2                          -0 
   473 ARC_NODE_IMPLICATION[18,1]
                                  -2                          -0 
   474 ARC_NODE_IMPLICATION[18,2]
                                  -2                          -0 
   475 ARC_NODE_IMPLICATION[18,3]
                                  -2                          -0 
   476 ARC_NODE_IMPLICATION[18,4]
                                  -2                          -0 
   477 ARC_NODE_IMPLICATION[18,5]
                                  -2                          -0 
   478 ARC_NODE_IMPLICATION[18,6]
                                  -2                          -0 
   479 ARC_NODE_IMPLICATION[18,7]
                                  -2                          -0 
   480 ARC_NODE_IMPLICATION[18,8]
                                  -2                          -0 
   481 ARC_NODE_IMPLICATION[18,9]
                                  -2                          -0 
   482 ARC_NODE_IMPLICATION[18,10]
                                  -2                          -0 
   483 ARC_NODE_IMPLICATION[18,11]
                                  -2                          -0 
   484 ARC_NODE_IMPLICATION[18,12]
                                   0                          -0 
   485 ARC_NODE_IMPLICATION[18,13]
                                  -2                          -0 
   486 ARC_NODE_IMPLICATION[18,14]
                                  -2                          -0 
   487 ARC_NODE_IMPLICATION[18,15]
                                  -2                          -0 
   488 ARC_NODE_IMPLICATION[18,16]
                                  -2                          -0 
   489 ARC_NODE_IMPLICATION[18,17]
                                  -2                          -0 
   490 ARC_NODE_IMPLICATION[18,19]
                                  -2                          -0 
   491 ARC_NODE_IMPLICATION[18,20]
                                  -2                          -0 
   492 ARC_NODE_IMPLICATION[18,21]
                                  -2                          -0 
   493 ARC_NODE_IMPLICATION[18,22]
                                  -2                          -0 
   494 ARC_NODE_IMPLICATION[18,23]
                                  -2                          -0 
   495 ARC_NODE_IMPLICATION[18,24]
                                  -2                          -0 
   496 ARC_NODE_IMPLICATION[18,25]
                                  -2                          -0 
   497 ARC_NODE_IMPLICATION[18,26]
                                  -2                          -0 
   498 ARC_NODE_IMPLICATION[19,2]
                                  -2                          -0 
   499 ARC_NODE_IMPLICATION[19,3]
                                  -2                          -0 
   500 ARC_NODE_IMPLICATION[19,4]
                                  -2                          -0 
   501 ARC_NODE_IMPLICATION[19,5]
                                   0                          -0 
   502 ARC_NODE_IMPLICATION[19,6]
                                  -2                          -0 
   503 ARC_NODE_IMPLICATION[19,7]
                                  -2                          -0 
   504 ARC_NODE_IMPLICATION[19,8]
                                  -2                          -0 
   505 ARC_NODE_IMPLICATION[19,9]
                                  -2                          -0 
   506 ARC_NODE_IMPLICATION[19,10]
                                  -2                          -0 
   507 ARC_NODE_IMPLICATION[19,12]
                                  -2                          -0 
   508 ARC_NODE_IMPLICATION[19,13]
                                  -2                          -0 
   509 ARC_NODE_IMPLICATION[19,14]
                                  -2                          -0 
   510 ARC_NODE_IMPLICATION[19,15]
                                  -2                          -0 
   511 ARC_NODE_IMPLICATION[19,16]
                                  -2                          -0 
   512 ARC_NODE_IMPLICATION[19,17]
                                  -2                          -0 
   513 ARC_NODE_IMPLICATION[19,18]
                                  -2                          -0 
   514 ARC_NODE_IMPLICATION[19,20]
                                  -2                          -0 
   515 ARC_NODE_IMPLICATION[19,21]
                                  -2                          -0 
   516 ARC_NODE_IMPLICATION[19,22]
                                  -2                          -0 
   517 ARC_NODE_IMPLICATION[19,23]
                                  -2                          -0 
   518 ARC_NODE_IMPLICATION[19,24]
                                  -2                          -0 
   519 ARC_NODE_IMPLICATION[19,25]
                                  -2                          -0 
   520 ARC_NODE_IMPLICATION[19,26]
                                  -2                          -0 
   521 ARC_NODE_IMPLICATION[20,1]
                                  -2                          -0 
   522 ARC_NODE_IMPLICATION[20,2]
                                  -2                          -0 
   523 ARC_NODE_IMPLICATION[20,3]
                                  -2                          -0 
   524 ARC_NODE_IMPLICATION[20,4]
                                  -2                          -0 
   525 ARC_NODE_IMPLICATION[20,5]
                                  -2                          -0 
   526 ARC_NODE_IMPLICATION[20,6]
                                  -2                          -0 
   527 ARC_NODE_IMPLICATION[20,7]
                                  -2                          -0 
   528 ARC_NODE_IMPLICATION[20,8]
                                   0                          -0 
   529 ARC_NODE_IMPLICATION[20,9]
                                  -2                          -0 
   530 ARC_NODE_IMPLICATION[20,10]
                                  -2                          -0 
   531 ARC_NODE_IMPLICATION[20,11]
                                  -2                          -0 
   532 ARC_NODE_IMPLICATION[20,12]
                                  -2                          -0 
   533 ARC_NODE_IMPLICATION[20,13]
                                  -2                          -0 
   534 ARC_NODE_IMPLICATION[20,14]
                                  -2                          -0 
   535 ARC_NODE_IMPLICATION[20,15]
                                  -2                          -0 
   536 ARC_NODE_IMPLICATION[20,16]
                                  -2                          -0 
   537 ARC_NODE_IMPLICATION[20,17]
                                  -2                          -0 
   538 ARC_NODE_IMPLICATION[20,18]
                                  -2                          -0 
   539 ARC_NODE_IMPLICATION[20,19]
                                  -2                          -0 
   540 ARC_NODE_IMPLICATION[20,21]
                                  -2                          -0 
   541 ARC_NODE_IMPLICATION[20,22]
                                  -2                          -0 
   542 ARC_NODE_IMPLICATION[20,23]
                                  -2                          -0 
   543 ARC_NODE_IMPLICATION[20,24]
                                  -2                          -0 
   544 ARC_NODE_IMPLICATION[20,25]
                                  -2                          -0 
   545 ARC_NODE_IMPLICATION[20,26]
                                  -2                          -0 
   546 ARC_NODE_IMPLICATION[21,1]
                                  -2                          -0 
   547 ARC_NODE_IMPLICATION[21,2]
                                  -2                          -0 
   548 ARC_NODE_IMPLICATION[21,3]
                                  -2                          -0 
   549 ARC_NODE_IMPLICATION[21,4]
                                  -2                          -0 
   550 ARC_NODE_IMPLICATION[21,5]
                                  -2                          -0 
   551 ARC_NODE_IMPLICATION[21,6]
                                  -2                          -0 
   552 ARC_NODE_IMPLICATION[21,7]
                                  -2                          -0 
   553 ARC_NODE_IMPLICATION[21,8]
                                  -2                          -0 
   554 ARC_NODE_IMPLICATION[21,9]
                                  -2                          -0 
   555 ARC_NODE_IMPLICATION[21,10]
                                  -2                          -0 
   556 ARC_NODE_IMPLICATION[21,11]
                                  -2                          -0 
   557 ARC_NODE_IMPLICATION[21,12]
                                  -2                          -0 
   558 ARC_NODE_IMPLICATION[21,13]
                                  -2                          -0 
   559 ARC_NODE_IMPLICATION[21,14]
                                  -2                          -0 
   560 ARC_NODE_IMPLICATION[21,15]
                                  -2                          -0 
   561 ARC_NODE_IMPLICATION[21,16]
                                  -2                          -0 
   562 ARC_NODE_IMPLICATION[21,17]
                                   0                          -0 
   563 ARC_NODE_IMPLICATION[21,18]
                                  -2                          -0 
   564 ARC_NODE_IMPLICATION[21,19]
                                  -2                          -0 
   565 ARC_NODE_IMPLICATION[21,20]
                                  -2                          -0 
   566 ARC_NODE_IMPLICATION[21,22]
                                  -2                          -0 
   567 ARC_NODE_IMPLICATION[21,23]
                                  -2                          -0 
   568 ARC_NODE_IMPLICATION[21,24]
                                  -2                          -0 
   569 ARC_NODE_IMPLICATION[21,25]
                                  -2                          -0 
   570 ARC_NODE_IMPLICATION[21,26]
                                  -2                          -0 
   571 ARC_NODE_IMPLICATION[22,1]
                                  -2                          -0 
   572 ARC_NODE_IMPLICATION[22,2]
                                  -2                          -0 
   573 ARC_NODE_IMPLICATION[22,3]
                                  -2                          -0 
   574 ARC_NODE_IMPLICATION[22,4]
                                  -2                          -0 
   575 ARC_NODE_IMPLICATION[22,5]
                                  -2                          -0 
   576 ARC_NODE_IMPLICATION[22,6]
                                  -2                          -0 
   577 ARC_NODE_IMPLICATION[22,7]
                                  -2                          -0 
   578 ARC_NODE_IMPLICATION[22,8]
                                  -2                          -0 
   579 ARC_NODE_IMPLICATION[22,9]
                                  -2                          -0 
   580 ARC_NODE_IMPLICATION[22,10]
                                  -2                          -0 
   581 ARC_NODE_IMPLICATION[22,11]
                                  -2                          -0 
   582 ARC_NODE_IMPLICATION[22,12]
                                  -2                          -0 
   583 ARC_NODE_IMPLICATION[22,13]
                                  -2                          -0 
   584 ARC_NODE_IMPLICATION[22,14]
                                  -2                          -0 
   585 ARC_NODE_IMPLICATION[22,15]
                                  -2                          -0 
   586 ARC_NODE_IMPLICATION[22,16]
                                  -2                          -0 
   587 ARC_NODE_IMPLICATION[22,18]
                                  -2                          -0 
   588 ARC_NODE_IMPLICATION[22,19]
                                  -2                          -0 
   589 ARC_NODE_IMPLICATION[22,20]
                                  -2                          -0 
   590 ARC_NODE_IMPLICATION[22,21]
                                  -2                          -0 
   591 ARC_NODE_IMPLICATION[22,23]
                                   0                          -0 
   592 ARC_NODE_IMPLICATION[22,24]
                                  -2                          -0 
   593 ARC_NODE_IMPLICATION[22,25]
                                  -2                          -0 
   594 ARC_NODE_IMPLICATION[22,26]
                                  -2                          -0 
   595 ARC_NODE_IMPLICATION[23,1]
                                  -2                          -0 
   596 ARC_NODE_IMPLICATION[23,2]
                                  -2                          -0 
   597 ARC_NODE_IMPLICATION[23,3]
                                  -2                          -0 
   598 ARC_NODE_IMPLICATION[23,4]
                                  -2                          -0 
   599 ARC_NODE_IMPLICATION[23,5]
                                  -2                          -0 
   600 ARC_NODE_IMPLICATION[23,6]
                                  -2                          -0 
   601 ARC_NODE_IMPLICATION[23,7]
                                  -2                          -0 
   602 ARC_NODE_IMPLICATION[23,8]
                                  -2                          -0 
   603 ARC_NODE_IMPLICATION[23,9]
                                  -2                          -0 
   604 ARC_NODE_IMPLICATION[23,10]
                                  -2                          -0 
   605 ARC_NODE_IMPLICATION[23,11]
                                  -2                          -0 
   606 ARC_NODE_IMPLICATION[23,12]
                                  -2                          -0 
   607 ARC_NODE_IMPLICATION[23,13]
                                  -2                          -0 
   608 ARC_NODE_IMPLICATION[23,14]
                                  -2                          -0 
   609 ARC_NODE_IMPLICATION[23,15]
                                  -2                          -0 
   610 ARC_NODE_IMPLICATION[23,16]
                                  -2                          -0 
   611 ARC_NODE_IMPLICATION[23,17]
                                  -2                          -0 
   612 ARC_NODE_IMPLICATION[23,18]
                                  -2                          -0 
   613 ARC_NODE_IMPLICATION[23,19]
                                  -2                          -0 
   614 ARC_NODE_IMPLICATION[23,20]
                                  -2                          -0 
   615 ARC_NODE_IMPLICATION[23,21]
                                  -2                          -0 
   616 ARC_NODE_IMPLICATION[23,22]
                                  -2                          -0 
   617 ARC_NODE_IMPLICATION[23,24]
                                  -2                          -0 
   618 ARC_NODE_IMPLICATION[23,25]
                                  -2                          -0 
   619 ARC_NODE_IMPLICATION[23,26]
                                   0                          -0 
   620 ARC_NODE_IMPLICATION[24,1]
                                  -2                          -0 
   621 ARC_NODE_IMPLICATION[24,2]
                                  -2                          -0 
   622 ARC_NODE_IMPLICATION[24,3]
                                  -2                          -0 
   623 ARC_NODE_IMPLICATION[24,4]
                                  -2                          -0 
   624 ARC_NODE_IMPLICATION[24,5]
                                  -2                          -0 
   625 ARC_NODE_IMPLICATION[24,6]
                                  -2                          -0 
   626 ARC_NODE_IMPLICATION[24,7]
                                  -2                          -0 
   627 ARC_NODE_IMPLICATION[24,8]
                                  -2                          -0 
   628 ARC_NODE_IMPLICATION[24,9]
                                  -2                          -0 
   629 ARC_NODE_IMPLICATION[24,10]
                                  -2                          -0 
   630 ARC_NODE_IMPLICATION[24,11]
                                  -2                          -0 
   631 ARC_NODE_IMPLICATION[24,12]
                                  -2                          -0 
   632 ARC_NODE_IMPLICATION[24,13]
                                  -2                          -0 
   633 ARC_NODE_IMPLICATION[24,14]
                                  -2                          -0 
   634 ARC_NODE_IMPLICATION[24,15]
                                  -2                          -0 
   635 ARC_NODE_IMPLICATION[24,16]
                                  -2                          -0 
   636 ARC_NODE_IMPLICATION[24,17]
                                  -2                          -0 
   637 ARC_NODE_IMPLICATION[24,18]
                                  -2                          -0 
   638 ARC_NODE_IMPLICATION[24,19]
                                  -2                          -0 
   639 ARC_NODE_IMPLICATION[24,20]
                                  -2                          -0 
   640 ARC_NODE_IMPLICATION[24,21]
                                  -2                          -0 
   641 ARC_NODE_IMPLICATION[24,22]
                                  -2                          -0 
   642 ARC_NODE_IMPLICATION[24,23]
                                  -2                          -0 
   643 ARC_NODE_IMPLICATION[24,25]
                                   0                          -0 
   644 ARC_NODE_IMPLICATION[24,26]
                                  -2                          -0 
   645 ARC_NODE_IMPLICATION[25,1]
                                  -2                          -0 
   646 ARC_NODE_IMPLICATION[25,2]
                                  -2                          -0 
   647 ARC_NODE_IMPLICATION[25,3]
                                  -2                          -0 
   648 ARC_NODE_IMPLICATION[25,4]
                                  -2                          -0 
   649 ARC_NODE_IMPLICATION[25,6]
                                  -2                          -0 
   650 ARC_NODE_IMPLICATION[25,7]
                                  -2                          -0 
   651 ARC_NODE_IMPLICATION[25,8]
                                  -2                          -0 
   652 ARC_NODE_IMPLICATION[25,9]
                                  -2                          -0 
   653 ARC_NODE_IMPLICATION[25,10]
                                  -2                          -0 
   654 ARC_NODE_IMPLICATION[25,11]
                                  -2                          -0 
   655 ARC_NODE_IMPLICATION[25,12]
                                  -2                          -0 
   656 ARC_NODE_IMPLICATION[25,13]
                                  -2                          -0 
   657 ARC_NODE_IMPLICATION[25,14]
                                  -2                          -0 
   658 ARC_NODE_IMPLICATION[25,15]
                                  -2                          -0 
   659 ARC_NODE_IMPLICATION[25,16]
                                  -2                          -0 
   660 ARC_NODE_IMPLICATION[25,17]
                                  -2                          -0 
   661 ARC_NODE_IMPLICATION[25,18]
                                   0                          -0 
   662 ARC_NODE_IMPLICATION[25,19]
                                  -2                          -0 
   663 ARC_NODE_IMPLICATION[25,20]
                                  -2                          -0 
   664 ARC_NODE_IMPLICATION[25,21]
                                  -2                          -0 
   665 ARC_NODE_IMPLICATION[25,22]
                                  -2                          -0 
   666 ARC_NODE_IMPLICATION[25,23]
                                  -2                          -0 
   667 ARC_NODE_IMPLICATION[25,24]
                                  -2                          -0 
   668 ARC_NODE_IMPLICATION[25,26]
                                  -2                          -0 
   669 SINGLE_ENTRY[1]
                                   1             1             = 
   670 SINGLE_ENTRY[2]
                                   1             1             = 
   671 SINGLE_ENTRY[3]
                                   1             1             = 
   672 SINGLE_ENTRY[4]
                                   1             1             = 
   673 SINGLE_ENTRY[5]
                                   1             1             = 
   674 SINGLE_ENTRY[6]
                                   1             1             = 
   675 SINGLE_ENTRY[7]
                                   1             1             = 
   676 SINGLE_ENTRY[8]
                                   1             1             = 
   677 SINGLE_ENTRY[9]
                                   1             1             = 
   678 SINGLE_ENTRY[10]
                                   1             1             = 
   679 SINGLE_ENTRY[11]
                                   1             1             = 
   680 SINGLE_ENTRY[12]
                                   1             1             = 
   681 SINGLE_ENTRY[13]
                                   1             1             = 
   682 SINGLE_ENTRY[14]
                                   1             1             = 
   683 SINGLE_ENTRY[15]
                                   1             1             = 
   684 SINGLE_ENTRY[16]
                                   1             1             = 
   685 SINGLE_ENTRY[17]
                                   1             1             = 
   686 SINGLE_ENTRY[18]
                                   1             1             = 
   687 SINGLE_ENTRY[19]
                                   1             1             = 
   688 SINGLE_ENTRY[20]
                                   1             1             = 
   689 SINGLE_ENTRY[21]
                                   1             1             = 
   690 SINGLE_ENTRY[22]
                                   1             1             = 
   691 SINGLE_ENTRY[23]
                                   1             1             = 
   692 SINGLE_ENTRY[24]
                                   1             1             = 
   693 SINGLE_ENTRY[25]
                                   1             1             = 
   694 SINGLE_ENTRY[26]
                                   1             1             = 
   695 SINGLE_EXIT[0]
                                   1             1             = 
   696 SINGLE_EXIT[1]
                                   1             1             = 
   697 SINGLE_EXIT[2]
                                   1             1             = 
   698 SINGLE_EXIT[3]
                                   1             1             = 
   699 SINGLE_EXIT[4]
                                   1             1             = 
   700 SINGLE_EXIT[5]
                                   1             1             = 
   701 SINGLE_EXIT[6]
                                   1             1             = 
   702 SINGLE_EXIT[7]
                                   1             1             = 
   703 SINGLE_EXIT[8]
                                   1             1             = 
   704 SINGLE_EXIT[9]
                                   1             1             = 
   705 SINGLE_EXIT[10]
                                   1             1             = 
   706 SINGLE_EXIT[11]
                                   1             1             = 
   707 SINGLE_EXIT[12]
                                   1             1             = 
   708 SINGLE_EXIT[13]
                                   1             1             = 
   709 SINGLE_EXIT[14]
                                   1             1             = 
   710 SINGLE_EXIT[15]
                                   1             1             = 
   711 SINGLE_EXIT[16]
                                   1             1             = 
   712 SINGLE_EXIT[17]
                                   1             1             = 
   713 SINGLE_EXIT[18]
                                   1             1             = 
   714 SINGLE_EXIT[19]
                                   1             1             = 
   715 SINGLE_EXIT[20]
                                   1             1             = 
   716 SINGLE_EXIT[21]
                                   1             1             = 
   717 SINGLE_EXIT[22]
                                   1             1             = 
   718 SINGLE_EXIT[23]
                                   1             1             = 
   719 SINGLE_EXIT[24]
                                   1             1             = 
   720 SINGLE_EXIT[25]
                                   1             1             = 
   721 PRECEDE[1,0]                1            -0               
   722 PRECEDE[2,0]                8            -0               
   723 PRECEDE[3,0]               22            -0               
   724 PRECEDE[4,0]                5            -0               
   725 PRECEDE[5,0]               11            -0               
   726 PRECEDE[6,0]               14            -0               
   727 PRECEDE[6,1]               13            -0               
   728 PRECEDE[7,0]               12            -0               
   729 PRECEDE[8,0]                4            -0               
   730 PRECEDE[9,0]               23            -0               
   731 PRECEDE[10,0]
                                   9            -0               
   732 PRECEDE[11,0]
                                   6            -0               
   733 PRECEDE[11,1]
                                   5            -0               
   734 PRECEDE[12,0]
                                  20            -0               
   735 PRECEDE[12,1]
                                  19            -0               
   736 PRECEDE[12,6]
                                   6            -0               
   737 PRECEDE[12,8]
                                  16            -0               
   738 PRECEDE[13,0]
                                   2            -0               
   739 PRECEDE[14,0]
                                  13            -0               
   740 PRECEDE[15,0]
                                  21            -0               
   741 PRECEDE[15,13]
                                  19            -0               
   742 PRECEDE[16,0]
                                   7            -0               
   743 PRECEDE[16,1]
                                   6            -0               
   744 PRECEDE[17,0]
                                  16            -0               
   745 PRECEDE[18,0]
                                  19            -0               
   746 PRECEDE[19,0]
                                  10            -0               
   747 PRECEDE[19,1]
                                   9            -0               
   748 PRECEDE[19,11]
                                   4            -0               
   749 PRECEDE[20,0]
                                   3            -0               
   750 PRECEDE[21,0]
                                  15            -0               
   751 PRECEDE[22,0]
                                  24            -0               
   752 PRECEDE[22,17]
                                   8            -0               
   753 PRECEDE[23,0]
                                  25            -0               
   754 PRECEDE[24,0]
                                  17            -0               
   755 PRECEDE[25,0]
                                  18            -0               
   756 PRECEDE[25,5]
                                   7            -0               
   757 PRECEDE[26,0]
                                  26            -0               
   758 PRECEDE[26,1]
                                  25            -0               
   759 PRECEDE[26,2]
                                  18            -0               
   760 PRECEDE[26,3]
                                   4            -0               
   761 PRECEDE[26,4]
                                  21            -0               
   762 PRECEDE[26,5]
                                  15            -0               
   763 PRECEDE[26,6]
                                  12            -0               
   764 PRECEDE[26,7]
                                  14            -0               
   765 PRECEDE[26,8]
                                  22            -0               
   766 PRECEDE[26,9]
                                   3            -0               
   767 PRECEDE[26,10]
                                  17            -0               
   768 PRECEDE[26,11]
                                  20            -0               
   769 PRECEDE[26,12]
                                   6            -0               
   770 PRECEDE[26,13]
                                  24            -0               
   771 PRECEDE[26,14]
                                  13            -0               
   772 PRECEDE[26,15]
                                   5            -0               
   773 PRECEDE[26,16]
                                  19            -0               
   774 PRECEDE[26,17]
                                  10            -0               
   775 PRECEDE[26,18]
                                   7            -0               
   776 PRECEDE[26,19]
                                  16            -0               
   777 PRECEDE[26,20]
                                  23            -0               
   778 PRECEDE[26,21]
                                  11            -0               
   779 PRECEDE[26,22]
                                   2            -0               
   780 PRECEDE[26,23]
                                   1            -0               
   781 PRECEDE[26,24]
                                   9            -0               
   782 PRECEDE[26,25]
                                   8            -0               
   783 FIRST_NODE_INDEX
                                   1             1             = 
   784 INDEX_ARC_IMPLICATION[1,2]
                                  -7                          26 
   785 INDEX_ARC_IMPLICATION[1,3]
                                 -21                          26 
   786 INDEX_ARC_IMPLICATION[1,4]
                                  -4                          26 
   787 INDEX_ARC_IMPLICATION[1,5]
                                 -10                          26 
   788 INDEX_ARC_IMPLICATION[1,6]
                                 -13                          26 
   789 INDEX_ARC_IMPLICATION[1,7]
                                 -11                          26 
   790 INDEX_ARC_IMPLICATION[1,8]
                                  -3                          26 
   791 INDEX_ARC_IMPLICATION[1,9]
                                 -22                          26 
   792 INDEX_ARC_IMPLICATION[1,10]
                                  -8                          26 
   793 INDEX_ARC_IMPLICATION[1,11]
                                  -5                          26 
   794 INDEX_ARC_IMPLICATION[1,12]
                                 -19                          26 
   795 INDEX_ARC_IMPLICATION[1,13]
                                  26                          26 
   796 INDEX_ARC_IMPLICATION[1,14]
                                 -12                          26 
   797 INDEX_ARC_IMPLICATION[1,15]
                                 -20                          26 
   798 INDEX_ARC_IMPLICATION[1,16]
                                  -6                          26 
   799 INDEX_ARC_IMPLICATION[1,17]
                                 -15                          26 
   800 INDEX_ARC_IMPLICATION[1,18]
                                 -18                          26 
   801 INDEX_ARC_IMPLICATION[1,19]
                                  -9                          26 
   802 INDEX_ARC_IMPLICATION[1,20]
                                  -2                          26 
   803 INDEX_ARC_IMPLICATION[1,21]
                                 -14                          26 
   804 INDEX_ARC_IMPLICATION[1,22]
                                 -23                          26 
   805 INDEX_ARC_IMPLICATION[1,23]
                                 -24                          26 
   806 INDEX_ARC_IMPLICATION[1,24]
                                 -16                          26 
   807 INDEX_ARC_IMPLICATION[1,25]
                                 -17                          26 
   808 INDEX_ARC_IMPLICATION[1,26]
                                 -25                          26 
   809 INDEX_ARC_IMPLICATION[2,1]
                                   7                          26 
   810 INDEX_ARC_IMPLICATION[2,3]
                                 -14                          26 
   811 INDEX_ARC_IMPLICATION[2,4]
                                   3                          26 
   812 INDEX_ARC_IMPLICATION[2,5]
                                  -3                          26 
   813 INDEX_ARC_IMPLICATION[2,6]
                                  -6                          26 
   814 INDEX_ARC_IMPLICATION[2,7]
                                  -4                          26 
   815 INDEX_ARC_IMPLICATION[2,8]
                                   4                          26 
   816 INDEX_ARC_IMPLICATION[2,9]
                                 -15                          26 
   817 INDEX_ARC_IMPLICATION[2,10]
                                  26                          26 
   818 INDEX_ARC_IMPLICATION[2,11]
                                   2                          26 
   819 INDEX_ARC_IMPLICATION[2,12]
                                 -12                          26 
   820 INDEX_ARC_IMPLICATION[2,13]
                                   6                          26 
   821 INDEX_ARC_IMPLICATION[2,14]
                                  -5                          26 
   822 INDEX_ARC_IMPLICATION[2,15]
                                 -13                          26 
   823 INDEX_ARC_IMPLICATION[2,16]
                                   1                          26 
   824 INDEX_ARC_IMPLICATION[2,17]
                                  -8                          26 
   825 INDEX_ARC_IMPLICATION[2,18]
                                 -11                          26 
   826 INDEX_ARC_IMPLICATION[2,19]
                                  -2                          26 
   827 INDEX_ARC_IMPLICATION[2,20]
                                   5                          26 
   828 INDEX_ARC_IMPLICATION[2,21]
                                  -7                          26 
   829 INDEX_ARC_IMPLICATION[2,22]
                                 -16                          26 
   830 INDEX_ARC_IMPLICATION[2,23]
                                 -17                          26 
   831 INDEX_ARC_IMPLICATION[2,24]
                                  -9                          26 
   832 INDEX_ARC_IMPLICATION[2,25]
                                 -10                          26 
   833 INDEX_ARC_IMPLICATION[2,26]
                                 -18                          26 
   834 INDEX_ARC_IMPLICATION[3,1]
                                  21                          26 
   835 INDEX_ARC_IMPLICATION[3,2]
                                  14                          26 
   836 INDEX_ARC_IMPLICATION[3,4]
                                  17                          26 
   837 INDEX_ARC_IMPLICATION[3,5]
                                  11                          26 
   838 INDEX_ARC_IMPLICATION[3,6]
                                   8                          26 
   839 INDEX_ARC_IMPLICATION[3,7]
                                  10                          26 
   840 INDEX_ARC_IMPLICATION[3,8]
                                  18                          26 
   841 INDEX_ARC_IMPLICATION[3,9]
                                  26                          26 
   842 INDEX_ARC_IMPLICATION[3,10]
                                  13                          26 
   843 INDEX_ARC_IMPLICATION[3,11]
                                  16                          26 
   844 INDEX_ARC_IMPLICATION[3,12]
                                   2                          26 
   845 INDEX_ARC_IMPLICATION[3,13]
                                  20                          26 
   846 INDEX_ARC_IMPLICATION[3,14]
                                   9                          26 
   847 INDEX_ARC_IMPLICATION[3,15]
                                   1                          26 
   848 INDEX_ARC_IMPLICATION[3,16]
                                  15                          26 
   849 INDEX_ARC_IMPLICATION[3,17]
                                   6                          26 
   850 INDEX_ARC_IMPLICATION[3,18]
                                   3                          26 
   851 INDEX_ARC_IMPLICATION[3,19]
                                  12                          26 
   852 INDEX_ARC_IMPLICATION[3,20]
                                  19                          26 
   853 INDEX_ARC_IMPLICATION[3,21]
                                   7                          26 
   854 INDEX_ARC_IMPLICATION[3,22]
                                  -2                          26 
   855 INDEX_ARC_IMPLICATION[3,23]
                                  -3                          26 
   856 INDEX_ARC_IMPLICATION[3,24]
                                   5                          26 
   857 INDEX_ARC_IMPLICATION[3,25]
                                   4                          26 
   858 INDEX_ARC_IMPLICATION[3,26]
                                  -4                          26 
   859 INDEX_ARC_IMPLICATION[4,1]
                                   4                          26 
   860 INDEX_ARC_IMPLICATION[4,2]
                                  -3                          26 
   861 INDEX_ARC_IMPLICATION[4,3]
                                 -17                          26 
   862 INDEX_ARC_IMPLICATION[4,5]
                                  -6                          26 
   863 INDEX_ARC_IMPLICATION[4,6]
                                  -9                          26 
   864 INDEX_ARC_IMPLICATION[4,7]
                                  -7                          26 
   865 INDEX_ARC_IMPLICATION[4,8]
                                   1                          26 
   866 INDEX_ARC_IMPLICATION[4,9]
                                 -18                          26 
   867 INDEX_ARC_IMPLICATION[4,10]
                                  -4                          26 
   868 INDEX_ARC_IMPLICATION[4,11]
                                  26                          26 
   869 INDEX_ARC_IMPLICATION[4,12]
                                 -15                          26 
   870 INDEX_ARC_IMPLICATION[4,13]
                                   3                          26 
   871 INDEX_ARC_IMPLICATION[4,14]
                                  -8                          26 
   872 INDEX_ARC_IMPLICATION[4,15]
                                 -16                          26 
   873 INDEX_ARC_IMPLICATION[4,16]
                                  -2                          26 
   874 INDEX_ARC_IMPLICATION[4,17]
                                 -11                          26 
   875 INDEX_ARC_IMPLICATION[4,18]
                                 -14                          26 
   876 INDEX_ARC_IMPLICATION[4,19]
                                  -5                          26 
   877 INDEX_ARC_IMPLICATION[4,20]
                                   2                          26 
   878 INDEX_ARC_IMPLICATION[4,21]
                                 -10                          26 
   879 INDEX_ARC_IMPLICATION[4,22]
                                 -19                          26 
   880 INDEX_ARC_IMPLICATION[4,23]
                                 -20                          26 
   881 INDEX_ARC_IMPLICATION[4,24]
                                 -12                          26 
   882 INDEX_ARC_IMPLICATION[4,25]
                                 -13                          26 
   883 INDEX_ARC_IMPLICATION[4,26]
                                 -21                          26 
   884 INDEX_ARC_IMPLICATION[5,1]
                                  10                          26 
   885 INDEX_ARC_IMPLICATION[5,2]
                                   3                          26 
   886 INDEX_ARC_IMPLICATION[5,3]
                                 -11                          26 
   887 INDEX_ARC_IMPLICATION[5,4]
                                   6                          26 
   888 INDEX_ARC_IMPLICATION[5,6]
                                  -3                          26 
   889 INDEX_ARC_IMPLICATION[5,7]
                                  26                          26 
   890 INDEX_ARC_IMPLICATION[5,8]
                                   7                          26 
   891 INDEX_ARC_IMPLICATION[5,9]
                                 -12                          26 
   892 INDEX_ARC_IMPLICATION[5,10]
                                   2                          26 
   893 INDEX_ARC_IMPLICATION[5,11]
                                   5                          26 
   894 INDEX_ARC_IMPLICATION[5,12]
                                  -9                          26 
   895 INDEX_ARC_IMPLICATION[5,13]
                                   9                          26 
   896 INDEX_ARC_IMPLICATION[5,14]
                                  -2                          26 
   897 INDEX_ARC_IMPLICATION[5,15]
                                 -10                          26 
   898 INDEX_ARC_IMPLICATION[5,16]
                                   4                          26 
   899 INDEX_ARC_IMPLICATION[5,17]
                                  -5                          26 
   900 INDEX_ARC_IMPLICATION[5,18]
                                  -8                          26 
   901 INDEX_ARC_IMPLICATION[5,19]
                                   1                          26 
   902 INDEX_ARC_IMPLICATION[5,20]
                                   8                          26 
   903 INDEX_ARC_IMPLICATION[5,21]
                                  -4                          26 
   904 INDEX_ARC_IMPLICATION[5,22]
                                 -13                          26 
   905 INDEX_ARC_IMPLICATION[5,23]
                                 -14                          26 
   906 INDEX_ARC_IMPLICATION[5,24]
                                  -6                          26 
   907 INDEX_ARC_IMPLICATION[5,25]
                                  -7                          26 
   908 INDEX_ARC_IMPLICATION[5,26]
                                 -15                          26 
   909 INDEX_ARC_IMPLICATION[6,2]
                                   6                          26 
   910 INDEX_ARC_IMPLICATION[6,3]
                                  -8                          26 
   911 INDEX_ARC_IMPLICATION[6,4]
                                   9                          26 
   912 INDEX_ARC_IMPLICATION[6,5]
                                   3                          26 
   913 INDEX_ARC_IMPLICATION[6,7]
                                   2                          26 
   914 INDEX_ARC_IMPLICATION[6,8]
                                  10                          26 
   915 INDEX_ARC_IMPLICATION[6,9]
                                  -9                          26 
   916 INDEX_ARC_IMPLICATION[6,10]
                                   5                          26 
   917 INDEX_ARC_IMPLICATION[6,11]
                                   8                          26 
   918 INDEX_ARC_IMPLICATION[6,12]
                                  -6                          26 
   919 INDEX_ARC_IMPLICATION[6,13]
                                  12                          26 
   920 INDEX_ARC_IMPLICATION[6,14]
                                   1                          26 
   921 INDEX_ARC_IMPLICATION[6,15]
                                  -7                          26 
   922 INDEX_ARC_IMPLICATION[6,16]
                                   7                          26 
   923 INDEX_ARC_IMPLICATION[6,17]
                                  -2                          26 
   924 INDEX_ARC_IMPLICATION[6,18]
                                  -5                          26 
   925 INDEX_ARC_IMPLICATION[6,19]
                                   4                          26 
   926 INDEX_ARC_IMPLICATION[6,20]
                                  11                          26 
   927 INDEX_ARC_IMPLICATION[6,21]
                                  26                          26 
   928 INDEX_ARC_IMPLICATION[6,22]
                                 -10                          26 
   929 INDEX_ARC_IMPLICATION[6,23]
                                 -11                          26 
   930 INDEX_ARC_IMPLICATION[6,24]
                                  -3                          26 
   931 INDEX_ARC_IMPLICATION[6,25]
                                  -4                          26 
   932 INDEX_ARC_IMPLICATION[6,26]
                                 -12                          26 
   933 INDEX_ARC_IMPLICATION[7,1]
                                  11                          26 
   934 INDEX_ARC_IMPLICATION[7,2]
                                   4                          26 
   935 INDEX_ARC_IMPLICATION[7,3]
                                 -10                          26 
   936 INDEX_ARC_IMPLICATION[7,4]
                                   7                          26 
   937 INDEX_ARC_IMPLICATION[7,5]
                                   1                          26 
   938 INDEX_ARC_IMPLICATION[7,6]
                                  -2                          26 
   939 INDEX_ARC_IMPLICATION[7,8]
                                   8                          26 
   940 INDEX_ARC_IMPLICATION[7,9]
                                 -11                          26 
   941 INDEX_ARC_IMPLICATION[7,10]
                                   3                          26 
   942 INDEX_ARC_IMPLICATION[7,11]
                                   6                          26 
   943 INDEX_ARC_IMPLICATION[7,12]
                                  -8                          26 
   944 INDEX_ARC_IMPLICATION[7,13]
                                  10                          26 
   945 INDEX_ARC_IMPLICATION[7,14]
                                  26                          26 
   946 INDEX_ARC_IMPLICATION[7,15]
                                  -9                          26 
   947 INDEX_ARC_IMPLICATION[7,16]
                                   5                          26 
   948 INDEX_ARC_IMPLICATION[7,17]
                                  -4                          26 
   949 INDEX_ARC_IMPLICATION[7,18]
                                  -7                          26 
   950 INDEX_ARC_IMPLICATION[7,19]
                                   2                          26 
   951 INDEX_ARC_IMPLICATION[7,20]
                                   9                          26 
   952 INDEX_ARC_IMPLICATION[7,21]
                                  -3                          26 
   953 INDEX_ARC_IMPLICATION[7,22]
                                 -12                          26 
   954 INDEX_ARC_IMPLICATION[7,23]
                                 -13                          26 
   955 INDEX_ARC_IMPLICATION[7,24]
                                  -5                          26 
   956 INDEX_ARC_IMPLICATION[7,25]
                                  -6                          26 
   957 INDEX_ARC_IMPLICATION[7,26]
                                 -14                          26 
   958 INDEX_ARC_IMPLICATION[8,1]
                                   3                          26 
   959 INDEX_ARC_IMPLICATION[8,2]
                                  -4                          26 
   960 INDEX_ARC_IMPLICATION[8,3]
                                 -18                          26 
   961 INDEX_ARC_IMPLICATION[8,4]
                                  26                          26 
   962 INDEX_ARC_IMPLICATION[8,5]
                                  -7                          26 
   963 INDEX_ARC_IMPLICATION[8,6]
                                 -10                          26 
   964 INDEX_ARC_IMPLICATION[8,7]
                                  -8                          26 
   965 INDEX_ARC_IMPLICATION[8,9]
                                 -19                          26 
   966 INDEX_ARC_IMPLICATION[8,10]
                                  -5                          26 
   967 INDEX_ARC_IMPLICATION[8,11]
                                  -2                          26 
   968 INDEX_ARC_IMPLICATION[8,12]
                                 -16                          26 
   969 INDEX_ARC_IMPLICATION[8,13]
                                   2                          26 
   970 INDEX_ARC_IMPLICATION[8,14]
                                  -9                          26 
   971 INDEX_ARC_IMPLICATION[8,15]
                                 -17                          26 
   972 INDEX_ARC_IMPLICATION[8,16]
                                  -3                          26 
   973 INDEX_ARC_IMPLICATION[8,17]
                                 -12                          26 
   974 INDEX_ARC_IMPLICATION[8,18]
                                 -15                          26 
   975 INDEX_ARC_IMPLICATION[8,19]
                                  -6                          26 
   976 INDEX_ARC_IMPLICATION[8,20]
                                   1                          26 
   977 INDEX_ARC_IMPLICATION[8,21]
                                 -11                          26 
   978 INDEX_ARC_IMPLICATION[8,22]
                                 -20                          26 
   979 INDEX_ARC_IMPLICATION[8,23]
                                 -21                          26 
   980 INDEX_ARC_IMPLICATION[8,24]
                                 -13                          26 
   981 INDEX_ARC_IMPLICATION[8,25]
                                 -14                          26 
   982 INDEX_ARC_IMPLICATION[8,26]
                                 -22                          26 
   983 INDEX_ARC_IMPLICATION[9,1]
                                  22                          26 
   984 INDEX_ARC_IMPLICATION[9,2]
                                  15                          26 
   985 INDEX_ARC_IMPLICATION[9,3]
                                   1                          26 
   986 INDEX_ARC_IMPLICATION[9,4]
                                  18                          26 
   987 INDEX_ARC_IMPLICATION[9,5]
                                  12                          26 
   988 INDEX_ARC_IMPLICATION[9,6]
                                   9                          26 
   989 INDEX_ARC_IMPLICATION[9,7]
                                  11                          26 
   990 INDEX_ARC_IMPLICATION[9,8]
                                  19                          26 
   991 INDEX_ARC_IMPLICATION[9,10]
                                  14                          26 
   992 INDEX_ARC_IMPLICATION[9,11]
                                  17                          26 
   993 INDEX_ARC_IMPLICATION[9,12]
                                   3                          26 
   994 INDEX_ARC_IMPLICATION[9,13]
                                  21                          26 
   995 INDEX_ARC_IMPLICATION[9,14]
                                  10                          26 
   996 INDEX_ARC_IMPLICATION[9,15]
                                   2                          26 
   997 INDEX_ARC_IMPLICATION[9,16]
                                  16                          26 
   998 INDEX_ARC_IMPLICATION[9,17]
                                   7                          26 
   999 INDEX_ARC_IMPLICATION[9,18]
                                   4                          26 
  1000 INDEX_ARC_IMPLICATION[9,19]
                                  13                          26 
  1001 INDEX_ARC_IMPLICATION[9,20]
                                  20                          26 
  1002 INDEX_ARC_IMPLICATION[9,21]
                                   8                          26 
  1003 INDEX_ARC_IMPLICATION[9,22]
                                  26                          26 
  1004 INDEX_ARC_IMPLICATION[9,23]
                                  -2                          26 
  1005 INDEX_ARC_IMPLICATION[9,24]
                                   6                          26 
  1006 INDEX_ARC_IMPLICATION[9,25]
                                   5                          26 
  1007 INDEX_ARC_IMPLICATION[9,26]
                                  -3                          26 
  1008 INDEX_ARC_IMPLICATION[10,1]
                                   8                          26 
  1009 INDEX_ARC_IMPLICATION[10,2]
                                   1                          26 
  1010 INDEX_ARC_IMPLICATION[10,3]
                                 -13                          26 
  1011 INDEX_ARC_IMPLICATION[10,4]
                                   4                          26 
  1012 INDEX_ARC_IMPLICATION[10,5]
                                  -2                          26 
  1013 INDEX_ARC_IMPLICATION[10,6]
                                  -5                          26 
  1014 INDEX_ARC_IMPLICATION[10,7]
                                  -3                          26 
  1015 INDEX_ARC_IMPLICATION[10,8]
                                   5                          26 
  1016 INDEX_ARC_IMPLICATION[10,9]
                                 -14                          26 
  1017 INDEX_ARC_IMPLICATION[10,11]
                                   3                          26 
  1018 INDEX_ARC_IMPLICATION[10,12]
                                 -11                          26 
  1019 INDEX_ARC_IMPLICATION[10,13]
                                   7                          26 
  1020 INDEX_ARC_IMPLICATION[10,14]
                                  -4                          26 
  1021 INDEX_ARC_IMPLICATION[10,15]
                                 -12                          26 
  1022 INDEX_ARC_IMPLICATION[10,16]
                                   2                          26 
  1023 INDEX_ARC_IMPLICATION[10,17]
                                  -7                          26 
  1024 INDEX_ARC_IMPLICATION[10,18]
                                 -10                          26 
  1025 INDEX_ARC_IMPLICATION[10,19]
                                  26                          26 
  1026 INDEX_ARC_IMPLICATION[10,20]
                                   6                          26 
  1027 INDEX_ARC_IMPLICATION[10,21]
                                  -6                          26 
  1028 INDEX_ARC_IMPLICATION[10,22]
                                 -15                          26 
  1029 INDEX_ARC_IMPLICATION[10,23]
                                 -16                          26 
  1030 INDEX_ARC_IMPLICATION[10,24]
                                  -8                          26 
  1031 INDEX_ARC_IMPLICATION[10,25]
                                  -9                          26 
  1032 INDEX_ARC_IMPLICATION[10,26]
                                 -17                          26 
  1033 INDEX_ARC_IMPLICATION[11,2]
                                  -2                          26 
  1034 INDEX_ARC_IMPLICATION[11,3]
                                 -16                          26 
  1035 INDEX_ARC_IMPLICATION[11,4]
                                   1                          26 
  1036 INDEX_ARC_IMPLICATION[11,5]
                                  -5                          26 
  1037 INDEX_ARC_IMPLICATION[11,6]
                                  -8                          26 
  1038 INDEX_ARC_IMPLICATION[11,7]
                                  -6                          26 
  1039 INDEX_ARC_IMPLICATION[11,8]
                                   2                          26 
  1040 INDEX_ARC_IMPLICATION[11,9]
                                 -17                          26 
  1041 INDEX_ARC_IMPLICATION[11,10]
                                  -3                          26 
  1042 INDEX_ARC_IMPLICATION[11,12]
                                 -14                          26 
  1043 INDEX_ARC_IMPLICATION[11,13]
                                   4                          26 
  1044 INDEX_ARC_IMPLICATION[11,14]
                                  -7                          26 
  1045 INDEX_ARC_IMPLICATION[11,15]
                                 -15                          26 
  1046 INDEX_ARC_IMPLICATION[11,16]
                                  26                          26 
  1047 INDEX_ARC_IMPLICATION[11,17]
                                 -10                          26 
  1048 INDEX_ARC_IMPLICATION[11,18]
                                 -13                          26 
  1049 INDEX_ARC_IMPLICATION[11,19]
                                  -4                          26 
  1050 INDEX_ARC_IMPLICATION[11,20]
                                   3                          26 
  1051 INDEX_ARC_IMPLICATION[11,21]
                                  -9                          26 
  1052 INDEX_ARC_IMPLICATION[11,22]
                                 -18                          26 
  1053 INDEX_ARC_IMPLICATION[11,23]
                                 -19                          26 
  1054 INDEX_ARC_IMPLICATION[11,24]
                                 -11                          26 
  1055 INDEX_ARC_IMPLICATION[11,25]
                                 -12                          26 
  1056 INDEX_ARC_IMPLICATION[11,26]
                                 -20                          26 
  1057 INDEX_ARC_IMPLICATION[12,2]
                                  12                          26 
  1058 INDEX_ARC_IMPLICATION[12,3]
                                  -2                          26 
  1059 INDEX_ARC_IMPLICATION[12,4]
                                  15                          26 
  1060 INDEX_ARC_IMPLICATION[12,5]
                                   9                          26 
  1061 INDEX_ARC_IMPLICATION[12,7]
                                   8                          26 
  1062 INDEX_ARC_IMPLICATION[12,9]
                                  -3                          26 
  1063 INDEX_ARC_IMPLICATION[12,10]
                                  11                          26 
  1064 INDEX_ARC_IMPLICATION[12,11]
                                  14                          26 
  1065 INDEX_ARC_IMPLICATION[12,13]
                                  18                          26 
  1066 INDEX_ARC_IMPLICATION[12,14]
                                   7                          26 
  1067 INDEX_ARC_IMPLICATION[12,15]
                                  26                          26 
  1068 INDEX_ARC_IMPLICATION[12,16]
                                  13                          26 
  1069 INDEX_ARC_IMPLICATION[12,17]
                                   4                          26 
  1070 INDEX_ARC_IMPLICATION[12,18]
                                   1                          26 
  1071 INDEX_ARC_IMPLICATION[12,19]
                                  10                          26 
  1072 INDEX_ARC_IMPLICATION[12,20]
                                  17                          26 
  1073 INDEX_ARC_IMPLICATION[12,21]
                                   5                          26 
  1074 INDEX_ARC_IMPLICATION[12,22]
                                  -4                          26 
  1075 INDEX_ARC_IMPLICATION[12,23]
                                  -5                          26 
  1076 INDEX_ARC_IMPLICATION[12,24]
                                   3                          26 
  1077 INDEX_ARC_IMPLICATION[12,25]
                                   2                          26 
  1078 INDEX_ARC_IMPLICATION[12,26]
                                  -6                          26 
  1079 INDEX_ARC_IMPLICATION[13,1]
                                   1                          26 
  1080 INDEX_ARC_IMPLICATION[13,2]
                                  -6                          26 
  1081 INDEX_ARC_IMPLICATION[13,3]
                                 -20                          26 
  1082 INDEX_ARC_IMPLICATION[13,4]
                                  -3                          26 
  1083 INDEX_ARC_IMPLICATION[13,5]
                                  -9                          26 
  1084 INDEX_ARC_IMPLICATION[13,6]
                                 -12                          26 
  1085 INDEX_ARC_IMPLICATION[13,7]
                                 -10                          26 
  1086 INDEX_ARC_IMPLICATION[13,8]
                                  -2                          26 
  1087 INDEX_ARC_IMPLICATION[13,9]
                                 -21                          26 
  1088 INDEX_ARC_IMPLICATION[13,10]
                                  -7                          26 
  1089 INDEX_ARC_IMPLICATION[13,11]
                                  -4                          26 
  1090 INDEX_ARC_IMPLICATION[13,12]
                                 -18                          26 
  1091 INDEX_ARC_IMPLICATION[13,14]
                                 -11                          26 
  1092 INDEX_ARC_IMPLICATION[13,15]
                                 -19                          26 
  1093 INDEX_ARC_IMPLICATION[13,16]
                                  -5                          26 
  1094 INDEX_ARC_IMPLICATION[13,17]
                                 -14                          26 
  1095 INDEX_ARC_IMPLICATION[13,18]
                                 -17                          26 
  1096 INDEX_ARC_IMPLICATION[13,19]
                                  -8                          26 
  1097 INDEX_ARC_IMPLICATION[13,20]
                                  26                          26 
  1098 INDEX_ARC_IMPLICATION[13,21]
                                 -13                          26 
  1099 INDEX_ARC_IMPLICATION[13,22]
                                 -22                          26 
  1100 INDEX_ARC_IMPLICATION[13,23]
                                 -23                          26 
  1101 INDEX_ARC_IMPLICATION[13,24]
                                 -15                          26 
  1102 INDEX_ARC_IMPLICATION[13,25]
                                 -16                          26 
  1103 INDEX_ARC_IMPLICATION[13,26]
                                 -24                          26 
  1104 INDEX_ARC_IMPLICATION[14,1]
                                  12                          26 
  1105 INDEX_ARC_IMPLICATION[14,2]
                                   5                          26 
  1106 INDEX_ARC_IMPLICATION[14,3]
                                  -9                          26 
  1107 INDEX_ARC_IMPLICATION[14,4]
                                   8                          26 
  1108 INDEX_ARC_IMPLICATION[14,5]
                                   2                          26 
  1109 INDEX_ARC_IMPLICATION[14,6]
                                  26                          26 
  1110 INDEX_ARC_IMPLICATION[14,7]
                                   1                          26 
  1111 INDEX_ARC_IMPLICATION[14,8]
                                   9                          26 
  1112 INDEX_ARC_IMPLICATION[14,9]
                                 -10                          26 
  1113 INDEX_ARC_IMPLICATION[14,10]
                                   4                          26 
  1114 INDEX_ARC_IMPLICATION[14,11]
                                   7                          26 
  1115 INDEX_ARC_IMPLICATION[14,12]
                                  -7                          26 
  1116 INDEX_ARC_IMPLICATION[14,13]
                                  11                          26 
  1117 INDEX_ARC_IMPLICATION[14,15]
                                  -8                          26 
  1118 INDEX_ARC_IMPLICATION[14,16]
                                   6                          26 
  1119 INDEX_ARC_IMPLICATION[14,17]
                                  -3                          26 
  1120 INDEX_ARC_IMPLICATION[14,18]
                                  -6                          26 
  1121 INDEX_ARC_IMPLICATION[14,19]
                                   3                          26 
  1122 INDEX_ARC_IMPLICATION[14,20]
                                  10                          26 
  1123 INDEX_ARC_IMPLICATION[14,21]
                                  -2                          26 
  1124 INDEX_ARC_IMPLICATION[14,22]
                                 -11                          26 
  1125 INDEX_ARC_IMPLICATION[14,23]
                                 -12                          26 
  1126 INDEX_ARC_IMPLICATION[14,24]
                                  -4                          26 
  1127 INDEX_ARC_IMPLICATION[14,25]
                                  -5                          26 
  1128 INDEX_ARC_IMPLICATION[14,26]
                                 -13                          26 
  1129 INDEX_ARC_IMPLICATION[15,1]
                                  20                          26 
  1130 INDEX_ARC_IMPLICATION[15,2]
                                  13                          26 
  1131 INDEX_ARC_IMPLICATION[15,3]
                                  26                          26 
  1132 INDEX_ARC_IMPLICATION[15,4]
                                  16                          26 
  1133 INDEX_ARC_IMPLICATION[15,5]
                                  10                          26 
  1134 INDEX_ARC_IMPLICATION[15,6]
                                   7                          26 
  1135 INDEX_ARC_IMPLICATION[15,7]
                                   9                          26 
  1136 INDEX_ARC_IMPLICATION[15,8]
                                  17                          26 
  1137 INDEX_ARC_IMPLICATION[15,9]
                                  -2                          26 
  1138 INDEX_ARC_IMPLICATION[15,10]
                                  12                          26 
  1139 INDEX_ARC_IMPLICATION[15,11]
                                  15                          26 
  1140 INDEX_ARC_IMPLICATION[15,12]
                                   1                          26 
  1141 INDEX_ARC_IMPLICATION[15,14]
                                   8                          26 
  1142 INDEX_ARC_IMPLICATION[15,16]
                                  14                          26 
  1143 INDEX_ARC_IMPLICATION[15,17]
                                   5                          26 
  1144 INDEX_ARC_IMPLICATION[15,18]
                                   2                          26 
  1145 INDEX_ARC_IMPLICATION[15,19]
                                  11                          26 
  1146 INDEX_ARC_IMPLICATION[15,20]
                                  18                          26 
  1147 INDEX_ARC_IMPLICATION[15,21]
                                   6                          26 
  1148 INDEX_ARC_IMPLICATION[15,22]
                                  -3                          26 
  1149 INDEX_ARC_IMPLICATION[15,23]
                                  -4                          26 
  1150 INDEX_ARC_IMPLICATION[15,24]
                                   4                          26 
  1151 INDEX_ARC_IMPLICATION[15,25]
                                   3                          26 
  1152 INDEX_ARC_IMPLICATION[15,26]
                                  -5                          26 
  1153 INDEX_ARC_IMPLICATION[16,2]
                                  26                          26 
  1154 INDEX_ARC_IMPLICATION[16,3]
                                 -15                          26 
  1155 INDEX_ARC_IMPLICATION[16,4]
                                   2                          26 
  1156 INDEX_ARC_IMPLICATION[16,5]
                                  -4                          26 
  1157 INDEX_ARC_IMPLICATION[16,6]
                                  -7                          26 
  1158 INDEX_ARC_IMPLICATION[16,7]
                                  -5                          26 
  1159 INDEX_ARC_IMPLICATION[16,8]
                                   3                          26 
  1160 INDEX_ARC_IMPLICATION[16,9]
                                 -16                          26 
  1161 INDEX_ARC_IMPLICATION[16,10]
                                  -2                          26 
  1162 INDEX_ARC_IMPLICATION[16,11]
                                   1                          26 
  1163 INDEX_ARC_IMPLICATION[16,12]
                                 -13                          26 
  1164 INDEX_ARC_IMPLICATION[16,13]
                                   5                          26 
  1165 INDEX_ARC_IMPLICATION[16,14]
                                  -6                          26 
  1166 INDEX_ARC_IMPLICATION[16,15]
                                 -14                          26 
  1167 INDEX_ARC_IMPLICATION[16,17]
                                  -9                          26 
  1168 INDEX_ARC_IMPLICATION[16,18]
                                 -12                          26 
  1169 INDEX_ARC_IMPLICATION[16,19]
                                  -3                          26 
  1170 INDEX_ARC_IMPLICATION[16,20]
                                   4                          26 
  1171 INDEX_ARC_IMPLICATION[16,21]
                                  -8                          26 
  1172 INDEX_ARC_IMPLICATION[16,22]
                                 -17                          26 
  1173 INDEX_ARC_IMPLICATION[16,23]
                                 -18                          26 
  1174 INDEX_ARC_IMPLICATION[16,24]
                                 -10                          26 
  1175 INDEX_ARC_IMPLICATION[16,25]
                                 -11                          26 
  1176 INDEX_ARC_IMPLICATION[16,26]
                                 -19                          26 
  1177 INDEX_ARC_IMPLICATION[17,1]
                                  15                          26 
  1178 INDEX_ARC_IMPLICATION[17,2]
                                   8                          26 
  1179 INDEX_ARC_IMPLICATION[17,3]
                                  -6                          26 
  1180 INDEX_ARC_IMPLICATION[17,4]
                                  11                          26 
  1181 INDEX_ARC_IMPLICATION[17,5]
                                   5                          26 
  1182 INDEX_ARC_IMPLICATION[17,6]
                                   2                          26 
  1183 INDEX_ARC_IMPLICATION[17,7]
                                   4                          26 
  1184 INDEX_ARC_IMPLICATION[17,8]
                                  12                          26 
  1185 INDEX_ARC_IMPLICATION[17,9]
                                  -7                          26 
  1186 INDEX_ARC_IMPLICATION[17,10]
                                   7                          26 
  1187 INDEX_ARC_IMPLICATION[17,11]
                                  10                          26 
  1188 INDEX_ARC_IMPLICATION[17,12]
                                  -4                          26 
  1189 INDEX_ARC_IMPLICATION[17,13]
                                  14                          26 
  1190 INDEX_ARC_IMPLICATION[17,14]
                                   3                          26 
  1191 INDEX_ARC_IMPLICATION[17,15]
                                  -5                          26 
  1192 INDEX_ARC_IMPLICATION[17,16]
                                   9                          26 
  1193 INDEX_ARC_IMPLICATION[17,18]
                                  -3                          26 
  1194 INDEX_ARC_IMPLICATION[17,19]
                                   6                          26 
  1195 INDEX_ARC_IMPLICATION[17,20]
                                  13                          26 
  1196 INDEX_ARC_IMPLICATION[17,21]
                                   1                          26 
  1197 INDEX_ARC_IMPLICATION[17,22]
                                  -8                          26 
  1198 INDEX_ARC_IMPLICATION[17,23]
                                  -9                          26 
  1199 INDEX_ARC_IMPLICATION[17,24]
                                  26                          26 
  1200 INDEX_ARC_IMPLICATION[17,25]
                                  -2                          26 
  1201 INDEX_ARC_IMPLICATION[17,26]
                                 -10                          26 
  1202 INDEX_ARC_IMPLICATION[18,1]
                                  18                          26 
  1203 INDEX_ARC_IMPLICATION[18,2]
                                  11                          26 
  1204 INDEX_ARC_IMPLICATION[18,3]
                                  -3                          26 
  1205 INDEX_ARC_IMPLICATION[18,4]
                                  14                          26 
  1206 INDEX_ARC_IMPLICATION[18,5]
                                   8                          26 
  1207 INDEX_ARC_IMPLICATION[18,6]
                                   5                          26 
  1208 INDEX_ARC_IMPLICATION[18,7]
                                   7                          26 
  1209 INDEX_ARC_IMPLICATION[18,8]
                                  15                          26 
  1210 INDEX_ARC_IMPLICATION[18,9]
                                  -4                          26 
  1211 INDEX_ARC_IMPLICATION[18,10]
                                  10                          26 
  1212 INDEX_ARC_IMPLICATION[18,11]
                                  13                          26 
  1213 INDEX_ARC_IMPLICATION[18,12]
                                  26                          26 
  1214 INDEX_ARC_IMPLICATION[18,13]
                                  17                          26 
  1215 INDEX_ARC_IMPLICATION[18,14]
                                   6                          26 
  1216 INDEX_ARC_IMPLICATION[18,15]
                                  -2                          26 
  1217 INDEX_ARC_IMPLICATION[18,16]
                                  12                          26 
  1218 INDEX_ARC_IMPLICATION[18,17]
                                   3                          26 
  1219 INDEX_ARC_IMPLICATION[18,19]
                                   9                          26 
  1220 INDEX_ARC_IMPLICATION[18,20]
                                  16                          26 
  1221 INDEX_ARC_IMPLICATION[18,21]
                                   4                          26 
  1222 INDEX_ARC_IMPLICATION[18,22]
                                  -5                          26 
  1223 INDEX_ARC_IMPLICATION[18,23]
                                  -6                          26 
  1224 INDEX_ARC_IMPLICATION[18,24]
                                   2                          26 
  1225 INDEX_ARC_IMPLICATION[18,25]
                                   1                          26 
  1226 INDEX_ARC_IMPLICATION[18,26]
                                  -7                          26 
  1227 INDEX_ARC_IMPLICATION[19,2]
                                   2                          26 
  1228 INDEX_ARC_IMPLICATION[19,3]
                                 -12                          26 
  1229 INDEX_ARC_IMPLICATION[19,4]
                                   5                          26 
  1230 INDEX_ARC_IMPLICATION[19,5]
                                  26                          26 
  1231 INDEX_ARC_IMPLICATION[19,6]
                                  -4                          26 
  1232 INDEX_ARC_IMPLICATION[19,7]
                                  -2                          26 
  1233 INDEX_ARC_IMPLICATION[19,8]
                                   6                          26 
  1234 INDEX_ARC_IMPLICATION[19,9]
                                 -13                          26 
  1235 INDEX_ARC_IMPLICATION[19,10]
                                   1                          26 
  1236 INDEX_ARC_IMPLICATION[19,12]
                                 -10                          26 
  1237 INDEX_ARC_IMPLICATION[19,13]
                                   8                          26 
  1238 INDEX_ARC_IMPLICATION[19,14]
                                  -3                          26 
  1239 INDEX_ARC_IMPLICATION[19,15]
                                 -11                          26 
  1240 INDEX_ARC_IMPLICATION[19,16]
                                   3                          26 
  1241 INDEX_ARC_IMPLICATION[19,17]
                                  -6                          26 
  1242 INDEX_ARC_IMPLICATION[19,18]
                                  -9                          26 
  1243 INDEX_ARC_IMPLICATION[19,20]
                                   7                          26 
  1244 INDEX_ARC_IMPLICATION[19,21]
                                  -5                          26 
  1245 INDEX_ARC_IMPLICATION[19,22]
                                 -14                          26 
  1246 INDEX_ARC_IMPLICATION[19,23]
                                 -15                          26 
  1247 INDEX_ARC_IMPLICATION[19,24]
                                  -7                          26 
  1248 INDEX_ARC_IMPLICATION[19,25]
                                  -8                          26 
  1249 INDEX_ARC_IMPLICATION[19,26]
                                 -16                          26 
  1250 INDEX_ARC_IMPLICATION[20,1]
                                   2                          26 
  1251 INDEX_ARC_IMPLICATION[20,2]
                                  -5                          26 
  1252 INDEX_ARC_IMPLICATION[20,3]
                                 -19                          26 
  1253 INDEX_ARC_IMPLICATION[20,4]
                                  -2                          26 
  1254 INDEX_ARC_IMPLICATION[20,5]
                                  -8                          26 
  1255 INDEX_ARC_IMPLICATION[20,6]
                                 -11                          26 
  1256 INDEX_ARC_IMPLICATION[20,7]
                                  -9                          26 
  1257 INDEX_ARC_IMPLICATION[20,8]
                                  26                          26 
  1258 INDEX_ARC_IMPLICATION[20,9]
                                 -20                          26 
  1259 INDEX_ARC_IMPLICATION[20,10]
                                  -6                          26 
  1260 INDEX_ARC_IMPLICATION[20,11]
                                  -3                          26 
  1261 INDEX_ARC_IMPLICATION[20,12]
                                 -17                          26 
  1262 INDEX_ARC_IMPLICATION[20,13]
                                   1                          26 
  1263 INDEX_ARC_IMPLICATION[20,14]
                                 -10                          26 
  1264 INDEX_ARC_IMPLICATION[20,15]
                                 -18                          26 
  1265 INDEX_ARC_IMPLICATION[20,16]
                                  -4                          26 
  1266 INDEX_ARC_IMPLICATION[20,17]
                                 -13                          26 
  1267 INDEX_ARC_IMPLICATION[20,18]
                                 -16                          26 
  1268 INDEX_ARC_IMPLICATION[20,19]
                                  -7                          26 
  1269 INDEX_ARC_IMPLICATION[20,21]
                                 -12                          26 
  1270 INDEX_ARC_IMPLICATION[20,22]
                                 -21                          26 
  1271 INDEX_ARC_IMPLICATION[20,23]
                                 -22                          26 
  1272 INDEX_ARC_IMPLICATION[20,24]
                                 -14                          26 
  1273 INDEX_ARC_IMPLICATION[20,25]
                                 -15                          26 
  1274 INDEX_ARC_IMPLICATION[20,26]
                                 -23                          26 
  1275 INDEX_ARC_IMPLICATION[21,1]
                                  14                          26 
  1276 INDEX_ARC_IMPLICATION[21,2]
                                   7                          26 
  1277 INDEX_ARC_IMPLICATION[21,3]
                                  -7                          26 
  1278 INDEX_ARC_IMPLICATION[21,4]
                                  10                          26 
  1279 INDEX_ARC_IMPLICATION[21,5]
                                   4                          26 
  1280 INDEX_ARC_IMPLICATION[21,6]
                                   1                          26 
  1281 INDEX_ARC_IMPLICATION[21,7]
                                   3                          26 
  1282 INDEX_ARC_IMPLICATION[21,8]
                                  11                          26 
  1283 INDEX_ARC_IMPLICATION[21,9]
                                  -8                          26 
  1284 INDEX_ARC_IMPLICATION[21,10]
                                   6                          26 
  1285 INDEX_ARC_IMPLICATION[21,11]
                                   9                          26 
  1286 INDEX_ARC_IMPLICATION[21,12]
                                  -5                          26 
  1287 INDEX_ARC_IMPLICATION[21,13]
                                  13                          26 
  1288 INDEX_ARC_IMPLICATION[21,14]
                                   2                          26 
  1289 INDEX_ARC_IMPLICATION[21,15]
                                  -6                          26 
  1290 INDEX_ARC_IMPLICATION[21,16]
                                   8                          26 
  1291 INDEX_ARC_IMPLICATION[21,17]
                                  26                          26 
  1292 INDEX_ARC_IMPLICATION[21,18]
                                  -4                          26 
  1293 INDEX_ARC_IMPLICATION[21,19]
                                   5                          26 
  1294 INDEX_ARC_IMPLICATION[21,20]
                                  12                          26 
  1295 INDEX_ARC_IMPLICATION[21,22]
                                  -9                          26 
  1296 INDEX_ARC_IMPLICATION[21,23]
                                 -10                          26 
  1297 INDEX_ARC_IMPLICATION[21,24]
                                  -2                          26 
  1298 INDEX_ARC_IMPLICATION[21,25]
                                  -3                          26 
  1299 INDEX_ARC_IMPLICATION[21,26]
                                 -11                          26 
  1300 INDEX_ARC_IMPLICATION[22,1]
                                  23                          26 
  1301 INDEX_ARC_IMPLICATION[22,2]
                                  16                          26 
  1302 INDEX_ARC_IMPLICATION[22,3]
                                   2                          26 
  1303 INDEX_ARC_IMPLICATION[22,4]
                                  19                          26 
  1304 INDEX_ARC_IMPLICATION[22,5]
                                  13                          26 
  1305 INDEX_ARC_IMPLICATION[22,6]
                                  10                          26 
  1306 INDEX_ARC_IMPLICATION[22,7]
                                  12                          26 
  1307 INDEX_ARC_IMPLICATION[22,8]
                                  20                          26 
  1308 INDEX_ARC_IMPLICATION[22,9]
                                   1                          26 
  1309 INDEX_ARC_IMPLICATION[22,10]
                                  15                          26 
  1310 INDEX_ARC_IMPLICATION[22,11]
                                  18                          26 
  1311 INDEX_ARC_IMPLICATION[22,12]
                                   4                          26 
  1312 INDEX_ARC_IMPLICATION[22,13]
                                  22                          26 
  1313 INDEX_ARC_IMPLICATION[22,14]
                                  11                          26 
  1314 INDEX_ARC_IMPLICATION[22,15]
                                   3                          26 
  1315 INDEX_ARC_IMPLICATION[22,16]
                                  17                          26 
  1316 INDEX_ARC_IMPLICATION[22,18]
                                   5                          26 
  1317 INDEX_ARC_IMPLICATION[22,19]
                                  14                          26 
  1318 INDEX_ARC_IMPLICATION[22,20]
                                  21                          26 
  1319 INDEX_ARC_IMPLICATION[22,21]
                                   9                          26 
  1320 INDEX_ARC_IMPLICATION[22,23]
                                  26                          26 
  1321 INDEX_ARC_IMPLICATION[22,24]
                                   7                          26 
  1322 INDEX_ARC_IMPLICATION[22,25]
                                   6                          26 
  1323 INDEX_ARC_IMPLICATION[22,26]
                                  -2                          26 
  1324 INDEX_ARC_IMPLICATION[23,1]
                                  24                          26 
  1325 INDEX_ARC_IMPLICATION[23,2]
                                  17                          26 
  1326 INDEX_ARC_IMPLICATION[23,3]
                                   3                          26 
  1327 INDEX_ARC_IMPLICATION[23,4]
                                  20                          26 
  1328 INDEX_ARC_IMPLICATION[23,5]
                                  14                          26 
  1329 INDEX_ARC_IMPLICATION[23,6]
                                  11                          26 
  1330 INDEX_ARC_IMPLICATION[23,7]
                                  13                          26 
  1331 INDEX_ARC_IMPLICATION[23,8]
                                  21                          26 
  1332 INDEX_ARC_IMPLICATION[23,9]
                                   2                          26 
  1333 INDEX_ARC_IMPLICATION[23,10]
                                  16                          26 
  1334 INDEX_ARC_IMPLICATION[23,11]
                                  19                          26 
  1335 INDEX_ARC_IMPLICATION[23,12]
                                   5                          26 
  1336 INDEX_ARC_IMPLICATION[23,13]
                                  23                          26 
  1337 INDEX_ARC_IMPLICATION[23,14]
                                  12                          26 
  1338 INDEX_ARC_IMPLICATION[23,15]
                                   4                          26 
  1339 INDEX_ARC_IMPLICATION[23,16]
                                  18                          26 
  1340 INDEX_ARC_IMPLICATION[23,17]
                                   9                          26 
  1341 INDEX_ARC_IMPLICATION[23,18]
                                   6                          26 
  1342 INDEX_ARC_IMPLICATION[23,19]
                                  15                          26 
  1343 INDEX_ARC_IMPLICATION[23,20]
                                  22                          26 
  1344 INDEX_ARC_IMPLICATION[23,21]
                                  10                          26 
  1345 INDEX_ARC_IMPLICATION[23,22]
                                   1                          26 
  1346 INDEX_ARC_IMPLICATION[23,24]
                                   8                          26 
  1347 INDEX_ARC_IMPLICATION[23,25]
                                   7                          26 
  1348 INDEX_ARC_IMPLICATION[23,26]
                                  26                          26 
  1349 INDEX_ARC_IMPLICATION[24,1]
                                  16                          26 
  1350 INDEX_ARC_IMPLICATION[24,2]
                                   9                          26 
  1351 INDEX_ARC_IMPLICATION[24,3]
                                  -5                          26 
  1352 INDEX_ARC_IMPLICATION[24,4]
                                  12                          26 
  1353 INDEX_ARC_IMPLICATION[24,5]
                                   6                          26 
  1354 INDEX_ARC_IMPLICATION[24,6]
                                   3                          26 
  1355 INDEX_ARC_IMPLICATION[24,7]
                                   5                          26 
  1356 INDEX_ARC_IMPLICATION[24,8]
                                  13                          26 
  1357 INDEX_ARC_IMPLICATION[24,9]
                                  -6                          26 
  1358 INDEX_ARC_IMPLICATION[24,10]
                                   8                          26 
  1359 INDEX_ARC_IMPLICATION[24,11]
                                  11                          26 
  1360 INDEX_ARC_IMPLICATION[24,12]
                                  -3                          26 
  1361 INDEX_ARC_IMPLICATION[24,13]
                                  15                          26 
  1362 INDEX_ARC_IMPLICATION[24,14]
                                   4                          26 
  1363 INDEX_ARC_IMPLICATION[24,15]
                                  -4                          26 
  1364 INDEX_ARC_IMPLICATION[24,16]
                                  10                          26 
  1365 INDEX_ARC_IMPLICATION[24,17]
                                   1                          26 
  1366 INDEX_ARC_IMPLICATION[24,18]
                                  -2                          26 
  1367 INDEX_ARC_IMPLICATION[24,19]
                                   7                          26 
  1368 INDEX_ARC_IMPLICATION[24,20]
                                  14                          26 
  1369 INDEX_ARC_IMPLICATION[24,21]
                                   2                          26 
  1370 INDEX_ARC_IMPLICATION[24,22]
                                  -7                          26 
  1371 INDEX_ARC_IMPLICATION[24,23]
                                  -8                          26 
  1372 INDEX_ARC_IMPLICATION[24,25]
                                  26                          26 
  1373 INDEX_ARC_IMPLICATION[24,26]
                                  -9                          26 
  1374 INDEX_ARC_IMPLICATION[25,1]
                                  17                          26 
  1375 INDEX_ARC_IMPLICATION[25,2]
                                  10                          26 
  1376 INDEX_ARC_IMPLICATION[25,3]
                                  -4                          26 
  1377 INDEX_ARC_IMPLICATION[25,4]
                                  13                          26 
  1378 INDEX_ARC_IMPLICATION[25,6]
                                   4                          26 
  1379 INDEX_ARC_IMPLICATION[25,7]
                                   6                          26 
  1380 INDEX_ARC_IMPLICATION[25,8]
                                  14                          26 
  1381 INDEX_ARC_IMPLICATION[25,9]
                                  -5                          26 
  1382 INDEX_ARC_IMPLICATION[25,10]
                                   9                          26 
  1383 INDEX_ARC_IMPLICATION[25,11]
                                  12                          26 
  1384 INDEX_ARC_IMPLICATION[25,12]
                                  -2                          26 
  1385 INDEX_ARC_IMPLICATION[25,13]
                                  16                          26 
  1386 INDEX_ARC_IMPLICATION[25,14]
                                   5                          26 
  1387 INDEX_ARC_IMPLICATION[25,15]
                                  -3                          26 
  1388 INDEX_ARC_IMPLICATION[25,16]
                                  11                          26 
  1389 INDEX_ARC_IMPLICATION[25,17]
                                   2                          26 
  1390 INDEX_ARC_IMPLICATION[25,18]
                                  26                          26 
  1391 INDEX_ARC_IMPLICATION[25,19]
                                   8                          26 
  1392 INDEX_ARC_IMPLICATION[25,20]
                                  15                          26 
  1393 INDEX_ARC_IMPLICATION[25,21]
                                   3                          26 
  1394 INDEX_ARC_IMPLICATION[25,22]
                                  -6                          26 
  1395 INDEX_ARC_IMPLICATION[25,23]
                                  -7                          26 
  1396 INDEX_ARC_IMPLICATION[25,24]
                                   1                          26 
  1397 INDEX_ARC_IMPLICATION[25,26]
                                  -8                          26 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[0,1]       *              1             0             1 
     2 x[0,2]       *              0             0             1 
     3 x[0,3]       *              0             0             1 
     4 x[0,4]       *              0             0             1 
     5 x[0,5]       *              0             0             1 
     6 x[0,6]       *              0             0             1 
     7 x[0,7]       *              0             0             1 
     8 x[0,8]       *              0             0             1 
     9 x[0,9]       *              0             0             1 
    10 x[0,10]      *              0             0             1 
    11 x[0,11]      *              0             0             1 
    12 x[0,12]      *              0             0             1 
    13 x[0,13]      *              0             0             1 
    14 x[0,14]      *              0             0             1 
    15 x[0,15]      *              0             0             1 
    16 x[0,16]      *              0             0             1 
    17 x[0,17]      *              0             0             1 
    18 x[0,18]      *              0             0             1 
    19 x[0,19]      *              0             0             1 
    20 x[0,20]      *              0             0             1 
    21 x[0,21]      *              0             0             1 
    22 x[0,22]      *              0             0             1 
    23 x[0,23]      *              0             0             1 
    24 x[0,24]      *              0             0             1 
    25 x[0,25]      *              0             0             1 
    26 x[0,26]      *              0             0             1 
    27 x[1,2]       *              0             0             1 
    28 x[1,3]       *              0             0             1 
    29 x[1,4]       *              0             0             1 
    30 x[1,5]       *              0             0             1 
    31 x[1,6]       *              0             0             1 
    32 x[1,7]       *              0             0             1 
    33 x[1,8]       *              0             0             1 
    34 x[1,9]       *              0             0             1 
    35 x[1,10]      *              0             0             1 
    36 x[1,11]      *              0             0             1 
    37 x[1,12]      *              0             0             1 
    38 x[1,13]      *              1             0             1 
    39 x[1,14]      *              0             0             1 
    40 x[1,15]      *              0             0             1 
    41 x[1,16]      *              0             0             1 
    42 x[1,17]      *              0             0             1 
    43 x[1,18]      *              0             0             1 
    44 x[1,19]      *              0             0             1 
    45 x[1,20]      *              0             0             1 
    46 x[1,21]      *              0             0             1 
    47 x[1,22]      *              0             0             1 
    48 x[1,23]      *              0             0             1 
    49 x[1,24]      *              0             0             1 
    50 x[1,25]      *              0             0             1 
    51 x[1,26]      *              0             0             1 
    52 x[2,1]       *              0             0             1 
    53 x[2,3]       *              0             0             1 
    54 x[2,4]       *              0             0             1 
    55 x[2,5]       *              0             0             1 
    56 x[2,6]       *              0             0             1 
    57 x[2,7]       *              0             0             1 
    58 x[2,8]       *              0             0             1 
    59 x[2,9]       *              0             0             1 
    60 x[2,10]      *              1             0             1 
    61 x[2,11]      *              0             0             1 
    62 x[2,12]      *              0             0             1 
    63 x[2,13]      *              0             0             1 
    64 x[2,14]      *              0             0             1 
    65 x[2,15]      *              0             0             1 
    66 x[2,16]      *              0             0             1 
    67 x[2,17]      *              0             0             1 
    68 x[2,18]      *              0             0             1 
    69 x[2,19]      *              0             0             1 
    70 x[2,20]      *              0             0             1 
    71 x[2,21]      *              0             0             1 
    72 x[2,22]      *              0             0             1 
    73 x[2,23]      *              0             0             1 
    74 x[2,24]      *              0             0             1 
    75 x[2,25]      *              0             0             1 
    76 x[2,26]      *              0             0             1 
    77 x[3,1]       *              0             0             1 
    78 x[3,2]       *              0             0             1 
    79 x[3,4]       *              0             0             1 
    80 x[3,5]       *              0             0             1 
    81 x[3,6]       *              0             0             1 
    82 x[3,7]       *              0             0             1 
    83 x[3,8]       *              0             0             1 
    84 x[3,9]       *              1             0             1 
    85 x[3,10]      *              0             0             1 
    86 x[3,11]      *              0             0             1 
    87 x[3,12]      *              0             0             1 
    88 x[3,13]      *              0             0             1 
    89 x[3,14]      *              0             0             1 
    90 x[3,15]      *              0             0             1 
    91 x[3,16]      *              0             0             1 
    92 x[3,17]      *              0             0             1 
    93 x[3,18]      *              0             0             1 
    94 x[3,19]      *              0             0             1 
    95 x[3,20]      *              0             0             1 
    96 x[3,21]      *              0             0             1 
    97 x[3,22]      *              0             0             1 
    98 x[3,23]      *              0             0             1 
    99 x[3,24]      *              0             0             1 
   100 x[3,25]      *              0             0             1 
   101 x[3,26]      *              0             0             1 
   102 x[4,1]       *              0             0             1 
   103 x[4,2]       *              0             0             1 
   104 x[4,3]       *              0             0             1 
   105 x[4,5]       *              0             0             1 
   106 x[4,6]       *              0             0             1 
   107 x[4,7]       *              0             0             1 
   108 x[4,8]       *              0             0             1 
   109 x[4,9]       *              0             0             1 
   110 x[4,10]      *              0             0             1 
   111 x[4,11]      *              1             0             1 
   112 x[4,12]      *              0             0             1 
   113 x[4,13]      *              0             0             1 
   114 x[4,14]      *              0             0             1 
   115 x[4,15]      *              0             0             1 
   116 x[4,16]      *              0             0             1 
   117 x[4,17]      *              0             0             1 
   118 x[4,18]      *              0             0             1 
   119 x[4,19]      *              0             0             1 
   120 x[4,20]      *              0             0             1 
   121 x[4,21]      *              0             0             1 
   122 x[4,22]      *              0             0             1 
   123 x[4,23]      *              0             0             1 
   124 x[4,24]      *              0             0             1 
   125 x[4,25]      *              0             0             1 
   126 x[4,26]      *              0             0             1 
   127 x[5,1]       *              0             0             1 
   128 x[5,2]       *              0             0             1 
   129 x[5,3]       *              0             0             1 
   130 x[5,4]       *              0             0             1 
   131 x[5,6]       *              0             0             1 
   132 x[5,7]       *              1             0             1 
   133 x[5,8]       *              0             0             1 
   134 x[5,9]       *              0             0             1 
   135 x[5,10]      *              0             0             1 
   136 x[5,11]      *              0             0             1 
   137 x[5,12]      *              0             0             1 
   138 x[5,13]      *              0             0             1 
   139 x[5,14]      *              0             0             1 
   140 x[5,15]      *              0             0             1 
   141 x[5,16]      *              0             0             1 
   142 x[5,17]      *              0             0             1 
   143 x[5,18]      *              0             0             1 
   144 x[5,19]      *              0             0             1 
   145 x[5,20]      *              0             0             1 
   146 x[5,21]      *              0             0             1 
   147 x[5,22]      *              0             0             1 
   148 x[5,23]      *              0             0             1 
   149 x[5,24]      *              0             0             1 
   150 x[5,25]      *              0             0             1 
   151 x[5,26]      *              0             0             1 
   152 x[6,2]       *              0             0             1 
   153 x[6,3]       *              0             0             1 
   154 x[6,4]       *              0             0             1 
   155 x[6,5]       *              0             0             1 
   156 x[6,7]       *              0             0             1 
   157 x[6,8]       *              0             0             1 
   158 x[6,9]       *              0             0             1 
   159 x[6,10]      *              0             0             1 
   160 x[6,11]      *              0             0             1 
   161 x[6,12]      *              0             0             1 
   162 x[6,13]      *              0             0             1 
   163 x[6,14]      *              0             0             1 
   164 x[6,15]      *              0             0             1 
   165 x[6,16]      *              0             0             1 
   166 x[6,17]      *              0             0             1 
   167 x[6,18]      *              0             0             1 
   168 x[6,19]      *              0             0             1 
   169 x[6,20]      *              0             0             1 
   170 x[6,21]      *              1             0             1 
   171 x[6,22]      *              0             0             1 
   172 x[6,23]      *              0             0             1 
   173 x[6,24]      *              0             0             1 
   174 x[6,25]      *              0             0             1 
   175 x[6,26]      *              0             0             1 
   176 x[7,1]       *              0             0             1 
   177 x[7,2]       *              0             0             1 
   178 x[7,3]       *              0             0             1 
   179 x[7,4]       *              0             0             1 
   180 x[7,5]       *              0             0             1 
   181 x[7,6]       *              0             0             1 
   182 x[7,8]       *              0             0             1 
   183 x[7,9]       *              0             0             1 
   184 x[7,10]      *              0             0             1 
   185 x[7,11]      *              0             0             1 
   186 x[7,12]      *              0             0             1 
   187 x[7,13]      *              0             0             1 
   188 x[7,14]      *              1             0             1 
   189 x[7,15]      *              0             0             1 
   190 x[7,16]      *              0             0             1 
   191 x[7,17]      *              0             0             1 
   192 x[7,18]      *              0             0             1 
   193 x[7,19]      *              0             0             1 
   194 x[7,20]      *              0             0             1 
   195 x[7,21]      *              0             0             1 
   196 x[7,22]      *              0             0             1 
   197 x[7,23]      *              0             0             1 
   198 x[7,24]      *              0             0             1 
   199 x[7,25]      *              0             0             1 
   200 x[7,26]      *              0             0             1 
   201 x[8,1]       *              0             0             1 
   202 x[8,2]       *              0             0             1 
   203 x[8,3]       *              0             0             1 
   204 x[8,4]       *              1             0             1 
   205 x[8,5]       *              0             0             1 
   206 x[8,6]       *              0             0             1 
   207 x[8,7]       *              0             0             1 
   208 x[8,9]       *              0             0             1 
   209 x[8,10]      *              0             0             1 
   210 x[8,11]      *              0             0             1 
   211 x[8,12]      *              0             0             1 
   212 x[8,13]      *              0             0             1 
   213 x[8,14]      *              0             0             1 
   214 x[8,15]      *              0             0             1 
   215 x[8,16]      *              0             0             1 
   216 x[8,17]      *              0             0             1 
   217 x[8,18]      *              0             0             1 
   218 x[8,19]      *              0             0             1 
   219 x[8,20]      *              0             0             1 
   220 x[8,21]      *              0             0             1 
   221 x[8,22]      *              0             0             1 
   222 x[8,23]      *              0             0             1 
   223 x[8,24]      *              0             0             1 
   224 x[8,25]      *              0             0             1 
   225 x[8,26]      *              0             0             1 
   226 x[9,1]       *              0             0             1 
   227 x[9,2]       *              0             0             1 
   228 x[9,3]       *              0             0             1 
   229 x[9,4]       *              0             0             1 
   230 x[9,5]       *              0             0             1 
   231 x[9,6]       *              0             0             1 
   232 x[9,7]       *              0             0             1 
   233 x[9,8]       *              0             0             1 
   234 x[9,10]      *              0             0             1 
   235 x[9,11]      *              0             0             1 
   236 x[9,12]      *              0             0             1 
   237 x[9,13]      *              0             0             1 
   238 x[9,14]      *              0             0             1 
   239 x[9,15]      *              0             0             1 
   240 x[9,16]      *              0             0             1 
   241 x[9,17]      *              0             0             1 
   242 x[9,18]      *              0             0             1 
   243 x[9,19]      *              0             0             1 
   244 x[9,20]      *              0             0             1 
   245 x[9,21]      *              0             0             1 
   246 x[9,22]      *              1             0             1 
   247 x[9,23]      *              0             0             1 
   248 x[9,24]      *              0             0             1 
   249 x[9,25]      *              0             0             1 
   250 x[9,26]      *              0             0             1 
   251 x[10,1]      *              0             0             1 
   252 x[10,2]      *              0             0             1 
   253 x[10,3]      *              0             0             1 
   254 x[10,4]      *              0             0             1 
   255 x[10,5]      *              0             0             1 
   256 x[10,6]      *              0             0             1 
   257 x[10,7]      *              0             0             1 
   258 x[10,8]      *              0             0             1 
   259 x[10,9]      *              0             0             1 
   260 x[10,11]     *              0             0             1 
   261 x[10,12]     *              0             0             1 
   262 x[10,13]     *              0             0             1 
   263 x[10,14]     *              0             0             1 
   264 x[10,15]     *              0             0             1 
   265 x[10,16]     *              0             0             1 
   266 x[10,17]     *              0             0             1 
   267 x[10,18]     *              0             0             1 
   268 x[10,19]     *              1             0             1 
   269 x[10,20]     *              0             0             1 
   270 x[10,21]     *              0             0             1 
   271 x[10,22]     *              0             0             1 
   272 x[10,23]     *              0             0             1 
   273 x[10,24]     *              0             0             1 
   274 x[10,25]     *              0             0             1 
   275 x[10,26]     *              0             0             1 
   276 x[11,2]      *              0             0             1 
   277 x[11,3]      *              0             0             1 
   278 x[11,4]      *              0             0             1 
   279 x[11,5]      *              0             0             1 
   280 x[11,6]      *              0             0             1 
   281 x[11,7]      *              0             0             1 
   282 x[11,8]      *              0             0             1 
   283 x[11,9]      *              0             0             1 
   284 x[11,10]     *              0             0             1 
   285 x[11,12]     *              0             0             1 
   286 x[11,13]     *              0             0             1 
   287 x[11,14]     *              0             0             1 
   288 x[11,15]     *              0             0             1 
   289 x[11,16]     *              1             0             1 
   290 x[11,17]     *              0             0             1 
   291 x[11,18]     *              0             0             1 
   292 x[11,19]     *              0             0             1 
   293 x[11,20]     *              0             0             1 
   294 x[11,21]     *              0             0             1 
   295 x[11,22]     *              0             0             1 
   296 x[11,23]     *              0             0             1 
   297 x[11,24]     *              0             0             1 
   298 x[11,25]     *              0             0             1 
   299 x[11,26]     *              0             0             1 
   300 x[12,2]      *              0             0             1 
   301 x[12,3]      *              0             0             1 
   302 x[12,4]      *              0             0             1 
   303 x[12,5]      *              0             0             1 
   304 x[12,7]      *              0             0             1 
   305 x[12,9]      *              0             0             1 
   306 x[12,10]     *              0             0             1 
   307 x[12,11]     *              0             0             1 
   308 x[12,13]     *              0             0             1 
   309 x[12,14]     *              0             0             1 
   310 x[12,15]     *              1             0             1 
   311 x[12,16]     *              0             0             1 
   312 x[12,17]     *              0             0             1 
   313 x[12,18]     *              0             0             1 
   314 x[12,19]     *              0             0             1 
   315 x[12,20]     *              0             0             1 
   316 x[12,21]     *              0             0             1 
   317 x[12,22]     *              0             0             1 
   318 x[12,23]     *              0             0             1 
   319 x[12,24]     *              0             0             1 
   320 x[12,25]     *              0             0             1 
   321 x[12,26]     *              0             0             1 
   322 x[13,1]      *              0             0             1 
   323 x[13,2]      *              0             0             1 
   324 x[13,3]      *              0             0             1 
   325 x[13,4]      *              0             0             1 
   326 x[13,5]      *              0             0             1 
   327 x[13,6]      *              0             0             1 
   328 x[13,7]      *              0             0             1 
   329 x[13,8]      *              0             0             1 
   330 x[13,9]      *              0             0             1 
   331 x[13,10]     *              0             0             1 
   332 x[13,11]     *              0             0             1 
   333 x[13,12]     *              0             0             1 
   334 x[13,14]     *              0             0             1 
   335 x[13,15]     *              0             0             1 
   336 x[13,16]     *              0             0             1 
   337 x[13,17]     *              0             0             1 
   338 x[13,18]     *              0             0             1 
   339 x[13,19]     *              0             0             1 
   340 x[13,20]     *              1             0             1 
   341 x[13,21]     *              0             0             1 
   342 x[13,22]     *              0             0             1 
   343 x[13,23]     *              0             0             1 
   344 x[13,24]     *              0             0             1 
   345 x[13,25]     *              0             0             1 
   346 x[13,26]     *              0             0             1 
   347 x[14,1]      *              0             0             1 
   348 x[14,2]      *              0             0             1 
   349 x[14,3]      *              0             0             1 
   350 x[14,4]      *              0             0             1 
   351 x[14,5]      *              0             0             1 
   352 x[14,6]      *              1             0             1 
   353 x[14,7]      *              0             0             1 
   354 x[14,8]      *              0             0             1 
   355 x[14,9]      *              0             0             1 
   356 x[14,10]     *              0             0             1 
   357 x[14,11]     *              0             0             1 
   358 x[14,12]     *              0             0             1 
   359 x[14,13]     *              0             0             1 
   360 x[14,15]     *              0             0             1 
   361 x[14,16]     *              0             0             1 
   362 x[14,17]     *              0             0             1 
   363 x[14,18]     *              0             0             1 
   364 x[14,19]     *              0             0             1 
   365 x[14,20]     *              0             0             1 
   366 x[14,21]     *              0             0             1 
   367 x[14,22]     *              0             0             1 
   368 x[14,23]     *              0             0             1 
   369 x[14,24]     *              0             0             1 
   370 x[14,25]     *              0             0             1 
   371 x[14,26]     *              0             0             1 
   372 x[15,1]      *              0             0             1 
   373 x[15,2]      *              0             0             1 
   374 x[15,3]      *              1             0             1 
   375 x[15,4]      *              0             0             1 
   376 x[15,5]      *              0             0             1 
   377 x[15,6]      *              0             0             1 
   378 x[15,7]      *              0             0             1 
   379 x[15,8]      *              0             0             1 
   380 x[15,9]      *              0             0             1 
   381 x[15,10]     *              0             0             1 
   382 x[15,11]     *              0             0             1 
   383 x[15,12]     *              0             0             1 
   384 x[15,14]     *              0             0             1 
   385 x[15,16]     *              0             0             1 
   386 x[15,17]     *              0             0             1 
   387 x[15,18]     *              0             0             1 
   388 x[15,19]     *              0             0             1 
   389 x[15,20]     *              0             0             1 
   390 x[15,21]     *              0             0             1 
   391 x[15,22]     *              0             0             1 
   392 x[15,23]     *              0             0             1 
   393 x[15,24]     *              0             0             1 
   394 x[15,25]     *              0             0             1 
   395 x[15,26]     *              0             0             1 
   396 x[16,2]      *              1             0             1 
   397 x[16,3]      *              0             0             1 
   398 x[16,4]      *              0             0             1 
   399 x[16,5]      *              0             0             1 
   400 x[16,6]      *              0             0             1 
   401 x[16,7]      *              0             0             1 
   402 x[16,8]      *              0             0             1 
   403 x[16,9]      *              0             0             1 
   404 x[16,10]     *              0             0             1 
   405 x[16,11]     *              0             0             1 
   406 x[16,12]     *              0             0             1 
   407 x[16,13]     *              0             0             1 
   408 x[16,14]     *              0             0             1 
   409 x[16,15]     *              0             0             1 
   410 x[16,17]     *              0             0             1 
   411 x[16,18]     *              0             0             1 
   412 x[16,19]     *              0             0             1 
   413 x[16,20]     *              0             0             1 
   414 x[16,21]     *              0             0             1 
   415 x[16,22]     *              0             0             1 
   416 x[16,23]     *              0             0             1 
   417 x[16,24]     *              0             0             1 
   418 x[16,25]     *              0             0             1 
   419 x[16,26]     *              0             0             1 
   420 x[17,1]      *              0             0             1 
   421 x[17,2]      *              0             0             1 
   422 x[17,3]      *              0             0             1 
   423 x[17,4]      *              0             0             1 
   424 x[17,5]      *              0             0             1 
   425 x[17,6]      *              0             0             1 
   426 x[17,7]      *              0             0             1 
   427 x[17,8]      *              0             0             1 
   428 x[17,9]      *              0             0             1 
   429 x[17,10]     *              0             0             1 
   430 x[17,11]     *              0             0             1 
   431 x[17,12]     *              0             0             1 
   432 x[17,13]     *              0             0             1 
   433 x[17,14]     *              0             0             1 
   434 x[17,15]     *              0             0             1 
   435 x[17,16]     *              0             0             1 
   436 x[17,18]     *              0             0             1 
   437 x[17,19]     *              0             0             1 
   438 x[17,20]     *              0             0             1 
   439 x[17,21]     *              0             0             1 
   440 x[17,22]     *              0             0             1 
   441 x[17,23]     *              0             0             1 
   442 x[17,24]     *              1             0             1 
   443 x[17,25]     *              0             0             1 
   444 x[17,26]     *              0             0             1 
   445 x[18,1]      *              0             0             1 
   446 x[18,2]      *              0             0             1 
   447 x[18,3]      *              0             0             1 
   448 x[18,4]      *              0             0             1 
   449 x[18,5]      *              0             0             1 
   450 x[18,6]      *              0             0             1 
   451 x[18,7]      *              0             0             1 
   452 x[18,8]      *              0             0             1 
   453 x[18,9]      *              0             0             1 
   454 x[18,10]     *              0             0             1 
   455 x[18,11]     *              0             0             1 
   456 x[18,12]     *              1             0             1 
   457 x[18,13]     *              0             0             1 
   458 x[18,14]     *              0             0             1 
   459 x[18,15]     *              0             0             1 
   460 x[18,16]     *              0             0             1 
   461 x[18,17]     *              0             0             1 
   462 x[18,19]     *              0             0             1 
   463 x[18,20]     *              0             0             1 
   464 x[18,21]     *              0             0             1 
   465 x[18,22]     *              0             0             1 
   466 x[18,23]     *              0             0             1 
   467 x[18,24]     *              0             0             1 
   468 x[18,25]     *              0             0             1 
   469 x[18,26]     *              0             0             1 
   470 x[19,2]      *              0             0             1 
   471 x[19,3]      *              0             0             1 
   472 x[19,4]      *              0             0             1 
   473 x[19,5]      *              1             0             1 
   474 x[19,6]      *              0             0             1 
   475 x[19,7]      *              0             0             1 
   476 x[19,8]      *              0             0             1 
   477 x[19,9]      *              0             0             1 
   478 x[19,10]     *              0             0             1 
   479 x[19,12]     *              0             0             1 
   480 x[19,13]     *              0             0             1 
   481 x[19,14]     *              0             0             1 
   482 x[19,15]     *              0             0             1 
   483 x[19,16]     *              0             0             1 
   484 x[19,17]     *              0             0             1 
   485 x[19,18]     *              0             0             1 
   486 x[19,20]     *              0             0             1 
   487 x[19,21]     *              0             0             1 
   488 x[19,22]     *              0             0             1 
   489 x[19,23]     *              0             0             1 
   490 x[19,24]     *              0             0             1 
   491 x[19,25]     *              0             0             1 
   492 x[19,26]     *              0             0             1 
   493 x[20,1]      *              0             0             1 
   494 x[20,2]      *              0             0             1 
   495 x[20,3]      *              0             0             1 
   496 x[20,4]      *              0             0             1 
   497 x[20,5]      *              0             0             1 
   498 x[20,6]      *              0             0             1 
   499 x[20,7]      *              0             0             1 
   500 x[20,8]      *              1             0             1 
   501 x[20,9]      *              0             0             1 
   502 x[20,10]     *              0             0             1 
   503 x[20,11]     *              0             0             1 
   504 x[20,12]     *              0             0             1 
   505 x[20,13]     *              0             0             1 
   506 x[20,14]     *              0             0             1 
   507 x[20,15]     *              0             0             1 
   508 x[20,16]     *              0             0             1 
   509 x[20,17]     *              0             0             1 
   510 x[20,18]     *              0             0             1 
   511 x[20,19]     *              0             0             1 
   512 x[20,21]     *              0             0             1 
   513 x[20,22]     *              0             0             1 
   514 x[20,23]     *              0             0             1 
   515 x[20,24]     *              0             0             1 
   516 x[20,25]     *              0             0             1 
   517 x[20,26]     *              0             0             1 
   518 x[21,1]      *              0             0             1 
   519 x[21,2]      *              0             0             1 
   520 x[21,3]      *              0             0             1 
   521 x[21,4]      *              0             0             1 
   522 x[21,5]      *              0             0             1 
   523 x[21,6]      *              0             0             1 
   524 x[21,7]      *              0             0             1 
   525 x[21,8]      *              0             0             1 
   526 x[21,9]      *              0             0             1 
   527 x[21,10]     *              0             0             1 
   528 x[21,11]     *              0             0             1 
   529 x[21,12]     *              0             0             1 
   530 x[21,13]     *              0             0             1 
   531 x[21,14]     *              0             0             1 
   532 x[21,15]     *              0             0             1 
   533 x[21,16]     *              0             0             1 
   534 x[21,17]     *              1             0             1 
   535 x[21,18]     *              0             0             1 
   536 x[21,19]     *              0             0             1 
   537 x[21,20]     *              0             0             1 
   538 x[21,22]     *              0             0             1 
   539 x[21,23]     *              0             0             1 
   540 x[21,24]     *              0             0             1 
   541 x[21,25]     *              0             0             1 
   542 x[21,26]     *              0             0             1 
   543 x[22,1]      *              0             0             1 
   544 x[22,2]      *              0             0             1 
   545 x[22,3]      *              0             0             1 
   546 x[22,4]      *              0             0             1 
   547 x[22,5]      *              0             0             1 
   548 x[22,6]      *              0             0             1 
   549 x[22,7]      *              0             0             1 
   550 x[22,8]      *              0             0             1 
   551 x[22,9]      *              0             0             1 
   552 x[22,10]     *              0             0             1 
   553 x[22,11]     *              0             0             1 
   554 x[22,12]     *              0             0             1 
   555 x[22,13]     *              0             0             1 
   556 x[22,14]     *              0             0             1 
   557 x[22,15]     *              0             0             1 
   558 x[22,16]     *              0             0             1 
   559 x[22,18]     *              0             0             1 
   560 x[22,19]     *              0             0             1 
   561 x[22,20]     *              0             0             1 
   562 x[22,21]     *              0             0             1 
   563 x[22,23]     *              1             0             1 
   564 x[22,24]     *              0             0             1 
   565 x[22,25]     *              0             0             1 
   566 x[22,26]     *              0             0             1 
   567 x[23,1]      *              0             0             1 
   568 x[23,2]      *              0             0             1 
   569 x[23,3]      *              0             0             1 
   570 x[23,4]      *              0             0             1 
   571 x[23,5]      *              0             0             1 
   572 x[23,6]      *              0             0             1 
   573 x[23,7]      *              0             0             1 
   574 x[23,8]      *              0             0             1 
   575 x[23,9]      *              0             0             1 
   576 x[23,10]     *              0             0             1 
   577 x[23,11]     *              0             0             1 
   578 x[23,12]     *              0             0             1 
   579 x[23,13]     *              0             0             1 
   580 x[23,14]     *              0             0             1 
   581 x[23,15]     *              0             0             1 
   582 x[23,16]     *              0             0             1 
   583 x[23,17]     *              0             0             1 
   584 x[23,18]     *              0             0             1 
   585 x[23,19]     *              0             0             1 
   586 x[23,20]     *              0             0             1 
   587 x[23,21]     *              0             0             1 
   588 x[23,22]     *              0             0             1 
   589 x[23,24]     *              0             0             1 
   590 x[23,25]     *              0             0             1 
   591 x[23,26]     *              1             0             1 
   592 x[24,1]      *              0             0             1 
   593 x[24,2]      *              0             0             1 
   594 x[24,3]      *              0             0             1 
   595 x[24,4]      *              0             0             1 
   596 x[24,5]      *              0             0             1 
   597 x[24,6]      *              0             0             1 
   598 x[24,7]      *              0             0             1 
   599 x[24,8]      *              0             0             1 
   600 x[24,9]      *              0             0             1 
   601 x[24,10]     *              0             0             1 
   602 x[24,11]     *              0             0             1 
   603 x[24,12]     *              0             0             1 
   604 x[24,13]     *              0             0             1 
   605 x[24,14]     *              0             0             1 
   606 x[24,15]     *              0             0             1 
   607 x[24,16]     *              0             0             1 
   608 x[24,17]     *              0             0             1 
   609 x[24,18]     *              0             0             1 
   610 x[24,19]     *              0             0             1 
   611 x[24,20]     *              0             0             1 
   612 x[24,21]     *              0             0             1 
   613 x[24,22]     *              0             0             1 
   614 x[24,23]     *              0             0             1 
   615 x[24,25]     *              1             0             1 
   616 x[24,26]     *              0             0             1 
   617 x[25,1]      *              0             0             1 
   618 x[25,2]      *              0             0             1 
   619 x[25,3]      *              0             0             1 
   620 x[25,4]      *              0             0             1 
   621 x[25,6]      *              0             0             1 
   622 x[25,7]      *              0             0             1 
   623 x[25,8]      *              0             0             1 
   624 x[25,9]      *              0             0             1 
   625 x[25,10]     *              0             0             1 
   626 x[25,11]     *              0             0             1 
   627 x[25,12]     *              0             0             1 
   628 x[25,13]     *              0             0             1 
   629 x[25,14]     *              0             0             1 
   630 x[25,15]     *              0             0             1 
   631 x[25,16]     *              0             0             1 
   632 x[25,17]     *              0             0             1 
   633 x[25,18]     *              1             0             1 
   634 x[25,19]     *              0             0             1 
   635 x[25,20]     *              0             0             1 
   636 x[25,21]     *              0             0             1 
   637 x[25,22]     *              0             0             1 
   638 x[25,23]     *              0             0             1 
   639 x[25,24]     *              0             0             1 
   640 x[25,26]     *              0             0             1 
   641 y[0]         *              1             0             1 
   642 y[1]         *              1             0             1 
   643 y[2]         *              1             0             1 
   644 y[3]         *              1             0             1 
   645 y[4]         *              1             0             1 
   646 y[5]         *              1             0             1 
   647 y[6]         *              1             0             1 
   648 y[7]         *              1             0             1 
   649 y[8]         *              1             0             1 
   650 y[9]         *              1             0             1 
   651 y[10]        *              1             0             1 
   652 y[11]        *              1             0             1 
   653 y[12]        *              1             0             1 
   654 y[13]        *              1             0             1 
   655 y[14]        *              1             0             1 
   656 y[15]        *              1             0             1 
   657 y[16]        *              1             0             1 
   658 y[17]        *              1             0             1 
   659 y[18]        *              1             0             1 
   660 y[19]        *              1             0             1 
   661 y[20]        *              1             0             1 
   662 y[21]        *              1             0             1 
   663 y[22]        *              1             0             1 
   664 y[23]        *              1             0             1 
   665 y[24]        *              1             0             1 
   666 y[25]        *              1             0             1 
   667 y[26]        *              1             0             1 
   668 u[1]                        1             0               
   669 u[0]                        0             0               
   670 u[2]                        8             0               
   671 u[3]                       22             0               
   672 u[4]                        5             0               
   673 u[5]                       11             0               
   674 u[6]                       14             0               
   675 u[7]                       12             0               
   676 u[8]                        4             0               
   677 u[9]                       23             0               
   678 u[10]                       9             0               
   679 u[11]                       6             0               
   680 u[12]                      20             0               
   681 u[13]                       2             0               
   682 u[14]                      13             0               
   683 u[15]                      21             0               
   684 u[16]                       7             0               
   685 u[17]                      16             0               
   686 u[18]                      19             0               
   687 u[19]                      10             0               
   688 u[20]                       3             0               
   689 u[21]                      15             0               
   690 u[22]                      24             0               
   691 u[23]                      25             0               
   692 u[24]                      17             0               
   693 u[25]                      18             0               
   694 u[26]                      26             0               

Integer feasibility conditions:

KKT.PE: max.abs.err = 3.55e-15 on row 1065
        max.rel.err = 9.11e-17 on row 1095
        High quality

KKT.PB: max.abs.err = 3.55e-15 on row 889
        max.rel.err = 1.32e-16 on row 889
        High quality

End of output
