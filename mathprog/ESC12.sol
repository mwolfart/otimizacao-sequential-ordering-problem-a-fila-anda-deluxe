Problem:    sop
Rows:       357
Columns:    174 (160 integer, 160 binary)
Non-zeros:  1338
Status:     INTEGER OPTIMAL
Objective:  totalcost = 1728 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 totalcost                1728                             
     2 VISIT_ALL[0]                1             1             = 
     3 VISIT_ALL[1]                1             1             = 
     4 VISIT_ALL[2]                1             1             = 
     5 VISIT_ALL[3]                1             1             = 
     6 VISIT_ALL[4]                1             1             = 
     7 VISIT_ALL[5]                1             1             = 
     8 VISIT_ALL[6]                1             1             = 
     9 VISIT_ALL[7]                1             1             = 
    10 VISIT_ALL[8]                1             1             = 
    11 VISIT_ALL[9]                1             1             = 
    12 VISIT_ALL[10]
                                   1             1             = 
    13 VISIT_ALL[11]
                                   1             1             = 
    14 VISIT_ALL[12]
                                   1             1             = 
    15 VISIT_ALL[13]
                                   1             1             = 
    16 ARC_NODE_IMPLICATION[0,1]
                                  -2                          -0 
    17 ARC_NODE_IMPLICATION[0,2]
                                  -2                          -0 
    18 ARC_NODE_IMPLICATION[0,3]
                                   0                          -0 
    19 ARC_NODE_IMPLICATION[0,4]
                                  -2                          -0 
    20 ARC_NODE_IMPLICATION[0,5]
                                  -2                          -0 
    21 ARC_NODE_IMPLICATION[0,6]
                                  -2                          -0 
    22 ARC_NODE_IMPLICATION[0,7]
                                  -2                          -0 
    23 ARC_NODE_IMPLICATION[0,8]
                                  -2                          -0 
    24 ARC_NODE_IMPLICATION[0,9]
                                  -2                          -0 
    25 ARC_NODE_IMPLICATION[0,10]
                                  -2                          -0 
    26 ARC_NODE_IMPLICATION[0,11]
                                  -2                          -0 
    27 ARC_NODE_IMPLICATION[0,12]
                                  -2                          -0 
    28 ARC_NODE_IMPLICATION[0,13]
                                  -2                          -0 
    29 ARC_NODE_IMPLICATION[1,2]
                                  -2                          -0 
    30 ARC_NODE_IMPLICATION[1,3]
                                  -2                          -0 
    31 ARC_NODE_IMPLICATION[1,4]
                                  -2                          -0 
    32 ARC_NODE_IMPLICATION[1,5]
                                  -2                          -0 
    33 ARC_NODE_IMPLICATION[1,6]
                                  -2                          -0 
    34 ARC_NODE_IMPLICATION[1,7]
                                   0                          -0 
    35 ARC_NODE_IMPLICATION[1,8]
                                  -2                          -0 
    36 ARC_NODE_IMPLICATION[1,9]
                                  -2                          -0 
    37 ARC_NODE_IMPLICATION[1,10]
                                  -2                          -0 
    38 ARC_NODE_IMPLICATION[1,11]
                                  -2                          -0 
    39 ARC_NODE_IMPLICATION[1,12]
                                  -2                          -0 
    40 ARC_NODE_IMPLICATION[1,13]
                                  -2                          -0 
    41 ARC_NODE_IMPLICATION[2,1]
                                  -2                          -0 
    42 ARC_NODE_IMPLICATION[2,3]
                                  -2                          -0 
    43 ARC_NODE_IMPLICATION[2,4]
                                  -2                          -0 
    44 ARC_NODE_IMPLICATION[2,5]
                                  -2                          -0 
    45 ARC_NODE_IMPLICATION[2,6]
                                   0                          -0 
    46 ARC_NODE_IMPLICATION[2,7]
                                  -2                          -0 
    47 ARC_NODE_IMPLICATION[2,8]
                                  -2                          -0 
    48 ARC_NODE_IMPLICATION[2,9]
                                  -2                          -0 
    49 ARC_NODE_IMPLICATION[2,10]
                                  -2                          -0 
    50 ARC_NODE_IMPLICATION[2,11]
                                  -2                          -0 
    51 ARC_NODE_IMPLICATION[2,12]
                                  -2                          -0 
    52 ARC_NODE_IMPLICATION[2,13]
                                  -2                          -0 
    53 ARC_NODE_IMPLICATION[3,1]
                                   0                          -0 
    54 ARC_NODE_IMPLICATION[3,2]
                                  -2                          -0 
    55 ARC_NODE_IMPLICATION[3,4]
                                  -2                          -0 
    56 ARC_NODE_IMPLICATION[3,5]
                                  -2                          -0 
    57 ARC_NODE_IMPLICATION[3,6]
                                  -2                          -0 
    58 ARC_NODE_IMPLICATION[3,7]
                                  -2                          -0 
    59 ARC_NODE_IMPLICATION[3,8]
                                  -2                          -0 
    60 ARC_NODE_IMPLICATION[3,9]
                                  -2                          -0 
    61 ARC_NODE_IMPLICATION[3,10]
                                  -2                          -0 
    62 ARC_NODE_IMPLICATION[3,11]
                                  -2                          -0 
    63 ARC_NODE_IMPLICATION[3,12]
                                  -2                          -0 
    64 ARC_NODE_IMPLICATION[3,13]
                                  -2                          -0 
    65 ARC_NODE_IMPLICATION[4,1]
                                  -2                          -0 
    66 ARC_NODE_IMPLICATION[4,2]
                                  -2                          -0 
    67 ARC_NODE_IMPLICATION[4,3]
                                  -2                          -0 
    68 ARC_NODE_IMPLICATION[4,5]
                                  -2                          -0 
    69 ARC_NODE_IMPLICATION[4,6]
                                  -2                          -0 
    70 ARC_NODE_IMPLICATION[4,7]
                                  -2                          -0 
    71 ARC_NODE_IMPLICATION[4,8]
                                   0                          -0 
    72 ARC_NODE_IMPLICATION[4,9]
                                  -2                          -0 
    73 ARC_NODE_IMPLICATION[4,10]
                                  -2                          -0 
    74 ARC_NODE_IMPLICATION[4,11]
                                  -2                          -0 
    75 ARC_NODE_IMPLICATION[4,12]
                                  -2                          -0 
    76 ARC_NODE_IMPLICATION[4,13]
                                  -2                          -0 
    77 ARC_NODE_IMPLICATION[5,1]
                                  -2                          -0 
    78 ARC_NODE_IMPLICATION[5,2]
                                  -2                          -0 
    79 ARC_NODE_IMPLICATION[5,3]
                                  -2                          -0 
    80 ARC_NODE_IMPLICATION[5,4]
                                  -2                          -0 
    81 ARC_NODE_IMPLICATION[5,6]
                                  -2                          -0 
    82 ARC_NODE_IMPLICATION[5,7]
                                  -2                          -0 
    83 ARC_NODE_IMPLICATION[5,8]
                                  -2                          -0 
    84 ARC_NODE_IMPLICATION[5,9]
                                  -2                          -0 
    85 ARC_NODE_IMPLICATION[5,10]
                                  -2                          -0 
    86 ARC_NODE_IMPLICATION[5,11]
                                  -2                          -0 
    87 ARC_NODE_IMPLICATION[5,12]
                                   0                          -0 
    88 ARC_NODE_IMPLICATION[5,13]
                                  -2                          -0 
    89 ARC_NODE_IMPLICATION[6,5]
                                  -2                          -0 
    90 ARC_NODE_IMPLICATION[6,7]
                                  -2                          -0 
    91 ARC_NODE_IMPLICATION[6,8]
                                  -2                          -0 
    92 ARC_NODE_IMPLICATION[6,9]
                                  -2                          -0 
    93 ARC_NODE_IMPLICATION[6,10]
                                  -2                          -0 
    94 ARC_NODE_IMPLICATION[6,11]
                                   0                          -0 
    95 ARC_NODE_IMPLICATION[6,12]
                                  -2                          -0 
    96 ARC_NODE_IMPLICATION[6,13]
                                  -2                          -0 
    97 ARC_NODE_IMPLICATION[7,1]
                                  -2                          -0 
    98 ARC_NODE_IMPLICATION[7,2]
                                  -2                          -0 
    99 ARC_NODE_IMPLICATION[7,3]
                                  -2                          -0 
   100 ARC_NODE_IMPLICATION[7,4]
                                  -2                          -0 
   101 ARC_NODE_IMPLICATION[7,5]
                                  -2                          -0 
   102 ARC_NODE_IMPLICATION[7,6]
                                  -2                          -0 
   103 ARC_NODE_IMPLICATION[7,8]
                                  -2                          -0 
   104 ARC_NODE_IMPLICATION[7,9]
                                   0                          -0 
   105 ARC_NODE_IMPLICATION[7,10]
                                  -2                          -0 
   106 ARC_NODE_IMPLICATION[7,11]
                                  -2                          -0 
   107 ARC_NODE_IMPLICATION[7,12]
                                  -2                          -0 
   108 ARC_NODE_IMPLICATION[7,13]
                                  -2                          -0 
   109 ARC_NODE_IMPLICATION[8,1]
                                  -2                          -0 
   110 ARC_NODE_IMPLICATION[8,2]
                                   0                          -0 
   111 ARC_NODE_IMPLICATION[8,3]
                                  -2                          -0 
   112 ARC_NODE_IMPLICATION[8,4]
                                  -2                          -0 
   113 ARC_NODE_IMPLICATION[8,5]
                                  -2                          -0 
   114 ARC_NODE_IMPLICATION[8,6]
                                  -2                          -0 
   115 ARC_NODE_IMPLICATION[8,7]
                                  -2                          -0 
   116 ARC_NODE_IMPLICATION[8,9]
                                  -2                          -0 
   117 ARC_NODE_IMPLICATION[8,10]
                                  -2                          -0 
   118 ARC_NODE_IMPLICATION[8,11]
                                  -2                          -0 
   119 ARC_NODE_IMPLICATION[8,12]
                                  -2                          -0 
   120 ARC_NODE_IMPLICATION[8,13]
                                  -2                          -0 
   121 ARC_NODE_IMPLICATION[9,1]
                                  -2                          -0 
   122 ARC_NODE_IMPLICATION[9,2]
                                  -2                          -0 
   123 ARC_NODE_IMPLICATION[9,3]
                                  -2                          -0 
   124 ARC_NODE_IMPLICATION[9,4]
                                  -2                          -0 
   125 ARC_NODE_IMPLICATION[9,5]
                                  -2                          -0 
   126 ARC_NODE_IMPLICATION[9,6]
                                  -2                          -0 
   127 ARC_NODE_IMPLICATION[9,7]
                                  -2                          -0 
   128 ARC_NODE_IMPLICATION[9,8]
                                  -2                          -0 
   129 ARC_NODE_IMPLICATION[9,10]
                                   0                          -0 
   130 ARC_NODE_IMPLICATION[9,11]
                                  -2                          -0 
   131 ARC_NODE_IMPLICATION[9,12]
                                  -2                          -0 
   132 ARC_NODE_IMPLICATION[9,13]
                                  -2                          -0 
   133 ARC_NODE_IMPLICATION[10,1]
                                  -2                          -0 
   134 ARC_NODE_IMPLICATION[10,2]
                                  -2                          -0 
   135 ARC_NODE_IMPLICATION[10,3]
                                  -2                          -0 
   136 ARC_NODE_IMPLICATION[10,4]
                                   0                          -0 
   137 ARC_NODE_IMPLICATION[10,5]
                                  -2                          -0 
   138 ARC_NODE_IMPLICATION[10,6]
                                  -2                          -0 
   139 ARC_NODE_IMPLICATION[10,7]
                                  -2                          -0 
   140 ARC_NODE_IMPLICATION[10,8]
                                  -2                          -0 
   141 ARC_NODE_IMPLICATION[10,9]
                                  -2                          -0 
   142 ARC_NODE_IMPLICATION[10,11]
                                  -2                          -0 
   143 ARC_NODE_IMPLICATION[10,12]
                                  -2                          -0 
   144 ARC_NODE_IMPLICATION[10,13]
                                  -2                          -0 
   145 ARC_NODE_IMPLICATION[11,1]
                                  -2                          -0 
   146 ARC_NODE_IMPLICATION[11,2]
                                  -2                          -0 
   147 ARC_NODE_IMPLICATION[11,3]
                                  -2                          -0 
   148 ARC_NODE_IMPLICATION[11,4]
                                  -2                          -0 
   149 ARC_NODE_IMPLICATION[11,5]
                                   0                          -0 
   150 ARC_NODE_IMPLICATION[11,6]
                                  -2                          -0 
   151 ARC_NODE_IMPLICATION[11,7]
                                  -2                          -0 
   152 ARC_NODE_IMPLICATION[11,8]
                                  -2                          -0 
   153 ARC_NODE_IMPLICATION[11,9]
                                  -2                          -0 
   154 ARC_NODE_IMPLICATION[11,10]
                                  -2                          -0 
   155 ARC_NODE_IMPLICATION[11,12]
                                  -2                          -0 
   156 ARC_NODE_IMPLICATION[11,13]
                                  -2                          -0 
   157 ARC_NODE_IMPLICATION[12,5]
                                  -2                          -0 
   158 ARC_NODE_IMPLICATION[12,9]
                                  -2                          -0 
   159 ARC_NODE_IMPLICATION[12,10]
                                  -2                          -0 
   160 ARC_NODE_IMPLICATION[12,11]
                                  -2                          -0 
   161 ARC_NODE_IMPLICATION[12,13]
                                   0                          -0 
   162 SINGLE_ENTRY[1]
                                   1             1             = 
   163 SINGLE_ENTRY[2]
                                   1             1             = 
   164 SINGLE_ENTRY[3]
                                   1             1             = 
   165 SINGLE_ENTRY[4]
                                   1             1             = 
   166 SINGLE_ENTRY[5]
                                   1             1             = 
   167 SINGLE_ENTRY[6]
                                   1             1             = 
   168 SINGLE_ENTRY[7]
                                   1             1             = 
   169 SINGLE_ENTRY[8]
                                   1             1             = 
   170 SINGLE_ENTRY[9]
                                   1             1             = 
   171 SINGLE_ENTRY[10]
                                   1             1             = 
   172 SINGLE_ENTRY[11]
                                   1             1             = 
   173 SINGLE_ENTRY[12]
                                   1             1             = 
   174 SINGLE_ENTRY[13]
                                   1             1             = 
   175 SINGLE_EXIT[0]
                                   1             1             = 
   176 SINGLE_EXIT[1]
                                   1             1             = 
   177 SINGLE_EXIT[2]
                                   1             1             = 
   178 SINGLE_EXIT[3]
                                   1             1             = 
   179 SINGLE_EXIT[4]
                                   1             1             = 
   180 SINGLE_EXIT[5]
                                   1             1             = 
   181 SINGLE_EXIT[6]
                                   1             1             = 
   182 SINGLE_EXIT[7]
                                   1             1             = 
   183 SINGLE_EXIT[8]
                                   1             1             = 
   184 SINGLE_EXIT[9]
                                   1             1             = 
   185 SINGLE_EXIT[10]
                                   1             1             = 
   186 SINGLE_EXIT[11]
                                   1             1             = 
   187 SINGLE_EXIT[12]
                                   1             1             = 
   188 PRECEDE[1,0]                1            -0               
   189 PRECEDE[2,0]                7            -0               
   190 PRECEDE[3,0]                0            -0               
   191 PRECEDE[4,0]                5            -0               
   192 PRECEDE[5,0]               13            -0               
   193 PRECEDE[6,0]               11            -0               
   194 PRECEDE[6,1]               10            -0               
   195 PRECEDE[6,2]                4            -0               
   196 PRECEDE[6,3]               11            -0               
   197 PRECEDE[6,4]                6            -0               
   198 PRECEDE[7,0]                2            -0               
   199 PRECEDE[8,0]                6            -0               
   200 PRECEDE[9,0]                3            -0               
   201 PRECEDE[10,0]
                                   4            -0               
   202 PRECEDE[11,0]
                                  12            -0               
   203 PRECEDE[12,0]
                                  14            -0               
   204 PRECEDE[12,1]
                                  13            -0               
   205 PRECEDE[12,2]
                                   7            -0               
   206 PRECEDE[12,3]
                                  14            -0               
   207 PRECEDE[12,4]
                                   9            -0               
   208 PRECEDE[12,6]
                                   3            -0               
   209 PRECEDE[12,7]
                                  12            -0               
   210 PRECEDE[12,8]
                                   8            -0               
   211 PRECEDE[13,0]
                                  15            -0               
   212 PRECEDE[13,1]
                                  14            -0               
   213 PRECEDE[13,2]
                                   8            -0               
   214 PRECEDE[13,3]
                                  15            -0               
   215 PRECEDE[13,4]
                                  10            -0               
   216 PRECEDE[13,5]
                                   2            -0               
   217 PRECEDE[13,6]
                                   4            -0               
   218 PRECEDE[13,7]
                                  13            -0               
   219 PRECEDE[13,8]
                                   9            -0               
   220 PRECEDE[13,9]
                                  12            -0               
   221 PRECEDE[13,10]
                                  11            -0               
   222 PRECEDE[13,11]
                                   3            -0               
   223 PRECEDE[13,12]
                                   1            -0               
   224 FIRST_NODE_INDEX
                                   1             1             = 
   225 INDEX_ARC_IMPLICATION[1,2]
                                  -6                          13 
   226 INDEX_ARC_IMPLICATION[1,3]
                                   1                          13 
   227 INDEX_ARC_IMPLICATION[1,4]
                                  -4                          13 
   228 INDEX_ARC_IMPLICATION[1,5]
                                 -12                          13 
   229 INDEX_ARC_IMPLICATION[1,6]
                                 -10                          13 
   230 INDEX_ARC_IMPLICATION[1,7]
                                  13                          13 
   231 INDEX_ARC_IMPLICATION[1,8]
                                  -5                          13 
   232 INDEX_ARC_IMPLICATION[1,9]
                                  -2                          13 
   233 INDEX_ARC_IMPLICATION[1,10]
                                  -3                          13 
   234 INDEX_ARC_IMPLICATION[1,11]
                                 -11                          13 
   235 INDEX_ARC_IMPLICATION[1,12]
                                 -13                          13 
   236 INDEX_ARC_IMPLICATION[1,13]
                                 -14                          13 
   237 INDEX_ARC_IMPLICATION[2,1]
                                   6                          13 
   238 INDEX_ARC_IMPLICATION[2,3]
                                   7                          13 
   239 INDEX_ARC_IMPLICATION[2,4]
                                   2                          13 
   240 INDEX_ARC_IMPLICATION[2,5]
                                  -6                          13 
   241 INDEX_ARC_IMPLICATION[2,6]
                                  10                          13 
   242 INDEX_ARC_IMPLICATION[2,7]
                                   5                          13 
   243 INDEX_ARC_IMPLICATION[2,8]
                                   1                          13 
   244 INDEX_ARC_IMPLICATION[2,9]
                                   4                          13 
   245 INDEX_ARC_IMPLICATION[2,10]
                                   3                          13 
   246 INDEX_ARC_IMPLICATION[2,11]
                                  -5                          13 
   247 INDEX_ARC_IMPLICATION[2,12]
                                  -7                          13 
   248 INDEX_ARC_IMPLICATION[2,13]
                                  -8                          13 
   249 INDEX_ARC_IMPLICATION[3,1]
                                  13                          13 
   250 INDEX_ARC_IMPLICATION[3,2]
                                  -7                          13 
   251 INDEX_ARC_IMPLICATION[3,4]
                                  -5                          13 
   252 INDEX_ARC_IMPLICATION[3,5]
                                 -13                          13 
   253 INDEX_ARC_IMPLICATION[3,6]
                                 -11                          13 
   254 INDEX_ARC_IMPLICATION[3,7]
                                  -2                          13 
   255 INDEX_ARC_IMPLICATION[3,8]
                                  -6                          13 
   256 INDEX_ARC_IMPLICATION[3,9]
                                  -3                          13 
   257 INDEX_ARC_IMPLICATION[3,10]
                                  -4                          13 
   258 INDEX_ARC_IMPLICATION[3,11]
                                 -12                          13 
   259 INDEX_ARC_IMPLICATION[3,12]
                                 -14                          13 
   260 INDEX_ARC_IMPLICATION[3,13]
                                 -15                          13 
   261 INDEX_ARC_IMPLICATION[4,1]
                                   4                          13 
   262 INDEX_ARC_IMPLICATION[4,2]
                                  -2                          13 
   263 INDEX_ARC_IMPLICATION[4,3]
                                   5                          13 
   264 INDEX_ARC_IMPLICATION[4,5]
                                  -8                          13 
   265 INDEX_ARC_IMPLICATION[4,6]
                                  -6                          13 
   266 INDEX_ARC_IMPLICATION[4,7]
                                   3                          13 
   267 INDEX_ARC_IMPLICATION[4,8]
                                  13                          13 
   268 INDEX_ARC_IMPLICATION[4,9]
                                   2                          13 
   269 INDEX_ARC_IMPLICATION[4,10]
                                   1                          13 
   270 INDEX_ARC_IMPLICATION[4,11]
                                  -7                          13 
   271 INDEX_ARC_IMPLICATION[4,12]
                                  -9                          13 
   272 INDEX_ARC_IMPLICATION[4,13]
                                 -10                          13 
   273 INDEX_ARC_IMPLICATION[5,1]
                                  12                          13 
   274 INDEX_ARC_IMPLICATION[5,2]
                                   6                          13 
   275 INDEX_ARC_IMPLICATION[5,3]
                                  13                          13 
   276 INDEX_ARC_IMPLICATION[5,4]
                                   8                          13 
   277 INDEX_ARC_IMPLICATION[5,6]
                                   2                          13 
   278 INDEX_ARC_IMPLICATION[5,7]
                                  11                          13 
   279 INDEX_ARC_IMPLICATION[5,8]
                                   7                          13 
   280 INDEX_ARC_IMPLICATION[5,9]
                                  10                          13 
   281 INDEX_ARC_IMPLICATION[5,10]
                                   9                          13 
   282 INDEX_ARC_IMPLICATION[5,11]
                                   1                          13 
   283 INDEX_ARC_IMPLICATION[5,12]
                                  13                          13 
   284 INDEX_ARC_IMPLICATION[5,13]
                                  -2                          13 
   285 INDEX_ARC_IMPLICATION[6,5]
                                  -2                          13 
   286 INDEX_ARC_IMPLICATION[6,7]
                                   9                          13 
   287 INDEX_ARC_IMPLICATION[6,8]
                                   5                          13 
   288 INDEX_ARC_IMPLICATION[6,9]
                                   8                          13 
   289 INDEX_ARC_IMPLICATION[6,10]
                                   7                          13 
   290 INDEX_ARC_IMPLICATION[6,11]
                                  13                          13 
   291 INDEX_ARC_IMPLICATION[6,12]
                                  -3                          13 
   292 INDEX_ARC_IMPLICATION[6,13]
                                  -4                          13 
   293 INDEX_ARC_IMPLICATION[7,1]
                                   1                          13 
   294 INDEX_ARC_IMPLICATION[7,2]
                                  -5                          13 
   295 INDEX_ARC_IMPLICATION[7,3]
                                   2                          13 
   296 INDEX_ARC_IMPLICATION[7,4]
                                  -3                          13 
   297 INDEX_ARC_IMPLICATION[7,5]
                                 -11                          13 
   298 INDEX_ARC_IMPLICATION[7,6]
                                  -9                          13 
   299 INDEX_ARC_IMPLICATION[7,8]
                                  -4                          13 
   300 INDEX_ARC_IMPLICATION[7,9]
                                  13                          13 
   301 INDEX_ARC_IMPLICATION[7,10]
                                  -2                          13 
   302 INDEX_ARC_IMPLICATION[7,11]
                                 -10                          13 
   303 INDEX_ARC_IMPLICATION[7,12]
                                 -12                          13 
   304 INDEX_ARC_IMPLICATION[7,13]
                                 -13                          13 
   305 INDEX_ARC_IMPLICATION[8,1]
                                   5                          13 
   306 INDEX_ARC_IMPLICATION[8,2]
                                  13                          13 
   307 INDEX_ARC_IMPLICATION[8,3]
                                   6                          13 
   308 INDEX_ARC_IMPLICATION[8,4]
                                   1                          13 
   309 INDEX_ARC_IMPLICATION[8,5]
                                  -7                          13 
   310 INDEX_ARC_IMPLICATION[8,6]
                                  -5                          13 
   311 INDEX_ARC_IMPLICATION[8,7]
                                   4                          13 
   312 INDEX_ARC_IMPLICATION[8,9]
                                   3                          13 
   313 INDEX_ARC_IMPLICATION[8,10]
                                   2                          13 
   314 INDEX_ARC_IMPLICATION[8,11]
                                  -6                          13 
   315 INDEX_ARC_IMPLICATION[8,12]
                                  -8                          13 
   316 INDEX_ARC_IMPLICATION[8,13]
                                  -9                          13 
   317 INDEX_ARC_IMPLICATION[9,1]
                                   2                          13 
   318 INDEX_ARC_IMPLICATION[9,2]
                                  -4                          13 
   319 INDEX_ARC_IMPLICATION[9,3]
                                   3                          13 
   320 INDEX_ARC_IMPLICATION[9,4]
                                  -2                          13 
   321 INDEX_ARC_IMPLICATION[9,5]
                                 -10                          13 
   322 INDEX_ARC_IMPLICATION[9,6]
                                  -8                          13 
   323 INDEX_ARC_IMPLICATION[9,7]
                                   1                          13 
   324 INDEX_ARC_IMPLICATION[9,8]
                                  -3                          13 
   325 INDEX_ARC_IMPLICATION[9,10]
                                  13                          13 
   326 INDEX_ARC_IMPLICATION[9,11]
                                  -9                          13 
   327 INDEX_ARC_IMPLICATION[9,12]
                                 -11                          13 
   328 INDEX_ARC_IMPLICATION[9,13]
                                 -12                          13 
   329 INDEX_ARC_IMPLICATION[10,1]
                                   3                          13 
   330 INDEX_ARC_IMPLICATION[10,2]
                                  -3                          13 
   331 INDEX_ARC_IMPLICATION[10,3]
                                   4                          13 
   332 INDEX_ARC_IMPLICATION[10,4]
                                  13                          13 
   333 INDEX_ARC_IMPLICATION[10,5]
                                  -9                          13 
   334 INDEX_ARC_IMPLICATION[10,6]
                                  -7                          13 
   335 INDEX_ARC_IMPLICATION[10,7]
                                   2                          13 
   336 INDEX_ARC_IMPLICATION[10,8]
                                  -2                          13 
   337 INDEX_ARC_IMPLICATION[10,9]
                                   1                          13 
   338 INDEX_ARC_IMPLICATION[10,11]
                                  -8                          13 
   339 INDEX_ARC_IMPLICATION[10,12]
                                 -10                          13 
   340 INDEX_ARC_IMPLICATION[10,13]
                                 -11                          13 
   341 INDEX_ARC_IMPLICATION[11,1]
                                  11                          13 
   342 INDEX_ARC_IMPLICATION[11,2]
                                   5                          13 
   343 INDEX_ARC_IMPLICATION[11,3]
                                  12                          13 
   344 INDEX_ARC_IMPLICATION[11,4]
                                   7                          13 
   345 INDEX_ARC_IMPLICATION[11,5]
                                  13                          13 
   346 INDEX_ARC_IMPLICATION[11,6]
                                   1                          13 
   347 INDEX_ARC_IMPLICATION[11,7]
                                  10                          13 
   348 INDEX_ARC_IMPLICATION[11,8]
                                   6                          13 
   349 INDEX_ARC_IMPLICATION[11,9]
                                   9                          13 
   350 INDEX_ARC_IMPLICATION[11,10]
                                   8                          13 
   351 INDEX_ARC_IMPLICATION[11,12]
                                  -2                          13 
   352 INDEX_ARC_IMPLICATION[11,13]
                                  -3                          13 
   353 INDEX_ARC_IMPLICATION[12,5]
                                   1                          13 
   354 INDEX_ARC_IMPLICATION[12,9]
                                  11                          13 
   355 INDEX_ARC_IMPLICATION[12,10]
                                  10                          13 
   356 INDEX_ARC_IMPLICATION[12,11]
                                   2                          13 
   357 INDEX_ARC_IMPLICATION[12,13]
                                  13                          13 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[0,1]       *              0             0             1 
     2 x[0,2]       *              0             0             1 
     3 x[0,3]       *              1             0             1 
     4 x[0,4]       *              0             0             1 
     5 x[0,5]       *              0             0             1 
     6 x[0,6]       *              0             0             1 
     7 x[0,7]       *              0             0             1 
     8 x[0,8]       *              0             0             1 
     9 x[0,9]       *              0             0             1 
    10 x[0,10]      *              0             0             1 
    11 x[0,11]      *              0             0             1 
    12 x[0,12]      *              0             0             1 
    13 x[0,13]      *              0             0             1 
    14 x[1,2]       *              0             0             1 
    15 x[1,3]       *              0             0             1 
    16 x[1,4]       *              0             0             1 
    17 x[1,5]       *              0             0             1 
    18 x[1,6]       *              0             0             1 
    19 x[1,7]       *              1             0             1 
    20 x[1,8]       *              0             0             1 
    21 x[1,9]       *              0             0             1 
    22 x[1,10]      *              0             0             1 
    23 x[1,11]      *              0             0             1 
    24 x[1,12]      *              0             0             1 
    25 x[1,13]      *              0             0             1 
    26 x[2,1]       *              0             0             1 
    27 x[2,3]       *              0             0             1 
    28 x[2,4]       *              0             0             1 
    29 x[2,5]       *              0             0             1 
    30 x[2,6]       *              1             0             1 
    31 x[2,7]       *              0             0             1 
    32 x[2,8]       *              0             0             1 
    33 x[2,9]       *              0             0             1 
    34 x[2,10]      *              0             0             1 
    35 x[2,11]      *              0             0             1 
    36 x[2,12]      *              0             0             1 
    37 x[2,13]      *              0             0             1 
    38 x[3,1]       *              1             0             1 
    39 x[3,2]       *              0             0             1 
    40 x[3,4]       *              0             0             1 
    41 x[3,5]       *              0             0             1 
    42 x[3,6]       *              0             0             1 
    43 x[3,7]       *              0             0             1 
    44 x[3,8]       *              0             0             1 
    45 x[3,9]       *              0             0             1 
    46 x[3,10]      *              0             0             1 
    47 x[3,11]      *              0             0             1 
    48 x[3,12]      *              0             0             1 
    49 x[3,13]      *              0             0             1 
    50 x[4,1]       *              0             0             1 
    51 x[4,2]       *              0             0             1 
    52 x[4,3]       *              0             0             1 
    53 x[4,5]       *              0             0             1 
    54 x[4,6]       *              0             0             1 
    55 x[4,7]       *              0             0             1 
    56 x[4,8]       *              1             0             1 
    57 x[4,9]       *              0             0             1 
    58 x[4,10]      *              0             0             1 
    59 x[4,11]      *              0             0             1 
    60 x[4,12]      *              0             0             1 
    61 x[4,13]      *              0             0             1 
    62 x[5,1]       *              0             0             1 
    63 x[5,2]       *              0             0             1 
    64 x[5,3]       *              0             0             1 
    65 x[5,4]       *              0             0             1 
    66 x[5,6]       *              0             0             1 
    67 x[5,7]       *              0             0             1 
    68 x[5,8]       *              0             0             1 
    69 x[5,9]       *              0             0             1 
    70 x[5,10]      *              0             0             1 
    71 x[5,11]      *              0             0             1 
    72 x[5,12]      *              1             0             1 
    73 x[5,13]      *              0             0             1 
    74 x[6,5]       *              0             0             1 
    75 x[6,7]       *              0             0             1 
    76 x[6,8]       *              0             0             1 
    77 x[6,9]       *              0             0             1 
    78 x[6,10]      *              0             0             1 
    79 x[6,11]      *              1             0             1 
    80 x[6,12]      *              0             0             1 
    81 x[6,13]      *              0             0             1 
    82 x[7,1]       *              0             0             1 
    83 x[7,2]       *              0             0             1 
    84 x[7,3]       *              0             0             1 
    85 x[7,4]       *              0             0             1 
    86 x[7,5]       *              0             0             1 
    87 x[7,6]       *              0             0             1 
    88 x[7,8]       *              0             0             1 
    89 x[7,9]       *              1             0             1 
    90 x[7,10]      *              0             0             1 
    91 x[7,11]      *              0             0             1 
    92 x[7,12]      *              0             0             1 
    93 x[7,13]      *              0             0             1 
    94 x[8,1]       *              0             0             1 
    95 x[8,2]       *              1             0             1 
    96 x[8,3]       *              0             0             1 
    97 x[8,4]       *              0             0             1 
    98 x[8,5]       *              0             0             1 
    99 x[8,6]       *              0             0             1 
   100 x[8,7]       *              0             0             1 
   101 x[8,9]       *              0             0             1 
   102 x[8,10]      *              0             0             1 
   103 x[8,11]      *              0             0             1 
   104 x[8,12]      *              0             0             1 
   105 x[8,13]      *              0             0             1 
   106 x[9,1]       *              0             0             1 
   107 x[9,2]       *              0             0             1 
   108 x[9,3]       *              0             0             1 
   109 x[9,4]       *              0             0             1 
   110 x[9,5]       *              0             0             1 
   111 x[9,6]       *              0             0             1 
   112 x[9,7]       *              0             0             1 
   113 x[9,8]       *              0             0             1 
   114 x[9,10]      *              1             0             1 
   115 x[9,11]      *              0             0             1 
   116 x[9,12]      *              0             0             1 
   117 x[9,13]      *              0             0             1 
   118 x[10,1]      *              0             0             1 
   119 x[10,2]      *              0             0             1 
   120 x[10,3]      *              0             0             1 
   121 x[10,4]      *              1             0             1 
   122 x[10,5]      *              0             0             1 
   123 x[10,6]      *              0             0             1 
   124 x[10,7]      *              0             0             1 
   125 x[10,8]      *              0             0             1 
   126 x[10,9]      *              0             0             1 
   127 x[10,11]     *              0             0             1 
   128 x[10,12]     *              0             0             1 
   129 x[10,13]     *              0             0             1 
   130 x[11,1]      *              0             0             1 
   131 x[11,2]      *              0             0             1 
   132 x[11,3]      *              0             0             1 
   133 x[11,4]      *              0             0             1 
   134 x[11,5]      *              1             0             1 
   135 x[11,6]      *              0             0             1 
   136 x[11,7]      *              0             0             1 
   137 x[11,8]      *              0             0             1 
   138 x[11,9]      *              0             0             1 
   139 x[11,10]     *              0             0             1 
   140 x[11,12]     *              0             0             1 
   141 x[11,13]     *              0             0             1 
   142 x[12,5]      *              0             0             1 
   143 x[12,9]      *              0             0             1 
   144 x[12,10]     *              0             0             1 
   145 x[12,11]     *              0             0             1 
   146 x[12,13]     *              1             0             1 
   147 y[0]         *              1             0             1 
   148 y[1]         *              1             0             1 
   149 y[2]         *              1             0             1 
   150 y[3]         *              1             0             1 
   151 y[4]         *              1             0             1 
   152 y[5]         *              1             0             1 
   153 y[6]         *              1             0             1 
   154 y[7]         *              1             0             1 
   155 y[8]         *              1             0             1 
   156 y[9]         *              1             0             1 
   157 y[10]        *              1             0             1 
   158 y[11]        *              1             0             1 
   159 y[12]        *              1             0             1 
   160 y[13]        *              1             0             1 
   161 u[1]                        1             0               
   162 u[0]                        0             0               
   163 u[2]                        7             0               
   164 u[3]                        0             0               
   165 u[4]                        5             0               
   166 u[5]                       13             0               
   167 u[6]                       11             0               
   168 u[7]                        2             0               
   169 u[8]                        6             0               
   170 u[9]                        3             0               
   171 u[10]                       4             0               
   172 u[11]                      12             0               
   173 u[12]                      14             0               
   174 u[13]                      15             0               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 1.78e-15 on row 275
        max.rel.err = 1.27e-16 on row 275
        High quality

End of output
