Problem:    sop
Rows:       141
Columns:    68 (59 integer, 59 binary)
Non-zeros:  463
Status:     INTEGER OPTIMAL
Objective:  totalcost = 2125 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 totalcost                2125                             
     2 VISIT_ALL[0]                1             1             = 
     3 VISIT_ALL[1]                1             1             = 
     4 VISIT_ALL[2]                1             1             = 
     5 VISIT_ALL[3]                1             1             = 
     6 VISIT_ALL[4]                1             1             = 
     7 VISIT_ALL[5]                1             1             = 
     8 VISIT_ALL[6]                1             1             = 
     9 VISIT_ALL[7]                1             1             = 
    10 VISIT_ALL[8]                1             1             = 
    11 ARC_NODE_IMPLICATION[0,1]
                                   0                          -0 
    12 ARC_NODE_IMPLICATION[0,2]
                                  -2                          -0 
    13 ARC_NODE_IMPLICATION[0,3]
                                  -2                          -0 
    14 ARC_NODE_IMPLICATION[0,4]
                                  -2                          -0 
    15 ARC_NODE_IMPLICATION[0,5]
                                  -2                          -0 
    16 ARC_NODE_IMPLICATION[0,6]
                                  -2                          -0 
    17 ARC_NODE_IMPLICATION[0,7]
                                  -2                          -0 
    18 ARC_NODE_IMPLICATION[0,8]
                                  -2                          -0 
    19 ARC_NODE_IMPLICATION[1,2]
                                  -2                          -0 
    20 ARC_NODE_IMPLICATION[1,3]
                                  -2                          -0 
    21 ARC_NODE_IMPLICATION[1,4]
                                   0                          -0 
    22 ARC_NODE_IMPLICATION[1,5]
                                  -2                          -0 
    23 ARC_NODE_IMPLICATION[1,6]
                                  -2                          -0 
    24 ARC_NODE_IMPLICATION[1,7]
                                  -2                          -0 
    25 ARC_NODE_IMPLICATION[1,8]
                                  -2                          -0 
    26 ARC_NODE_IMPLICATION[2,1]
                                  -2                          -0 
    27 ARC_NODE_IMPLICATION[2,3]
                                  -2                          -0 
    28 ARC_NODE_IMPLICATION[2,4]
                                  -2                          -0 
    29 ARC_NODE_IMPLICATION[2,5]
                                  -2                          -0 
    30 ARC_NODE_IMPLICATION[2,6]
                                   0                          -0 
    31 ARC_NODE_IMPLICATION[2,7]
                                  -2                          -0 
    32 ARC_NODE_IMPLICATION[2,8]
                                  -2                          -0 
    33 ARC_NODE_IMPLICATION[3,1]
                                  -2                          -0 
    34 ARC_NODE_IMPLICATION[3,2]
                                  -2                          -0 
    35 ARC_NODE_IMPLICATION[3,4]
                                  -2                          -0 
    36 ARC_NODE_IMPLICATION[3,5]
                                  -2                          -0 
    37 ARC_NODE_IMPLICATION[3,6]
                                  -2                          -0 
    38 ARC_NODE_IMPLICATION[3,7]
                                  -2                          -0 
    39 ARC_NODE_IMPLICATION[3,8]
                                   0                          -0 
    40 ARC_NODE_IMPLICATION[4,2]
                                  -2                          -0 
    41 ARC_NODE_IMPLICATION[4,3]
                                  -2                          -0 
    42 ARC_NODE_IMPLICATION[4,5]
                                  -2                          -0 
    43 ARC_NODE_IMPLICATION[4,6]
                                  -2                          -0 
    44 ARC_NODE_IMPLICATION[4,7]
                                   0                          -0 
    45 ARC_NODE_IMPLICATION[4,8]
                                  -2                          -0 
    46 ARC_NODE_IMPLICATION[5,2]
                                  -2                          -0 
    47 ARC_NODE_IMPLICATION[5,3]
                                   0                          -0 
    48 ARC_NODE_IMPLICATION[5,8]
                                  -2                          -0 
    49 ARC_NODE_IMPLICATION[6,2]
                                  -2                          -0 
    50 ARC_NODE_IMPLICATION[6,3]
                                  -2                          -0 
    51 ARC_NODE_IMPLICATION[6,4]
                                  -2                          -0 
    52 ARC_NODE_IMPLICATION[6,5]
                                   0                          -0 
    53 ARC_NODE_IMPLICATION[6,7]
                                  -2                          -0 
    54 ARC_NODE_IMPLICATION[6,8]
                                  -2                          -0 
    55 ARC_NODE_IMPLICATION[7,2]
                                   0                          -0 
    56 ARC_NODE_IMPLICATION[7,3]
                                  -2                          -0 
    57 ARC_NODE_IMPLICATION[7,4]
                                  -2                          -0 
    58 ARC_NODE_IMPLICATION[7,5]
                                  -2                          -0 
    59 ARC_NODE_IMPLICATION[7,6]
                                  -2                          -0 
    60 ARC_NODE_IMPLICATION[7,8]
                                  -2                          -0 
    61 SINGLE_ENTRY[1]
                                   1             1             = 
    62 SINGLE_ENTRY[2]
                                   1             1             = 
    63 SINGLE_ENTRY[3]
                                   1             1             = 
    64 SINGLE_ENTRY[4]
                                   1             1             = 
    65 SINGLE_ENTRY[5]
                                   1             1             = 
    66 SINGLE_ENTRY[6]
                                   1             1             = 
    67 SINGLE_ENTRY[7]
                                   1             1             = 
    68 SINGLE_ENTRY[8]
                                   1             1             = 
    69 SINGLE_EXIT[0]
                                   1             1             = 
    70 SINGLE_EXIT[1]
                                   1             1             = 
    71 SINGLE_EXIT[2]
                                   1             1             = 
    72 SINGLE_EXIT[3]
                                   1             1             = 
    73 SINGLE_EXIT[4]
                                   1             1             = 
    74 SINGLE_EXIT[5]
                                   1             1             = 
    75 SINGLE_EXIT[6]
                                   1             1             = 
    76 SINGLE_EXIT[7]
                                   1             1             = 
    77 PRECEDE[1,0]                1            -0               
    78 PRECEDE[2,0]                4            -0               
    79 PRECEDE[3,0]                9            -0               
    80 PRECEDE[4,0]                2            -0               
    81 PRECEDE[4,1]                1            -0               
    82 PRECEDE[5,0]                8            -0               
    83 PRECEDE[5,1]                7            -0               
    84 PRECEDE[5,4]                6            -0               
    85 PRECEDE[5,6]                1            -0               
    86 PRECEDE[5,7]                5            -0               
    87 PRECEDE[6,0]                7            -0               
    88 PRECEDE[6,1]                6            -0               
    89 PRECEDE[7,0]                3            -0               
    90 PRECEDE[7,1]                2            -0               
    91 PRECEDE[8,0]               10            -0               
    92 PRECEDE[8,1]                9            -0               
    93 PRECEDE[8,2]                6            -0               
    94 PRECEDE[8,3]                1            -0               
    95 PRECEDE[8,4]                8            -0               
    96 PRECEDE[8,5]                2            -0               
    97 PRECEDE[8,6]                3            -0               
    98 PRECEDE[8,7]                7            -0               
    99 FIRST_NODE_INDEX
                                   1             1             = 
   100 INDEX_ARC_IMPLICATION[1,2]
                                  -3                           8 
   101 INDEX_ARC_IMPLICATION[1,3]
                                  -8                           8 
   102 INDEX_ARC_IMPLICATION[1,4]
                                   8                           8 
   103 INDEX_ARC_IMPLICATION[1,5]
                                  -7                           8 
   104 INDEX_ARC_IMPLICATION[1,6]
                                  -6                           8 
   105 INDEX_ARC_IMPLICATION[1,7]
                                  -2                           8 
   106 INDEX_ARC_IMPLICATION[1,8]
                                  -9                           8 
   107 INDEX_ARC_IMPLICATION[2,1]
                                   3                           8 
   108 INDEX_ARC_IMPLICATION[2,3]
                                  -5                           8 
   109 INDEX_ARC_IMPLICATION[2,4]
                                   2                           8 
   110 INDEX_ARC_IMPLICATION[2,5]
                                  -4                           8 
   111 INDEX_ARC_IMPLICATION[2,6]
                                   6                           8 
   112 INDEX_ARC_IMPLICATION[2,7]
                                   1                           8 
   113 INDEX_ARC_IMPLICATION[2,8]
                                  -6                           8 
   114 INDEX_ARC_IMPLICATION[3,1]
                                   8                           8 
   115 INDEX_ARC_IMPLICATION[3,2]
                                   5                           8 
   116 INDEX_ARC_IMPLICATION[3,4]
                                   7                           8 
   117 INDEX_ARC_IMPLICATION[3,5]
                                   1                           8 
   118 INDEX_ARC_IMPLICATION[3,6]
                                   2                           8 
   119 INDEX_ARC_IMPLICATION[3,7]
                                   6                           8 
   120 INDEX_ARC_IMPLICATION[3,8]
                                   8                           8 
   121 INDEX_ARC_IMPLICATION[4,2]
                                  -2                           8 
   122 INDEX_ARC_IMPLICATION[4,3]
                                  -7                           8 
   123 INDEX_ARC_IMPLICATION[4,5]
                                  -6                           8 
   124 INDEX_ARC_IMPLICATION[4,6]
                                  -5                           8 
   125 INDEX_ARC_IMPLICATION[4,7]
                                   8                           8 
   126 INDEX_ARC_IMPLICATION[4,8]
                                  -8                           8 
   127 INDEX_ARC_IMPLICATION[5,2]
                                   4                           8 
   128 INDEX_ARC_IMPLICATION[5,3]
                                   8                           8 
   129 INDEX_ARC_IMPLICATION[5,8]
                                  -2                           8 
   130 INDEX_ARC_IMPLICATION[6,2]
                                   3                           8 
   131 INDEX_ARC_IMPLICATION[6,3]
                                  -2                           8 
   132 INDEX_ARC_IMPLICATION[6,4]
                                   5                           8 
   133 INDEX_ARC_IMPLICATION[6,5]
                                   8                           8 
   134 INDEX_ARC_IMPLICATION[6,7]
                                   4                           8 
   135 INDEX_ARC_IMPLICATION[6,8]
                                  -3                           8 
   136 INDEX_ARC_IMPLICATION[7,2]
                                   8                           8 
   137 INDEX_ARC_IMPLICATION[7,3]
                                  -6                           8 
   138 INDEX_ARC_IMPLICATION[7,4]
                                   1                           8 
   139 INDEX_ARC_IMPLICATION[7,5]
                                  -5                           8 
   140 INDEX_ARC_IMPLICATION[7,6]
                                  -4                           8 
   141 INDEX_ARC_IMPLICATION[7,8]
                                  -7                           8 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[0,1]       *              1             0             1 
     2 x[0,2]       *              0             0             1 
     3 x[0,3]       *              0             0             1 
     4 x[0,4]       *              0             0             1 
     5 x[0,5]       *              0             0             1 
     6 x[0,6]       *              0             0             1 
     7 x[0,7]       *              0             0             1 
     8 x[0,8]       *              0             0             1 
     9 x[1,2]       *              0             0             1 
    10 x[1,3]       *              0             0             1 
    11 x[1,4]       *              1             0             1 
    12 x[1,5]       *              0             0             1 
    13 x[1,6]       *              0             0             1 
    14 x[1,7]       *              0             0             1 
    15 x[1,8]       *              0             0             1 
    16 x[2,1]       *              0             0             1 
    17 x[2,3]       *              0             0             1 
    18 x[2,4]       *              0             0             1 
    19 x[2,5]       *              0             0             1 
    20 x[2,6]       *              1             0             1 
    21 x[2,7]       *              0             0             1 
    22 x[2,8]       *              0             0             1 
    23 x[3,1]       *              0             0             1 
    24 x[3,2]       *              0             0             1 
    25 x[3,4]       *              0             0             1 
    26 x[3,5]       *              0             0             1 
    27 x[3,6]       *              0             0             1 
    28 x[3,7]       *              0             0             1 
    29 x[3,8]       *              1             0             1 
    30 x[4,2]       *              0             0             1 
    31 x[4,3]       *              0             0             1 
    32 x[4,5]       *              0             0             1 
    33 x[4,6]       *              0             0             1 
    34 x[4,7]       *              1             0             1 
    35 x[4,8]       *              0             0             1 
    36 x[5,2]       *              0             0             1 
    37 x[5,3]       *              1             0             1 
    38 x[5,8]       *              0             0             1 
    39 x[6,2]       *              0             0             1 
    40 x[6,3]       *              0             0             1 
    41 x[6,4]       *              0             0             1 
    42 x[6,5]       *              1             0             1 
    43 x[6,7]       *              0             0             1 
    44 x[6,8]       *              0             0             1 
    45 x[7,2]       *              1             0             1 
    46 x[7,3]       *              0             0             1 
    47 x[7,4]       *              0             0             1 
    48 x[7,5]       *              0             0             1 
    49 x[7,6]       *              0             0             1 
    50 x[7,8]       *              0             0             1 
    51 y[0]         *              1             0             1 
    52 y[1]         *              1             0             1 
    53 y[2]         *              1             0             1 
    54 y[3]         *              1             0             1 
    55 y[4]         *              1             0             1 
    56 y[5]         *              1             0             1 
    57 y[6]         *              1             0             1 
    58 y[7]         *              1             0             1 
    59 y[8]         *              1             0             1 
    60 u[1]                        1             0               
    61 u[0]                        0             0               
    62 u[2]                        4             0               
    63 u[3]                        9             0               
    64 u[4]                        2             0               
    65 u[5]                        8             0               
    66 u[6]                        7             0               
    67 u[7]                        3             0               
    68 u[8]                       10             0               

Integer feasibility conditions:

KKT.PE: max.abs.err = 1.78e-15 on row 136
        max.rel.err = 7.11e-17 on row 136
        High quality

KKT.PB: max.abs.err = 1.78e-15 on row 125
        max.rel.err = 1.97e-16 on row 125
        High quality

End of output
