set VERTICES;
set ARCS within (VERTICES cross VERTICES);
set P_PAIRS within (VERTICES cross VERTICES);

param cost{ARCS};
param dimension;
param v0, symbolic, in VERTICES;
param vF, symbolic, in VERTICES;

var x{(i,j) in ARCS} binary;
var y{i in VERTICES} binary;
var u{i in VERTICES} >= 0;

minimize totalcost: sum{(i,j) in ARCS} x[i,j] * cost[i,j];
        
s.t. VISIT_ALL{i in VERTICES}: y[i] = 1;

s.t. ARC_NODE_IMPLICATION{(i,j) in ARCS}:
        2 * x[i,j] <= y[i] + y[j];
        
s.t. SINGLE_ENTRY{i in VERTICES: i <> v0}:
        sum{j in VERTICES: (j, i) in ARCS} x[j,i] = 1;
        
s.t. SINGLE_EXIT{i in VERTICES: i <> vF}:
        sum{j in VERTICES: (i, j) in ARCS} x[i,j] = 1;
        
s.t. PRECEDE{(i,j) in P_PAIRS}:
        u[i] >= u[j];

s.t. FIRST_NODE_INDEX:
		u[1] = 1;
        
s.t. INDEX_ARC_IMPLICATION{(i,j) in ARCS: i <> v0}:
		u[i] <= u[j] - 1 + (1 - x[i,j]) * dimension;

end;