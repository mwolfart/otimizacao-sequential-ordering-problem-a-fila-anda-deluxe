
public class Graph {
	private Integer[][] weights;
	private Integer numberOfNodes;

	public Graph(Integer[][] weights, Integer numberOfNodes) {
		this.setWeights(weights);
		this.setNumberOfNodes(numberOfNodes);
	}

	public void setWeights(Integer[][] weights) {
		this.weights = weights;
	}

	public void setNumberOfNodes(Integer numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}

	public Integer[][] getWeights() {
		return this.weights;
	}

	public Integer getNumberOfNodes() {
		return this.numberOfNodes;
	}

	public Integer getWeight(Integer source, Integer target) {
		return weights[source][target];
	}
}