
public class Main {   
    public static void main ( String [] args ) {
    	if (args.length != 6) {
    		System.out.println("Sintaxe: java -cp . Main <arquivo> <temperatura> <taxa de decremento> <temperatura minima> <numero de vizinhos> <semente>");
    	} else {
    		long startTime = System.currentTimeMillis();

    		String file = args[0];
    		Double temperature = Double.parseDouble(args[1]);
    		Double coolingRate = Double.parseDouble(args[2]);
    		Double minTemp = Double.parseDouble(args[3]);
    		Integer numOfGeneratedNeighbors = Integer.parseInt(args[4]);
    		Long seed = Long.parseLong(args[5], 10);
    		
    		System.out.println("Processando instancia \"" + file + "\"...");
	        Instance instance = new Instance();
	        instance = instance.parserGraphFromFile(file);
	        System.out.println("Instancia carregada! Resolvendo problema...");

	    	Graph graph = new Graph(instance.getWeightMatrix(), instance.getDimension());
	    	Path solution = new Path(graph);

	        solution.findRandomFullPath();
	        solution.runAnnealing(temperature, coolingRate, minTemp, numOfGeneratedNeighbors, seed);

	        System.out.println("-- SOLUCAO ENCONTRADA --");
       		System.out.println("Minima distancia percorrida: " + solution.getPathDistance());
        	System.out.println("Caminho percorrido: " + solution.getBestPath());

	        long endTime = System.currentTimeMillis();
	        long totalTime = endTime - startTime;
	        System.out.println("Processo terminado em " + totalTime + "ms");
	    }
    }
}