
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.lang.Math;

public class Path{
    private Graph graph;
    private List<Integer> bestPath;
    private Integer pathDistance;
    private Random randNum;
    
    public Path(Graph graph) {
        this.setGraph(graph);
        this.bestPath = new ArrayList<Integer>();
        this.pathDistance = 0;
        this.randNum = new Random();
    }
    
    public void setGraph(Graph graph) { this.graph = graph; }
    public Graph getGraph() { return this.graph; }
    public List<Integer> getBestPath() { return this.bestPath; }
    public Integer getPathDistance() { return this.pathDistance; }

    // Pega um caminho qualquer válido que visita todos os nodos
    public void findRandomFullPath() {
        // Inicializa o caminho andado, adicionando o nodo raiz.
        List<Integer> currentPath = new ArrayList<Integer>();
        Integer currentNodeIndex = 0;
        Integer currentNode = 0;
        currentPath.add(currentNode);

        // Inicializa os nodos não visitados
        List<Integer> notVisitedNodes = new ArrayList<Integer>();
        for (int i = 1; i < graph.getNumberOfNodes(); i++)
            notVisitedNodes.add(i);

        Boolean nodeRequisitesValid;    

        // Monta um caminho aleatório do nodo 0 pro nodo final, sempre respeitando os requisitos
        for(int i = 0; i < graph.getNumberOfNodes()-1; i++) {
            // Tenta encontrar um nodo que respeite os requisitos
            do { 
                currentNodeIndex = getRandomInt(0, notVisitedNodes.size());
                currentNode = notVisitedNodes.get(currentNodeIndex);
                nodeRequisitesValid = (preRequisitesValid(currentNode, currentPath));
            } while (!nodeRequisitesValid);


            // Adiciona nodo visitado no caminho
            currentPath.add(currentNode);

            // Retira dos nao visitados
            notVisitedNodes.remove(currentNode);
        }

        bestPath = currentPath;
        pathDistance = getTotalDistanceFromPath(currentPath);
    }

    // Simulated Annealing
    public void runAnnealing(Double temperature, Double coolingRate, double minTemperature, Integer numberOfNeighbors, Long seed){
        randNum.setSeed(seed);

        while (temperature > minTemperature) {
            temperature = temperature * coolingRate;

            // Deep copy
            List<Integer> newRandomPath = new ArrayList<Integer>(bestPath);

            for(int i = 1; i < numberOfNeighbors; i++) {
                // Gera um vizinho aleatório (um caminho alternativo)
                swapTwo(newRandomPath);

                if(!pathAttendsRequisites(newRandomPath)) {
                    // Vizinho (caminho) novo não é válido. Reseta-se usando deep copy.
                    newRandomPath = new ArrayList<Integer>(bestPath);
                }
                else {
                    // Vizinho válido. Mede-se a qualidade da solução.
                    Integer distanceOfNewBestPath = getTotalDistanceFromPath(newRandomPath);

                    Double probOfAcceptingWorse = Math.exp((pathDistance - distanceOfNewBestPath)/temperature) * 100;
                    Integer randInt = getRandomInt(0, 100);

                    // Se a distância for melhor (ou se o S.A. aceita a solução ruim), aceita o vizinho.
                    if ((distanceOfNewBestPath < pathDistance) || (randInt <= probOfAcceptingWorse)) {
                        bestPath = new ArrayList<Integer>(newRandomPath);
                        pathDistance = distanceOfNewBestPath;
                    }
                }
            }
        }     
    }

    /* MÉTODOS PRIVADOS */

    // Escolhe dois elementos ao acaso, numa lista de inteiros, e os troca de posição
    // Atualiza tambem os pesos na lista de pesos (eficiencia)
    private void swapTwo(List<Integer> list) {
        Integer index1, index2, temp;

        do {
            index1 = getRandomInt(1, graph.getNumberOfNodes() - 2);
            index2 = getRandomInt(1, graph.getNumberOfNodes() - 2);
        } while (index1 == index2);

        temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    // Verifica se um caminho atende os pré-requisitos - isto é, se a lista de precedência
    // é obedecida.
    private boolean pathAttendsRequisites(List<Integer> path){
        List<Integer> visitedNodes = new ArrayList<Integer>();

        for(int i = 0; i < path.size(); i++) {
            visitedNodes.add(path.get(i));
            if(preRequisitesValid(path.get(i), visitedNodes))
                continue;
            else
                return false;
        }

        return true;
    }

    // Dado um nodo, verifica se todos os pré-requisitos dele foram atendidos
    // Isto é, se todos os nodos que deveriam ser visitados antes dele foram visitados
    private boolean preRequisitesValid(Integer node, List<Integer> visitedNodes) {
        List<Integer> nodePreRequisites = computePreRequisitesOfNode(node);

        // Se temos mais pré-requisitos que nodos visitados, obviamente não foi atendido
        if (nodePreRequisites.size() > visitedNodes.size())
            return false;

        // Para cada pré-requisito, verifica se ele foi atendido
        for (int i = 0; i < nodePreRequisites.size(); i++) {
            Boolean preRequisiteNotAttended = (visitedNodes.indexOf(nodePreRequisites.get(i)) < 0);

            if (preRequisiteNotAttended)
               return false;
        }

        return true;
    }

    // Dado um nodo, computa todos os pré-requisitos dele (todos os nodos que
    // devem ser visitados antes dele).
    private List<Integer> computePreRequisitesOfNode(Integer node) {
        List<Integer> preRequisites = new ArrayList<Integer>();

        for (int i = 0; i < graph.getNumberOfNodes(); i++) {
            Boolean preRequisiteNecessary = (graph.getWeight(node, i) == -1);

            if(preRequisiteNecessary)
                preRequisites.add(i);
        }

        return preRequisites;
    }


    // Dado um caminho, retorna a soma total dele (usando as distâncias)
    private Integer getTotalDistanceFromPath(List<Integer> path) {
        Integer totalDistance = 0;
        Integer lastVisitedNode = path.get(0);

        for (int i = 1; i < path.size(); i++) {
            Integer currentNode = path.get(i);
            totalDistance += graph.getWeight(lastVisitedNode, currentNode);
            lastVisitedNode = currentNode;
        }

        return totalDistance;
    }  

    //Pega um inteiro aleatório entre 0 e um dado limitante superior
    private Integer getRandomInt(Integer lowerLimitant, Integer upperLimitant) {
        return randNum.nextInt(upperLimitant - lowerLimitant) + lowerLimitant;
    }
}