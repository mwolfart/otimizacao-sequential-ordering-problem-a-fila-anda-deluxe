import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.PrintWriter;

public class Instance {
	private String name;
	private String type;
	private String Comment;
	private Integer dimension;
	private String edgeWeightType;
	private String edgeWeightFormat;
	private Integer[][] weightMatrix;

	public Instance() {
	}

	public Instance(String name, String type, String comment,
			Integer dimension, String edgeWeightType, String edgeWeightFormat,
			Integer[][] weightMatrix) {
		super();
		this.name = name;
		this.type = type;
		Comment = comment;
		this.dimension = dimension;
		this.edgeWeightType = edgeWeightType;
		this.edgeWeightFormat = edgeWeightFormat;
		this.weightMatrix = weightMatrix;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public String getEdgeWeightType() {
		return edgeWeightType;
	}

	public void setEdgeWeightType(String edgeWeightType) {
		this.edgeWeightType = edgeWeightType;
	}

	public String getEdgeWeightFormat() {
		return edgeWeightFormat;
	}

	public void setEdgeWeightFormat(String edgeWeightFormat) {
		this.edgeWeightFormat = edgeWeightFormat;
	}

	public Integer[][] getWeightMatrix() {
		return weightMatrix;
	}

	public void setWeightMatrix(Integer[][] weightMatrix) {
		this.weightMatrix = weightMatrix;
	}

	@Override
	public String toString() {
		return "Instance [name=" + name + ", type=" + type + ", Comment="
				+ Comment + ", dimension=" + dimension + ", edgeWeightType="
				+ edgeWeightType + ", edgeWeightFormat=" + edgeWeightFormat
				+ "]";
	}

	// Converte uma instância para um arquivo .dat, para usar no GLPK
	public void instanceToFile() {
		try {
			PrintWriter writer = new PrintWriter("ESC07.dat", "UTF-8");

			writer.println("set VERTICES :=");

			for(int i = 0; i < dimension; i++) {
				writer.println("	    " + i);
			}

			writer.println("	;");
			writer.println("");
			writer.println("param dimension := " + dimension + ";");
			writer.println("param v0 := 0;");
			writer.println("param vF := " + (dimension-1) + ";");
			writer.println("param: ARCS: cost :=");

			for(int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					if ((i != j) && (weightMatrix[i][j] != -1)) {
						writer.println("	    " + i + "     " + j + "    " + weightMatrix[i][j]);
					}
				}
			}

			writer.println("	;");
			writer.println("");
			writer.println("set P_PAIRS :=");

			for(int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					if ((i != j) && (weightMatrix[i][j] == -1)) {
						writer.println("	    " + i + "     " + j);
					}
				}
			}

			writer.println("	;");
			writer.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	// Lê uma instância contida em um arquivo
	public Instance parserGraphFromFile(String file) {
		Instance instance = new Instance();

		List<String> linesFile = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			int count = 1;

			while (br.ready()) {
				String line = br.readLine();
				String[] part = line.split(" ");

				if (count < 8) {
					switch (count) {
					case 1:
						instance.setName(part[1]);
						break;
					case 2:
						instance.setType(part[1]);
						break;
					case 3:
						String[] comment = line.split(":");
						instance.setComment(comment[1]);
						break;
					case 4:
						instance.setDimension(Integer.parseInt(part[1]));
						break;
					case 5:
						instance.setEdgeWeightType(part[1]);
						break;
					case 6:
						instance.setEdgeWeightFormat(part[1]);
						break;
					default:
					}
					count++;
				} else {
					line = line.trim().replaceAll(" +", " ");
					linesFile.add(line);
				}
			}
			
			br.close();
		} catch (IOException io) {
			io.printStackTrace();
		}

		// Construindo matriz de pesos
		Integer dimension = instance.getDimension();
		Integer[][] weights = new Integer[dimension][dimension];

		int j = 0;
		for (int i = 0; i < dimension; i++) {
			String line = linesFile.get(i);
			int currentNumber = 0;
			String newNumber = "";

			line = line + " "; // Assim o último número é incluso
			while (j < line.length()) {

				if (line.charAt(j) != ' ') {
					newNumber = newNumber + line.charAt(j);
				}
				else
				{
					weights[i][currentNumber] = Integer.parseInt(newNumber);
					currentNumber++;
					newNumber = "";
				}

				j++;
			}
			j = 0;
		}

		instance.setWeightMatrix(weights);

		for (int i = 0; i < dimension; i++) {
			//System.out.print(i + "->");
			for (j = 0; j < dimension; j++) {
				//System.out.print(" " + weights[i][j]);

			}
			//System.out.println(" ");
		}

		return instance;
	}

}
